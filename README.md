# mCrossNet

mCrossNet is an unofficial protocol library for versions of Minecraft between Minecraft Classic c0.30_01c and Minecraft Beta 1.8.1.

It was going to cover other versions, and then I realized that was going to take forever.

It also contains an experimental 'workbench' server for testing the protocol library.

Consider this project a basis for further work.

## Credits

wiki.vg's history archive for the "Protocol" page is the primary source of information on the protocol.

However, it is to be said that this method of handling protocol versioning doesn't allow for retroactive work to the documentation,
 so I ended up having to work out a few things myself.

Between that and the mechanism used to reduce the workload, errors are likely my own fault.

For details, please see THANKS.md

## License

CC0 (see COPYING.txt)

