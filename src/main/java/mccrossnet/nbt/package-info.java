/**
 * Exists as a feature-rich CC0 implementation of NBT and SNBT.
 * While these have storage uses, these are also critical formats
 *  for later protocol iterations.
 */
package mccrossnet.nbt;