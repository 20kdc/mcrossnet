package mccrossnet.nbt;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

/**
 * Yup, it's SNBT time again!
 * This time, writing.
 */
public class SNBTWriter {
	public final Writer base;
	public int indentC = 0;
	public final String indent;
	
	/**
	 * 
	 * @param w The target
	 * @param prettyIndent Null to disable prettyprint
	 */
	public SNBTWriter(Writer w, String prettyIndent) {
		base = w;
		indent = prettyIndent;
	}
	
	private void enterIndent() throws IOException {
		indentC++;
		newLine();
	}
	private void exitIndent() throws IOException {
		indentC--;
		newLine();
	}
	private void newLine() throws IOException {
		if (indent != null) {
			base.write('\n');
			for (int i = 0; i < indentC; i++)
				base.write(indent);
		}
	}
	
	/**
	 * Writes some Java-marshalled NBT as SNBT.
	 * @param nbt The marshalled NBT
	 */
	public void write(Object nbt) throws IOException {
		if (nbt instanceof Byte) {
			base.write(nbt.toString());
			base.write('b');
		} else if (nbt instanceof Short) {
			base.write(nbt.toString());
			base.write('s');
		} else if (nbt instanceof Integer) {
			base.write(nbt.toString());
		} else if (nbt instanceof Long) {
			base.write(nbt.toString());
			base.write('L');
		} else if (nbt instanceof Float) {
			base.write(nbt.toString());
			base.write('f');
		} else if (nbt instanceof Double) {
			base.write(nbt.toString());
			base.write('d');
		} else if (nbt instanceof byte[]) {
			base.write("[B;");
			enterIndent();
			boolean first = true;
			for (byte b : (byte[]) nbt) {
				if (!first)
					base.write(',');
				first = false;
				base.write(Byte.toString(b));
			}
			exitIndent();
			base.write("]");
		} else if (nbt instanceof String) {
			base.write('"');
			for (char c : ((String) nbt).toCharArray()) {
				if (c == '"') {
					base.write("\\\"");
				} else {
					base.write(c);
				}
			}
			base.write('"');
		} else if (nbt instanceof Object[]) {
			base.write('[');
			enterIndent();
			boolean first = true;
			for (Object val : (Object[]) nbt) {
				if (!first) {
					base.write(',');
					newLine();
				}
				first = false;
				write(val);
			}
			exitIndent();
			base.write(']');
		} else if (nbt instanceof HashMap) {
			HashMap<String, Object> map = (HashMap<String, Object>) nbt;
			base.write('{');
			enterIndent();
			boolean first = true;
			for (Map.Entry<String, Object> obj : map.entrySet()) {
				if (!first) {
					base.write(',');
					newLine();
				}
				first = false;
				write(obj.getKey());
				base.write(':');
				if (indent != null)
					base.write(' ');
				write(obj.getValue());
			}
			exitIndent();
			base.write('}');
		} else if (nbt instanceof int[]) {
			base.write("[I;");
			enterIndent();
			boolean first = true;
			for (int b : (int[]) nbt) {
				if (!first)
					base.write(',');
				first = false;
				base.write(Integer.toString(b));
			}
			exitIndent();
			base.write("]");
		} else if (nbt instanceof long[]) {
			base.write("[L;");
			enterIndent();
			boolean first = true;
			for (long b : (long[]) nbt) {
				if (!first)
					base.write(',');
				first = false;
				base.write(Long.toString(b));
			}
			exitIndent();
			base.write("]");
		} else {
			throw new RuntimeException("Now why'd you put that in here? " + nbt);
		}
	}
}
