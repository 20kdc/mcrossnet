package mccrossnet.nbt;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Named Binary Tag reading/writing.
 * 
 * Read marshalling is as follows:
 * 
 * All numbers are converted to their respective boxed numeric types.
 * String is String.
 * ByteArray, IntArray, and LongArray are converted to their respective array types.
 * List is converted to Object[]. Empty lists are converted to zero-sized byte Lists.
 * Compound is converted to HashMap<String, Object>.
 * End is converted to null.
 * 
 * Write marshalling amounts to inverting the process.
 * However, the restrictions are a little looser.
 * Any Map with String keys and usable values is a valid Compound.
 */
public class NamedBinaryTag {
	/**
	 * Reads an NBT compound's content.
	 * 
	 * @param dis The source.
	 * @param single If only a single tag should be read (use this for reading the root tag)
	 * @return The resulting compound.
	 * @throws IOException
	 */
	public static HashMap<String, Object> read(DataInputStream dis, boolean single) throws IOException {
		HashMap<String, Object> content = new HashMap<String, Object>();
		while (true) {
			int type = dis.read();
			if (type == 0 || type == -1)
				break;
			String name = dis.readUTF();
			content.put(name, readContent(dis, (byte) type));
			// If in single-tag mode, break now
			if (single)
				break;
		}
		return content;
	}

	/**
	 * Reads the content of a tag.
	 * 
	 * @param dis The source.
	 * @param byte The tag ID.
	 * @return The resulting value, or null on EOF or TAG_End.
	 * @throws IOException
	 */
	public static Object readContent(DataInputStream dis, byte type) throws IOException {
		if (type == 0) {
			// End
			// You shouldn't actually get here, but ok.
			return null;
		} else if (type == 1) {
			return dis.readByte();
		} else if (type == 2) {
			return dis.readShort();
		} else if (type == 3) {
			return dis.readInt();
		} else if (type == 4) {
			return dis.readLong();
		} else if (type == 5) {
			return dis.readFloat();
		} else if (type == 6) {
			return dis.readDouble();
		} else if (type == 7) {
			byte[] data = new byte[dis.readInt()];
			dis.readFully(data);
			return data;
		} else if (type == 8) {
			return dis.readUTF();
		} else if (type == 9) {
			// List
			byte subType = dis.readByte();
			Object[] list = new Object[dis.readInt()];
			for (int i = 0; i < list.length; i++)
				list[i] = readContent(dis, subType);
			return list;
		} else if (type == 10) {
			// Compound! Read it.
			return read(dis, false);
		} else if (type == 11) {
			int[] data = new int[dis.readInt()];
			for (int i = 0; i < data.length; i++)
				data[i] = dis.readInt();
			return data;
		} else if (type == 12) {
			long[] data = new long[dis.readInt()];
			for (int i = 0; i < data.length; i++)
				data[i] = dis.readLong();
			return data;
		} else {
			throw new IOException("Unable to get content of this type.");
		}
	}
	
	/**
	 * Writes a single named tag.
	 * @param dos Where to write the tag.
	 * @param name The name.
	 * @param value The value.
	 */
	public static void write(DataOutputStream dos, String name, Object value) throws IOException {
		if (value == null) {
			dos.writeByte(0);
		} else {
			dos.writeByte(getTypeOf(value.getClass()));
			dos.writeUTF(name);
			writeContent(dos, value);
		}
	}
	
	/**
	 * Gets the marshalled type of an object.
	 * @param o The object to check.
	 * @return The result.
	 */
	public static byte getTypeOf(Class c) {
		if ((c == Byte.class) || (c == byte.class))
			return (byte) 1;
		if ((c == Short.class) || (c == short.class))
			return (byte) 2;
		if ((c == Integer.class) || (c == int.class))
			return (byte) 3;
		if ((c == Long.class) || (c == long.class))
			return (byte) 4;
		if ((c == Float.class) || (c == float.class))
			return (byte) 5;
		if ((c == Double.class) || (c == double.class))
			return (byte) 6;
		if (c == byte[].class)
			return (byte) 7;
		if (c == String.class)
			return (byte) 8;
		if (c == Object[].class)
			return (byte) 9;
		if (c == int[].class)
			return (byte) 11;
		if (c == long[].class)
			return (byte) 12;
		// put this at the end because it's so general
		if (Map.class.isAssignableFrom(c))
			return (byte) 10;
		throw new RuntimeException("Cannot marshal a " + c);
	}
	
	/**
	 * Writes the content of a tag.
	 * Note that the type byte is assumed.
	 * @param dos Where to write
	 * @param o What to write
	 */
	public static void writeContent(DataOutputStream dos, Object o) throws IOException {
		if (o instanceof Byte) {
			dos.writeByte((Byte) o);
		} else if (o instanceof Short) {
			dos.writeShort((Short) o);
		} else if (o instanceof Integer) {
			dos.writeInt((Integer) o);
		} else if (o instanceof Long) {
			dos.writeLong((Long) o);
		} else if (o instanceof Float) {
			dos.writeFloat((Float) o);
		} else if (o instanceof Double) {
			dos.writeDouble((Double) o);
		} else if (o instanceof byte[]) {
			byte[] b = (byte[]) o;
			dos.writeInt(b.length);
			dos.write(b);
		} else if (o instanceof String) {
			dos.writeUTF((String) o);
		} else if (o instanceof Object[]) {
			byte tag = 1;
			Object[] list = (Object[]) o;
			if (list.length > 0)
				tag = getTypeOf(list[0].getClass());
			dos.write(tag);
			dos.writeInt(list.length);
			for (Object ov : list) {
				if (getTypeOf(ov.getClass()) != tag)
					throw new IOException("Mismatch in list");
				writeContent(dos, ov);
			}
		} else if (o instanceof int[]) {
			int[] b = (int[]) o;
			dos.writeInt(b.length);
			for (int v : b)
				dos.writeInt(v);
		} else if (o instanceof long[]) {
			long[] b = (long[]) o;
			dos.writeInt(b.length);
			for (long v : b)
				dos.writeLong(v);
		} else if (o instanceof Map) {
			// Alright, it's time to write a compound.
			for (Map.Entry<String, Object> entry : ((Map<String, Object>) o).entrySet())
				write(dos, entry.getKey(), entry.getValue());
			// Write ending tag.
			dos.writeByte(0);
		} else {
			throw new IOException("You broke it, didn't you");
		}
	}
}
