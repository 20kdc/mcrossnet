package mccrossnet.nbt;

import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * The code to read SNBT.
 */
public class SNBTReader {
	private final Reader base;
	private LinkedList<Integer> unget = new LinkedList<Integer>();
	
	public SNBTReader(Reader r) {
		base = r;
	}
	
	private int getChar() throws IOException {
		if (!unget.isEmpty())
			return unget.removeLast();
		return base.read();
	}
	
	private void eliminateWhitespace() throws IOException {
		while (true) {
			int chr = getChar();
			if (chr < 0)
				return;
			if (chr > 32) {
				unget.add(chr);
				break;
			}
		}
	}
	
	/**
	 * Reads an SNBT object.
	 * @return A NBT-marshalled-into-Java object (see NamedBinaryTag) or null (EOF) 
	 */
	public Object read() throws IOException {
		eliminateWhitespace();
		int base = getChar();
		if (base < 0)
			return null;
		if ((base == '\'') || (base == '\"')) {
			StringBuilder sb = new StringBuilder();
			boolean escape = false;
			while (true) {
				int val = getChar();
				if (val < 0)
					throw new IOException("String interrupted");
				if (escape) {
					sb.append((char) val);
					escape = false;
				} else if (val == '\\') {
					escape = true;
				} else if (val == base) {
					break;
				} else {
					sb.append((char) val);
				}
			}
			return sb.toString();
		} else if (base == '{') {
			HashMap<String, Object> map = new HashMap<String, Object>();
			boolean first = true;
			while (true) {
				eliminateWhitespace();
				int terminatorCheck = getChar();
				if (terminatorCheck < 0)
					throw new IOException("Compound interrupted");
				if (terminatorCheck == ']')
					throw new IOException("Compound ended with list end");
				if (terminatorCheck == '}')
					break;
				if (!first) {
					if (terminatorCheck != ',')
						throw new IOException("Requires comma");
				} else {
					unget.add(terminatorCheck);
				}
				Object key = read();
				if (!(key instanceof String))
					throw new IOException("Compound key must be string");
				eliminateWhitespace();
				int midCheck = getChar();
				if (midCheck != ':')
					throw new IOException("Requires k/v separator");
				Object value = read();
				map.put((String) key, value);
				first = false;
			}
			return map;
		} else if (base == '[') {
			// Determine if this list is a byte array, int array, etc.
			int transformTypeA = getChar();
			if (transformTypeA == ']') {
				unget.add(transformTypeA);
				transformTypeA = 0;
			} else {
				int transformTypeB = getChar();
				if (transformTypeB != ';') {
					unget.add(transformTypeB);
					unget.add(transformTypeA);
					transformTypeA = 0;
				}
			}
			
			LinkedList<Object> list = new LinkedList<Object>();
			boolean first = true;
			while (true) {
				eliminateWhitespace();
				int terminatorCheck = getChar();
				if (terminatorCheck < 0)
					throw new IOException("List interrupted");
				if (terminatorCheck == '}')
					throw new IOException("List ended with compound end");
				if (terminatorCheck == ']')
					break;
				if (!first) {
					if (terminatorCheck != ',')
						throw new IOException("Requires comma");
				} else {
					unget.add(terminatorCheck);
				}
				Object key = read();
				list.add(key);
				first = false;
			}
			
			// Handle array stuff
			if (transformTypeA == 'B') {
				byte[] array = new byte[list.size()];
				for (int i = 0; i < array.length; i++) {
					Object dbgVal = list.removeFirst();
					//System.out.println(dbgVal);
					array[i] = ((Number) dbgVal).byteValue();
				}
				return array;
			} else if (transformTypeA == 'I') {
				int[] array = new int[list.size()];
				for (int i = 0; i < array.length; i++)
					array[i] = ((Number) list.removeFirst()).intValue();
				return array;
			} else if (transformTypeA == 'L') {
				long[] array = new long[list.size()];
				for (int i = 0; i < array.length; i++)
					array[i] = ((Number) list.removeFirst()).longValue();
				return array;
			} else if (transformTypeA != 0) {
				throw new IOException("Unknown list transform type");
			}
			return list;
		} else {
			// Could be a string or could be a number, because this is weird
			StringBuilder sb = new StringBuilder();
			sb.append((char) base);
			while (true) {
				int val = getChar();
				if (val <= 32)
					break;
				if ((val == '[') || (val == ']') || (val == '{') || (val == '}') || (val == ':') || (val == ',')) {
					unget.add(val);
					break;
				}
				sb.append((char) val);
			}
			String result = sb.toString();
			// Determine if number
			try {
				return Integer.parseInt(result);
			} catch (NumberFormatException nfe) {
				// This just means it's not accurate and we should move on to the next case.
			}
			try {
				return Double.parseDouble(result);
			} catch (NumberFormatException nfe) {
				// This just means it's not accurate and we should move on to the next case.
			}
			// Determine if postfixed number
			char last = result.charAt(result.length() - 1);
			String outLast = result.substring(0, result.length() - 1);
			if ((last == 'b') || (last == 'B')) {
				try {
					return Byte.parseByte(outLast);
				} catch (NumberFormatException nfe) {
					// This just means it's not accurate and we should move on to the next case.
				}
			} else if ((last == 's') || (last == 'S')) {
				try {
					return Short.parseShort(outLast);
				} catch (NumberFormatException nfe) {
					// This just means it's not accurate and we should move on to the next case.
				}
			} else if ((last == 'l') || (last == 'L')) {
				try {
					return Long.parseLong(outLast);
				} catch (NumberFormatException nfe) {
					// This just means it's not accurate and we should move on to the next case.
				}
			} else if ((last == 'f') || (last == 'F')) {
				try {
					return Float.parseFloat(outLast);
				} catch (NumberFormatException nfe) {
					// This just means it's not accurate and we should move on to the next case.
				}
			} else if ((last == 'd') || (last == 'D')) {
				try {
					return Double.parseDouble(outLast);
				} catch (NumberFormatException nfe) {
					// This just means it's not accurate and we should move on to the next case.
				}
			}
			// Ok, string
			return result;
		}
	}
}
