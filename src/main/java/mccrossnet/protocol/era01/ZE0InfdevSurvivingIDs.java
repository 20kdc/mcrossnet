package mccrossnet.protocol.era01;

/*
 * This covers all of the IDs that were added
 *  in Infdev versions that survived until the Flattening..
 */
public class ZE0InfdevSurvivingIDs extends ZE0IndevSurvivingIDs {
	// The Minecraft Wiki changelogs helped a lot in finding
	//  which versions had what features.
	// That said, if any 'intermediate Flattenings' occurred,
	//  they wouldn't be accounted for here.
	// inf-20100227
	public static final short I_GOLDEN_APPLE = 322;
	// inf-20100607
	public static final byte B_SIGN_STANDING = 63;
	public static final byte B_LADDER = 64;
	public static final byte B_DOOR = 65;
	public static final short I_SIGN = 323;
	public static final short I_DOOR = 324;
	// inf-20100615
	public static final short I_BUCKET_EMPTY = 325;
	public static final short I_BUCKET_WATER = 326;
	public static final short I_BUCKET_LAVA = 327;
	// inf-20100618
	public static final byte B_RAIL = 66;
	public static final short I_MINECART = 328;
	// inf-20100624 has a save file format specific to it that SHOULD be researched
	//  for fun, but doesn't have any new blocks.
	// inf-20100625-2
	// According to: https://minecraft.gamepedia.com/Java_Edition_Infdev_20100625-2
	// This was when 52/53, which as you know are non-surviving,
	//  were removed
	public static final short I_SADDLE = 329;
	public static final byte B_SPAWNER = 52;
	// inf-20100629
	public static final byte B_WOOD_STAIRS = 53;
	public static final byte B_COBBLE_STAIRS = 67;
	// after this point Alpha is entered;
	// see ZE0AlphaSurvivingIDs
}
