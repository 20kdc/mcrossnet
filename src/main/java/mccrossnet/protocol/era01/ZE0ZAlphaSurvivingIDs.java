package mccrossnet.protocol.era01;

/*
 * This represents the IDs added after Alpha began but before 
 */
public class ZE0ZAlphaSurvivingIDs extends ZE0InfdevSurvivingIDs {
	// v1.0.0 was retroactive and simply set up the boundary that made
	//  this Alpha as opposed to the other versions.
	// https://minecraft.gamepedia.com/Java_Edition_Alpha_v1.0.1
	public static final byte B_SIGN_WALL = 68;
	public static final byte B_LEVER = 69;
	public static final byte B_STONE_PLATE = 70;
	public static final byte B_IRON_DOOR = 71;
	public static final byte B_WOOD_PLATE = 72;
	public static final byte B_REDSTONE_ORE = 73;
	public static final byte B_REDSTONE_ORE_ACTIVATED = 74;
	public static final byte B_REDSTONE_TORCH_OFF = 75;
	public static final byte B_REDSTONE_TORCH_ON = 76;
	public static final byte B_STONE_BUTTON = 77;
	
	public static final short I_IRON_DOOR = 330;
	public static final short I_REDSTONE_DUST = 331;
	// v1.0.4 adds snow & ice.
	// https://minecraft.gamepedia.com/Java_Edition_Alpha_v1.0.4
	public static final byte B_SNOW = 78;
	public static final byte B_ICE = 79;
	// v1.0.5 adds snow blocks & snowballs.
	// https://minecraft.gamepedia.com/Java_Edition_Alpha_v1.0.5
	public static final byte B_SNOW_BLOCK = 80;
	public static final short I_SNOWBALL = 332;
	// v1.0.5_01's changelog doesn't add blocks BUT does imply
	//  that SMP history has already begun here.
	// v1.0.6 adds boats & cacti
	// You know the URLs by this point so I won't repeat them
	public static final byte B_CACTUS = 81;
	public static final short I_BOAT = 333;
	// v1.0.8 adds milk as an unobtainable item, but it's there
	// It also adds leather
	public static final short I_LEATHER = 334;
	public static final short I_BUCKET_MILK = 335;
	
	// --- FROM THIS POINT FORWARD, AN UNMODIFIED CLIENT
	// CAN *TECHNICALLY* ACCESS THE MULTIPLAYER FUNCTIONALITY ---
	// (but it's basically out of scope)
	
	// v1.0.10 is where multiplayer got semi-added,
	//  but it's only accessible via an "invite code".
	// The invite code scheme in this version has an encrypted server IP & port,
	//  which is decrypted by the invite code.
	// Luckily, it got leaked during an "invite your friends" test.
	// Of course you'd still need a custom server AND you'd need to reroute the
	//  IP to localhost in order to actually experience this.
	// So it's out of scope for mccrossnet.
	//  invite code: "!!Thomas!!"
	// thanks, https://twitter.com/arlnet/status/19622552811

	// v1.0.11 adds a bunch of survival stuff to make
	//  bookshelves & bricks obtainable.
	public static final byte B_CLAY = 82;
	public static final byte B_CANE = 83;
	public static final short I_BRICK = 336;
	public static final short I_CLAY = 337;
	public static final short I_CANE = 338;
	public static final short I_PAPER = 339;
	public static final short I_BOOK = 340;
	public static final short I_SLIMEBALL = 341;
	// v1.0.12: nothing really interesting?
	// v1.0.13: SMP goes semi-public, with the invite code written on the dialog
	//  invite code: "LaserPeople"
	// v1.0.14:
	//  invite code: "GravelHorse"
	public static final byte B_JUKEBOX = 84;
	public static final short I_MINECART_CHEST = 342;
	public static final short I_MINECART_FURNACE = 343;
	public static final short I_EGG = 344;
	public static final short I_MUSIC_13 = 2256;
	public static final short I_MUSIC_CAT = 2257;
	// --- v1.0.15 is E1V13, and thus we're out of the ERA0/1 transition period ---
	
}
