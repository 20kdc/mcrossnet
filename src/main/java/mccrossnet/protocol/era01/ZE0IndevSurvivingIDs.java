package mccrossnet.protocol.era01;

import mccrossnet.protocol.era0.v7.ZE0V7SurvivingIDs;

/*
 * REGARDING THE NAME:
 * INDEV DOES NOT HAVE A PROTOCOL VERSION.
 * So per-version Indev history is totally and completely useless. 
 * 
 * This contains all things from Indev that survived until the Flattening.
 * So 52 & 53 aren't in here because they got replaced,
 *  and Wool isn't present (as with the Classic listing).
 * This is otherwise the Indev block listing.
 */
public class ZE0IndevSurvivingIDs extends ZE0V7SurvivingIDs {
	// -- START OF INDEV STUFF --
	// Need to work out what's in this version past this point,
	//  but it's AT LEAST this
	public static final byte B_TORCH = 50;
	public static final byte B_FIRE = 51;
	// 52/53 did not survive as-is
	public static final byte B_CHEST = 54;
	// 55 did not survive as-is
	public static final byte B_DIAMOND_ORE = 56;
	public static final byte B_DIAMOND_BLOCK = 57;
	public static final byte B_CRAFTING_TABLE = 58;
	public static final byte B_WHEAT = 59;
	public static final byte B_FARMLAND = 60;
	public static final byte B_FURNACE_OFF = 61;
	public static final byte B_FURNACE_ON = 62;
	
	// random early tools & citems (inc. iron base 4)
	public static final short I_IRON_SHOVEL = 256;
	public static final short I_IRON_PICK = 257;
	public static final short I_IRON_AXE = 258;
	public static final short I_FLINT_STEEL = 259;
	public static final short I_APPLE = 260;
	public static final short I_BOW = 261;
	public static final short I_ARROW = 262;
	public static final short I_COAL = 263;
	public static final short I_DIAMOND = 264;
	public static final short I_IRON_INGOT = 265;
	public static final short I_GOLD_INGOT = 266;
	public static final short I_IRON_SWORD = 267;
	// wood stone diamond base 4
	public static final short I_WOOD_SWORD = 268;
	public static final short I_WOOD_SHOVEL = 269;
	public static final short I_WOOD_PICK = 270;
	public static final short I_WOOD_AXE = 271;
	public static final short I_STONE_SWORD = 272;
	public static final short I_STONE_SHOVEL = 273;
	public static final short I_STONE_PICK = 274;
	public static final short I_STONE_AXE = 275;
	public static final short I_DIAMOND_SWORD = 276;
	public static final short I_DIAMOND_SHOVEL = 277;
	public static final short I_DIAMOND_PICK = 278;
	public static final short I_DIAMOND_AXE = 279;
	// citems
	public static final short I_STICK = 280;
	// bowl/stew
	public static final short I_BOWL = 281;
	public static final short I_MUSHROOM_STEW = 282;
	// gold base 4
	public static final short I_GOLD_SWORD = 283;
	public static final short I_GOLD_SHOVEL = 284;
	public static final short I_GOLD_AXE = 285;
	public static final short I_GOLD_PICK = 286;
	// citems
	public static final short I_STRING = 287;
	public static final short I_FEATHER = 288;
	public static final short I_GUNPOWDER = 289;
	// the entirety of early farming
	public static final short I_WOOD_HOE = 290;
	public static final short I_STONE_HOE = 291;
	public static final short I_IRON_HOE = 292;
	public static final short I_DIAMOND_HOE = 293;
	public static final short I_GOLD_HOE = 294;
	public static final short I_SEEDS = 295;
	public static final short I_WHEAT = 296;
	public static final short I_BREAD = 297;
	// armour
	public static final short I_LEATHER_A0 = 298;
	public static final short I_LEATHER_A1 = 299;
	public static final short I_LEATHER_A2 = 300;
	public static final short I_LEATHER_A3 = 301;
	public static final short I_CHAIN_A0 = 302;
	public static final short I_CHAIN_A1 = 303;
	public static final short I_CHAIN_A2 = 304;
	public static final short I_CHAIN_A3 = 305;
	public static final short I_IRON_A0 = 306;
	public static final short I_IRON_A1 = 307;
	public static final short I_IRON_A2 = 308;
	public static final short I_IRON_A3 = 309;
	public static final short I_DIAMOND_A0 = 310;
	public static final short I_DIAMOND_A1 = 311;
	public static final short I_DIAMOND_A2 = 312;
	public static final short I_DIAMOND_A3 = 313;
	public static final short I_GOLD_A0 = 314;
	public static final short I_GOLD_A1 = 315;
	public static final short I_GOLD_A2 = 316;
	public static final short I_GOLD_A3 = 317;
	public static final short I_FLINT = 318;
	public static final short I_PORKCHOP = 319;
	public static final short I_PORKCHOP_COOKED = 320;
	public static final short I_PAINTING = 321;
}
