package mccrossnet.protocol.era2.alpha.v3;

import mccrossnet.protocol.all.flags.AConnectPacketSeedAndDimension;
import mccrossnet.protocol.era2.alpha.v2.ZE2V2Codec;

/**
 * Alpha v1.2.0 / .1 or E2V3
 * <a href="protocol.html">Protocol</a>
 */
public class ZE2V3Codec extends ZE2V2Codec implements AConnectPacketSeedAndDimension {
	public ZE2V3Codec() {
		// https://wiki.vg/index.php?title=Protocol&oldid=124
		registry = ZE2V3IDs.INSTANCE;
		// The change to 0x01 occurs via AConnectPacketSeedAndDimension
	}
}
