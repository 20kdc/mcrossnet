package mccrossnet.protocol.era2.alpha.v3;

import mccrossnet.protocol.era2.alpha.v1.ZMob48IDs;
import mccrossnet.protocol.era2.alpha.v1.ZMob49IDs;
import mccrossnet.protocol.era2.alpha.v2.ZE2V2IDs;
import mccrossnet.protocol.era2.alpha.v2.ZE2V2SurvivingIDs;

public class ZE2V3IDs extends ZE2V3SurvivingIDs implements ZMob48IDs, ZMob49IDs {
	public static final ZE2V3IDs INSTANCE = new ZE2V3IDs();
}
