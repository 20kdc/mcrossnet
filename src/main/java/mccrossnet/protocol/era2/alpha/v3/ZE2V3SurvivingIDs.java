package mccrossnet.protocol.era2.alpha.v3;

import mccrossnet.protocol.era2.alpha.v2.ZE2V2IDs;
import mccrossnet.protocol.era2.alpha.v2.ZE2V2SurvivingIDs;

public class ZE2V3SurvivingIDs extends ZE2V2SurvivingIDs {
	public static final byte B_PUMPKIN = 86;
	public static final byte B_NETHERRACK = 87;
	public static final byte B_SOULSAND = 88;
	public static final byte B_GLOWSTONE = 89;
	public static final byte B_NETHER_PORTAL = 90;
	public static final byte B_PUMPKIN_LIT = 91;
	
	public static final short I_CLOCK = 347;
	public static final short I_GLOWSTONE_DUST = 348;
	public static final short I_RAW_FISH = 349;
	public static final short I_COOKED_FISH = 350;

	public static final byte O_FISHING = 90;
	public static final byte O_GHAST_FIREBALL = 63;
	
	public static final byte M_GHAST = 56;
	public static final byte M_ZOMBIE_PIGMAN = 57;
}
