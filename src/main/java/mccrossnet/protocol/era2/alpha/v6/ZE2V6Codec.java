package mccrossnet.protocol.era2.alpha.v6;

import java.io.InputStream;
import java.io.OutputStream;

import mccrossnet.protocol.era2.alpha.v5.ZE2V5Codec;
import mccrossnet.base.data.FormatContext;

/**
 * <a href="protocol.html">Protocol</a>
 */
public class ZE2V6Codec extends ZE2V5Codec {

	public ZE2V6Codec() {
		// https://wiki.vg/index.php?title=Protocol&oldid=167
		// 0x26 is described as being added in Protocol Version 6.
		// It's also described as being added in Protocol Version 5.
		// Settling for 5.
		attachStoc(0x3C, BExplosion.class);
	}
	
}
