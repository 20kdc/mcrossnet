package mccrossnet.protocol.era2.alpha.v6;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Format;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.data.formats.IntPrefixed3ByteArrayFormat;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StandardStruct;

/**
 * I really, really don't understand this thing.
 * If I had to guess, I'd say the contents of vectors means 'remove these blocks'.
 * But one can never quite be sure.
 * There's already other packets to remove a lot of blocks efficiently.
 * And with more than one involved, those other packets become much better choices.
 */
public class BExplosion extends StandardStruct implements Packet {
	/**
	 * X
	 */
	@Order(0)
	public double x;
	
	/**
	 * Y
	 */
	@Order(1)
	public double y;
	
	/**
	 * Z
	 */
	@Order(2)
	public double z;
	
	/**
	 * Power
	 */
	@Order(3)
	public float power;
	
	/**
	 * Some list of blocks.
	 * Specifically, triplets of signed byte vectors.
	 * What does it do? Nobody knows!
	 */
	@Order(4)
	@Format(IntPrefixed3ByteArrayFormat.class)
	public byte[] vectors;
}
