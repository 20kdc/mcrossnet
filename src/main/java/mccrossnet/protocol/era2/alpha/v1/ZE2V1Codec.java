package mccrossnet.protocol.era2.alpha.v1;

import java.io.InputStream;
import java.io.OutputStream;

import mccrossnet.protocol.era1.v14.ZE1V14Codec;
import mccrossnet.base.data.FormatContext;

/**
 * Alpha v1.0.17 or E2V1
 * <a href="protocol.html">Protocol</a>
 */
public class ZE2V1Codec extends ZE1V14Codec {
	
	public ZE2V1Codec() {
		registry = ZE2V1IDs.INSTANCE;
		attachStoc(0x04, BTimeUpdate.class);
		attachStoc(0x18, BESpawnMob.class);
	}
	
}
