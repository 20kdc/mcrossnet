package mccrossnet.protocol.era2.alpha.v1;

/**
 * This covers a specific ID that spans multiple versions but disappears later.
 * Expect this to be a design pattern if I need to do this again.
 */
public interface ZMob48IDs {
	/**
	 * Appears as a human, though it's probably the base class.
	 * Useless as of Beta 1.2 & higher E2V8+, crashes.
	 */
	public static final byte M_MOB = 48;
}
