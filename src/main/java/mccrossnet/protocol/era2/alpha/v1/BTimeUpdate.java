package mccrossnet.protocol.era2.alpha.v1;

import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.StandardStruct;

/**
 * Sent by the server to update world time.
 */
public class BTimeUpdate extends StandardStruct implements Packet {
	/**
	 * The world time (in ticks).
	 */
	@Order(0)
	public long time;
	
	public BTimeUpdate() {
		
	}
	
	public BTimeUpdate(long time) {
		this.time = time;
	}
}
