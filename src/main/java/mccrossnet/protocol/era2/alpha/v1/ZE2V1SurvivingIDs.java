package mccrossnet.protocol.era2.alpha.v1;

import mccrossnet.protocol.era1.v13.ZE1V13IDs;

public class ZE2V1SurvivingIDs extends ZE1V13IDs {
	public static final byte B_FENCE = 85;
	
	public static final byte M_CREEPER = 50;
	public static final byte M_SPIDER = 51;
	public static final byte M_SKELETON = 52;
	public static final byte M_GIANT = 53;
	public static final byte M_ZOMBIE = 54;
	public static final byte M_SLIME = 55;
	
	public static final byte M_PIG = 90;
	public static final byte M_SHEEP = 91;
	public static final byte M_COW = 92;
	public static final byte M_CHICKEN = 93;
}
