package mccrossnet.protocol.era2.alpha.v1;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Format;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.protocol.all.EntityMetadata;
import mccrossnet.protocol.all.EntityPacket;
import mccrossnet.protocol.all.vectors.IntVec3;
import mccrossnet.protocol.all.vectors.IntVec3Format;
import mccrossnet.protocol.era0.v7.BETeleport;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;

public class BESpawnMob extends BETeleport {
	// eid
	
	/**
	 * The type of mob.
	 * See <a href="ids.html#mob">the 'mob' set of IDs for valid types</a>.
	 */
	@Order(1)
	public byte type;
	
	// pos/rot
	
	/**
	 * The entity metadata.
	 * Not supported until E2V8.
	 */
	@Order(4)
	public EntityMetadata[] metadata = EntityMetadata.NONE;
}
