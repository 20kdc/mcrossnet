package mccrossnet.protocol.era2.alpha.v1;

/**
 * This covers a specific ID that spans multiple versions but disappears later.
 * Expect this to be a design pattern if I need to do this again.
 */
public interface ZMob49IDs {
	/**
	 * Appears as a human, though it's probably the inner base class.
	 * Useless as of Beta 1.8 & higher (E2V17+), crashes.
	 */
	public static final byte M_MONSTER = 49;
}
