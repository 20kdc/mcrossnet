package mccrossnet.protocol.era2.alpha.v5;

import mccrossnet.protocol.era2.alpha.v1.ZMob48IDs;
import mccrossnet.protocol.era2.alpha.v1.ZMob49IDs;

public class ZE2V5IDs extends ZE2V5SurvivingIDs implements ZMob48IDs, ZMob49IDs {
	public static final ZE2V5IDs INSTANCE = new ZE2V5IDs();
}
