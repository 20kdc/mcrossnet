package mccrossnet.protocol.era2.alpha.v5;

import java.io.InputStream;
import java.io.OutputStream;

import mccrossnet.protocol.all.flags.BEClickEntityLeft;
import mccrossnet.protocol.all.flags.BRespawnDimension;
import mccrossnet.protocol.all.markers.BPlayerHealthValue;
import mccrossnet.protocol.era2.alpha.v4.ZE2V4Codec;
import mccrossnet.base.data.ValueFormat;
import mccrossnet.base.data.BaseFormat;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.formats.BooleanFormat;
import mccrossnet.base.data.formats.ByteAsShortFormat;
import mccrossnet.base.data.formats.ByteFormat;
import mccrossnet.base.data.formats.NotPresentInThisVersionFormat;

/**
 * Alpha v1.2.3 (not _05) or E2V5
 * <a href="protocol.html">Protocol</a>
 */
public class ZE2V5Codec extends ZE2V4Codec implements BEClickEntityLeft {

	public ZE2V5Codec() {
		// https://wiki.vg/index.php?title=Protocol&oldid=146
		//  is NOT definitive, because Entity Status was retroactively added to V5's history...
		// Having trouble finding V6 though
		// For info on that see ZE2V6Codec
		registry = ZE2V5IDs.INSTANCE;

		// The change to BEClickEntity is handled as a flag
		attachStoc(0x08, BPlayerHealth.class);
		attachCtos(0x09, BRespawnCtos.class);
		attachStoc(0x09, BRespawnStoc.class);
		attachStoc(0x26, BEMobAction.class);
	}

	@Override
	public <T> BaseFormat<T> getDefaultFormat(Class<T> fieldType) {
		if (fieldType == BPlayerHealthValue.class)
			return (BaseFormat<T>) ByteAsShortFormat.INSTANCE;
		return super.getDefaultFormat(fieldType);
	}
}
