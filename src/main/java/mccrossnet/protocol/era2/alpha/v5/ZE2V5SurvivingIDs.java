package mccrossnet.protocol.era2.alpha.v5;

import mccrossnet.protocol.era2.alpha.v3.ZE2V3IDs;
import mccrossnet.protocol.era2.alpha.v3.ZE2V3SurvivingIDs;

public class ZE2V5SurvivingIDs extends ZE2V3SurvivingIDs {
	public static final ZE2V5SurvivingIDs INSTANCE = new ZE2V5SurvivingIDs();
	
	public static final byte MA_HURT = 2;
	public static final byte MA_DIES = 3;
	public static final byte MA_CREEPER_IGNITE = 4;
}
