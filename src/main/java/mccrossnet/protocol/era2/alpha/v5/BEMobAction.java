package mccrossnet.protocol.era2.alpha.v5;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.protocol.all.EntityPacket;
import mccrossnet.protocol.all.EntityStruct;

/**
 * A totally-not-a-meaningless-packet packet that definitely couldn't be replaced with more use of BEAnimationA.
 */
public class BEMobAction extends EntityStruct implements Packet {
	/**
	 * The command for the entity (the amount of alternate ways to say 'magic ID' is getting silly)
	 * 0/1: Unknown, if anything
	 * 2: The mob is hurt (flashes red, twitches)
	 * 3: The mob dies (falls over, disappears in a puff of smoke)
	 * 
	 * Creeper:
	 * 4: Explodes, disappears
	 * 
	 * 5: ???
	 */
	@Order(1)
	public byte command;
}
