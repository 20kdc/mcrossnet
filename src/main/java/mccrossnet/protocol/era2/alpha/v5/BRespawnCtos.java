package mccrossnet.protocol.era2.alpha.v5;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Marker;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.data.annotations.Requires;
import mccrossnet.protocol.all.EmptyPacket;
import mccrossnet.protocol.all.flags.BRespawnDimension;
import mccrossnet.protocol.all.flags.TheAdventureUpdate;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StandardStruct;

/**
 * Sent CtoS to indicate the client wants to respawn.
 * Sent StoC to confirm that the client should respawn at the spawn position.
 */
public class BRespawnCtos extends StandardStruct implements Packet {
	/**
	 * Unused byte that doesn't exist until E2V13.
	 */
	@Order(0)
	@Requires(BRespawnDimension.class)
	public byte unused1;
	
	/**
	 * E2V15+
	 */
	@Order(1)
	@Requires(TheAdventureUpdate.class)
	public byte unused2;

	/**
	 * E2V15+
	 */
	@Order(2)
	@Requires(TheAdventureUpdate.class)
	public byte unused3;

	/**
	 * E2V15+
	 */
	@Order(3)
	@Requires(TheAdventureUpdate.class)
	public short unused4;

	/**
	 * E2V15+
	 */
	@Order(4)
	@Requires(TheAdventureUpdate.class)
	public long unused5;
}
