package mccrossnet.protocol.era2.alpha.v5;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Marker;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.data.annotations.Requires;
import mccrossnet.protocol.all.flags.TheAdventureUpdate;
import mccrossnet.protocol.all.markers.BPlayerHealthValue;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StandardStruct;

/**
 * StoC, contains the player's health.
 */
public class BPlayerHealth extends StandardStruct implements Packet {
	/**
	 * The player's health, in half-hearts.
	 * Defaults to 20 (full normal health).
	 * Check the marker for version-specific storage details.
	 */
	@Order(0)
	@Marker(BPlayerHealthValue.class)
	public short health = 20;

	public BPlayerHealth() {
	}
	public BPlayerHealth(short s) {
		health = s;
	}
	
	/**
	 * The state of the hunger bar.
	 * Note that the name is the "wrong way around".
	 * 20 is full, 0 is "probably about to die".
	 * Obviously, doesn't exist before b1.8.
	 */
	@Order(1)
	@Requires(TheAdventureUpdate.class)
	public short hunger = 20;
	
	/**
	 * The state of 'saturation'.
	 * From 0 to 5.
	 * This is kind of a "secret mechanic".
	 * Obviously, doesn't exist before b1.8.
	 */
	@Order(2)
	@Requires(TheAdventureUpdate.class)
	public float saturation = 5.0f;
}
