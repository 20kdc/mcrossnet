package mccrossnet.protocol.era2.alpha.v5;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Marker;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.data.annotations.Requires;
import mccrossnet.protocol.all.EmptyPacket;
import mccrossnet.protocol.all.flags.BRespawnDimension;
import mccrossnet.protocol.all.flags.TheAdventureUpdate;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StandardStruct;

/**
 * Sent CtoS to indicate the client wants to respawn.
 * Sent StoC to confirm that the client should respawn at the spawn position.
 */
public class BRespawnStoc extends StandardStruct implements Packet {
	/**
	 * Indicates the dimension being spawned into.
	 * Presumably acts as a replacement for the dimension transition packet in this case.
	 * Doesn't exist until E2V13.
	 */
	@Order(0)
	@Requires(BRespawnDimension.class)
	public byte dimension;

	/**
	 * Difficulty
	 * E2V15+
	 */
	@Order(1)
	@Requires(TheAdventureUpdate.class)
	public byte difficulty;

	/**
	 * Gamemode
	 * E2V15+
	 */
	@Order(2)
	@Requires(TheAdventureUpdate.class)
	public byte gamemode;

	/**
	 * World height
	 * E2V15+
	 */
	@Order(3)
	@Requires(TheAdventureUpdate.class)
	public short worldHeight = 128;

	/**
	 * E2V15+
	 */
	@Order(4)
	@Requires(TheAdventureUpdate.class)
	public long worldSeed;
}
