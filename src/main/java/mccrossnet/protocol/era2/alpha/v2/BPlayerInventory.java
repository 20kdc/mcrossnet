package mccrossnet.protocol.era2.alpha.v2;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Format;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StandardStruct;
import mccrossnet.protocol.all.items.ICDItemStack;
import mccrossnet.protocol.all.items.ShortPrefixedICDItemStackICDArrayFormat;

/**
 * This was introduced in E2V2 as a two-way (CtoS and StoC) packet.
 * Notice that server-side inventory was introduced in Beta.
 * This isn't server-side inventory.
 * Essentially the server just lets the client do anything,
 *  and the server acts as save/load storage for client inv.
 */
public class BPlayerInventory extends StandardStruct implements Packet {
	/**
	 * Which kind of inventory is being modified.
	 * 
	 * -1: Main 36 slots (including hotbar)
	 * -2: 4 Armour slots
	 * -3: 4 Crafting slots
	 */
	@Order(0)
	public int inventoryType;
	
	/**
	 * The contents of the inventory.
	 */
	@Order(1)
	@Format(ShortPrefixedICDItemStackICDArrayFormat.class)
	public ICDItemStack[] content;
}
