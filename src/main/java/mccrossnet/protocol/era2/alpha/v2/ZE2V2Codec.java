package mccrossnet.protocol.era2.alpha.v2;

import java.io.InputStream;
import java.io.OutputStream;

import mccrossnet.protocol.era2.alpha.v1.ZE2V1Codec;
import mccrossnet.base.data.FormatContext;

/**
 * Alpha v1.1.x or E2V2
 * <a href="protocol.html">Protocol</a>
 */
public class ZE2V2Codec extends ZE2V1Codec {

	public ZE2V2Codec() {
		// This is (finally!) equivalent to https://wiki.vg/index.php?title=Protocol&oldid=110
		registry = ZE2V2IDs.INSTANCE;
		
		attach(0x05, BPlayerInventory.class);
		attachStoc(0x06, BSpawnPosition.class);
		attach(0x3B, BTileEntity.class);
	}

}
