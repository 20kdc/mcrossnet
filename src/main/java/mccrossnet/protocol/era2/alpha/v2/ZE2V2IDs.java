package mccrossnet.protocol.era2.alpha.v2;

import mccrossnet.protocol.era2.alpha.v1.ZMob48IDs;
import mccrossnet.protocol.era2.alpha.v1.ZMob49IDs;

public class ZE2V2IDs extends ZE2V2SurvivingIDs implements ZMob48IDs, ZMob49IDs {
	public static final ZE2V2IDs INSTANCE = new ZE2V2IDs();
}
