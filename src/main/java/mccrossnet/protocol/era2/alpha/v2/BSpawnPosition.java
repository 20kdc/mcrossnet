package mccrossnet.protocol.era2.alpha.v2;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StandardStruct;

/**
 * Server-to-client: controls the compass.
 * However, the compass is based on entity-positions, so it ends up at a block corner.
 * Also controls the player respawn location.
 */
public class BSpawnPosition extends StandardStruct implements Packet {
	/**
	 * X
	 */
	@Order(0)
	public int x;
	
	/**
	 * Y
	 */
	@Order(1)
	public int y;

	/**
	 * Z
	 */
	@Order(2)
	public int z;

	public BSpawnPosition() {
		
	}
	public BSpawnPosition(int i, int j, int k) {
		x = i;
		y = j;
		z = k;
	}
}
