package mccrossnet.protocol.era2.alpha.v2;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Format;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.data.formats.ShortPrefixedByteArrayFormat;
import mccrossnet.nbt.NamedBinaryTag;
import mccrossnet.nbt.SNBTWriter;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StandardStruct;

/**
 * Objectively the best way of handling TE downloads and I'm really sad they ditched this.
 * On the other hand, this is also used for TE uploads. And that's weird.
 */
public class BTileEntity extends StandardStruct implements Packet {
	/**
	 * X
	 */
	@Order(0)
	public int x;
	
	/**
	 * Y (it confuses me, too)
	 */
	@Order(1)
	public short y;
	
	/**
	 * Z
	 */
	@Order(2)
	public int z;

	/**
	 * GZIP'd NBT data.
	 */
	@Order(3)
	@Format(ShortPrefixedByteArrayFormat.class)
	public byte[] nbtGZ;

	public static BTileEntity create(int x, int y, int z, Object nbt) {
		BTileEntity result = new BTileEntity();
		result.x = x;
		result.y = (short) y;
		result.z = z;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			GZIPOutputStream zos = new GZIPOutputStream(baos);
			NamedBinaryTag.write(new DataOutputStream(zos), "TileEntity", nbt);
			zos.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		result.nbtGZ = baos.toByteArray();
		return result;
	}
	
	@Override
	public String toString() {
		// An actual toString method, that's rare...
		String nbt = "(unparsable)";
		try {
			GZIPInputStream gzi = new GZIPInputStream(new ByteArrayInputStream(nbtGZ));
			Object o = NamedBinaryTag.read(new DataInputStream(gzi), true);
			StringWriter sw = new StringWriter();
			new SNBTWriter(sw, "\t").write(o);
			return sw.toString();
		} catch (Exception e2) {
			// nope
		}
		return "BTileEntity{x = " + x + ", y = " + y + ", z = " + z + ", nbt = " + nbt + "}";
	}
}
