package mccrossnet.protocol.era2.alpha.v2;

import mccrossnet.protocol.era2.alpha.v1.ZE2V1SurvivingIDs;

public class ZE2V2SurvivingIDs extends ZE2V1SurvivingIDs {
	public static final short I_COMPASS = 345;
	// This was actually added in Alpha v1.1.1, not v1.1.0
	// But it's documented as having the same protocol version number.
	// Even though it adds sneaking?
	// Very odd. Very, very odd.
	public static final short I_FISHING_ROD = 346;
}
