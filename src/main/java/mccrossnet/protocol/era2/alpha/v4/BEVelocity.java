package mccrossnet.protocol.era2.alpha.v4;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.annotations.Format;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.protocol.all.EntityPacket;
import mccrossnet.protocol.all.EntityStruct;
import mccrossnet.protocol.all.vectors.IntVec3;
import mccrossnet.protocol.all.vectors.ShortIntVec3Format;

/**
 * Sets the velocity of an entity for more accurate client prediction (read: less lag weirdness)
 */
public class BEVelocity extends EntityStruct implements Packet {
	/**
	 * Velocity (/32)
	 */
	@Order(1)
	@Format(ShortIntVec3Format.class)
	public final IntVec3 velocity = new IntVec3();
}
