package mccrossnet.protocol.era2.alpha.v4;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.annotations.Marker;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.data.annotations.Requires;
import mccrossnet.protocol.all.EntityPacket;
import mccrossnet.protocol.all.EntityStruct;
import mccrossnet.protocol.all.flags.BEClickEntityLeft;
import mccrossnet.protocol.all.markers.EntityID;

/**
 * This is weird. It's a CtoS entity packet with a second entity (ala BEAnimateItemPickup but CtoS, not StoC).
 */
public class BEClickEntity extends EntityStruct implements Packet {
	/**
	 * The target entity ID.
	 */
	@Order(1)
	@Marker(EntityID.class)
	public int targetID;
	
	/**
	 * Left-click (indicating attack).
	 * Not available until E2V5.
	 */
	@Order(2)
	@Requires(BEClickEntityLeft.class)
	public boolean leftClick;
}
