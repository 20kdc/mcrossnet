package mccrossnet.protocol.era2.alpha.v4;

import java.io.InputStream;
import java.io.OutputStream;

import mccrossnet.protocol.all.flags.BEClickEntityLeft;
import mccrossnet.protocol.era2.alpha.v3.ZE2V3Codec;
import mccrossnet.base.data.BaseFormat;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.formats.NotPresentInThisVersionFormat;

/**
 * Alpha v1.2.2 or E2V4
 * <a href="protocol.html">Protocol</a>
 */
public class ZE2V4Codec extends ZE2V3Codec {

	public ZE2V4Codec() {
		// https://wiki.vg/index.php?title=Protocol&oldid=139
		//  ...is actually TOO NEW, and includes facets of V5.
		// https://wiki.vg/index.php?title=Protocol&oldid=125 is correct.
		// This is made clear by https://wiki.vg/index.php?title=Protocol&oldid=146#Protocol_History 
		
		attachCtos(0x07, BEClickEntity.class);
		// 0x10 was already both CtoS and StoC, just ignore the 'change'
		// 0x15 is identical in format it's just being sent CtoS now
		attachStoc(0x1C, BEVelocity.class);
		attachStoc(0x27, BEAttachEntity.class);
	}
}
