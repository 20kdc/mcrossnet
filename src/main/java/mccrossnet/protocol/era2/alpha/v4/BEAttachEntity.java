package mccrossnet.protocol.era2.alpha.v4;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.annotations.Marker;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.protocol.all.EntityPacket;
import mccrossnet.protocol.all.EntityStruct;
import mccrossnet.protocol.all.markers.EntityID;

/**
 * Indicates the attachment of one entity to another.
 * That includes, say, your player, to, say, a minecart, via the AConnectPacket-supplied EID.
 * Also, spider jockeys.
 */
public class BEAttachEntity extends EntityStruct implements Packet {
	/**
	 * -1 is 'detach'. Otherwise, the vehicle to attach to.
	 */
	@Order(1)
	@Marker(EntityID.class)
	public int vehicleID;

	public BEAttachEntity() {
		
	}
	public BEAttachEntity(int i, int j) {
		entityID = i;
		vehicleID = j;
	}
}
