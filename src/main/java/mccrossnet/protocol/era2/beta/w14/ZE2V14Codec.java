package mccrossnet.protocol.era2.beta.w14;

import java.io.InputStream;
import java.io.OutputStream;

import mccrossnet.protocol.era2.beta.w13.ZE2V13Codec;
import mccrossnet.base.data.FormatContext;

/**
 * <a href="protocol.html">Protocol</a>
 */
public class ZE2V14Codec extends ZE2V13Codec {

	public ZE2V14Codec() {
		// https://wiki.vg/index.php?title=Protocol&oldid=517
		registry = ZE2V14IDs.INSTANCE;
		// nothing to do, apparently
	}

}
