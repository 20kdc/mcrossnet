package mccrossnet.protocol.era2.beta.w14;

import mccrossnet.protocol.era2.beta.w13.ZE2V13IDs;
import mccrossnet.protocol.era2.beta.w13.ZE2V13SurvivingIDs;

public class ZE2V14SurvivingIDs extends ZE2V13SurvivingIDs {
	// https://minecraft.gamepedia.com/Java_Edition_Beta_1.7
	public static final byte B_PISTON = 33;
	public static final byte B_PISTON_HEAD = 34;
	public static final byte B_RAIL_DETECTOR = 28;
	public static final short I_SHEARS = 359;
}
