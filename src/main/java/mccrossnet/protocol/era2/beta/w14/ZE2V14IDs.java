package mccrossnet.protocol.era2.beta.w14;

import mccrossnet.protocol.era2.alpha.v1.ZMob49IDs;
import mccrossnet.protocol.era2.beta.w13.ZE2V13IDs;

public class ZE2V14IDs extends ZE2V14SurvivingIDs implements ZMob49IDs {
	public static final ZE2V14IDs INSTANCE = new ZE2V14IDs();
}
