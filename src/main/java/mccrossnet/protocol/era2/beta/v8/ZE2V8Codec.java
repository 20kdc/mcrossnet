package mccrossnet.protocol.era2.beta.v8;

import java.io.InputStream;
import java.io.OutputStream;

import mccrossnet.protocol.all.EntityMetadata;
import mccrossnet.protocol.all.EntityMetadataFormat;
import mccrossnet.protocol.all.items.ICDItemStack;
import mccrossnet.protocol.all.items.neganull.ICDItemStackICDFormat;
import mccrossnet.protocol.all.items.neganull.ICDItemStackXICFormat;
import mccrossnet.protocol.all.markers.BESpawnItemInterior;
import mccrossnet.protocol.era2.beta.v7.BESlot;
import mccrossnet.protocol.era2.beta.v7.ZE2V7Codec;
import mccrossnet.base.data.ValueFormat;
import mccrossnet.base.data.BaseFormat;
import mccrossnet.base.data.FormatContext;

/**
 * <a href="protocol.html">Protocol</a>
 */
public class ZE2V8Codec extends ZE2V7Codec {

	public ZE2V8Codec() {
		// https://wiki.vg/index.php?title=Protocol&oldid=326
		// OH WELL IT DOESN'T EXIST NOW
		registry = ZE2V8IDs.INSTANCE;
		
		// 0x05 change actually occurred in V7
		// 0x0F changes handled with .before
		attachCtos(0x13, BEPlayerControlButton.class);
		// 0x15 is handled through getDefaultFormat
		// 0x18 is handled through getDefaultFormat
		attachStoc(0x19, BESpawnPainting.class);
		attachStoc(0x28, BEMetadata.class);
		attachStoc(0x36, BBlockDoesAThing.class);
		// 0x66 & 0x67 handled with .before
	}

	@Override
	public <T> BaseFormat<T> getDefaultFormat(Class<T> fieldType) {
		if (fieldType == ICDItemStack.class)
			return (BaseFormat<T>) ICDItemStackICDFormat.INSTANCE;
		if (fieldType == BESpawnItemInterior.class)
			return (BaseFormat<T>) ICDItemStackICDFormat.INSTANCE;
		if (fieldType == EntityMetadata[].class)
			return (BaseFormat<T>) EntityMetadataFormat.INSTANCE;
		return super.getDefaultFormat(fieldType);
	}
}
