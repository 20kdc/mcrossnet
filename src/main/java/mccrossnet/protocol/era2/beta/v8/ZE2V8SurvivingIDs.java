package mccrossnet.protocol.era2.beta.v8;

import mccrossnet.protocol.era2.alpha.v5.ZE2V5IDs;
import mccrossnet.protocol.era2.beta.v7.ZE2V7IDs;
import mccrossnet.protocol.era2.beta.v7.ZE2V7SurvivingIDs;

public class ZE2V8SurvivingIDs extends ZE2V7SurvivingIDs {
	// https://minecraft.gamepedia.com/Java_Edition_Beta_1.2
	public static final byte B_LAPIS_ORE = 21;
	public static final byte B_LAPIS_BLOCK = 22;
	public static final byte B_DISPENSER = 23;
	public static final byte B_SANDSTONE = 24;
	public static final byte B_NOTE_BLOCK = 25;
	public static final byte B_CAKE = 92;
	// this is the first instance of durability values being used as a form of metadata
	// charcoal is added via metadata as well
	public static final short I_DYE = 351;
	// the different types of logs/leaves are metadata
	public static final short I_BONES = 352;
	public static final short I_SUGAR = 353;
	public static final short I_CAKE = 354;

	public static final byte M_SQUID = 94;
}
