package mccrossnet.protocol.era2.beta.v8;

import mccrossnet.protocol.era2.alpha.v1.ZMob49IDs;
import mccrossnet.protocol.era2.alpha.v5.ZE2V5IDs;
import mccrossnet.protocol.era2.beta.v7.ZE2V7IDs;

public class ZE2V8IDs extends ZE2V8SurvivingIDs implements ZMob49IDs {
	public static final ZE2V8IDs INSTANCE = new ZE2V8IDs();
}
