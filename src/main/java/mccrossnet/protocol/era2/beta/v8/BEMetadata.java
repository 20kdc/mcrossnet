package mccrossnet.protocol.era2.beta.v8;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.protocol.all.EntityMetadata;
import mccrossnet.protocol.all.EntityPacket;
import mccrossnet.protocol.all.EntityStruct;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.annotations.Order;

/**
 * Sent by the server to set or update parts of an entity's metadata.
 */
public class BEMetadata extends EntityStruct implements Packet {
	/**
	 * The metadata to set.
	 */
	@Order(1)
	public EntityMetadata[] metadata = EntityMetadata.NONE;
}
