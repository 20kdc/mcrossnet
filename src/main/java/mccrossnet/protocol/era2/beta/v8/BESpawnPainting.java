package mccrossnet.protocol.era2.beta.v8;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.protocol.all.EntityPacket;
import mccrossnet.protocol.all.EntityStruct;
import mccrossnet.protocol.all.Face;
import mccrossnet.protocol.all.FacePaintingFormat;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.annotations.Format;
import mccrossnet.base.data.annotations.Order;

/**
 * Places a painting! ... ...replica! ... ran off of a really bad dot-matrix!
 */
public class BESpawnPainting extends EntityStruct implements Packet {
	/**
	 * The name of the painting.
	 */
	@Order(1)
	public String name;
	
	/**
	 * X (fixed???)
	 */
	@Order(2)
	public int x;
	
	/**
	 * Y (fixed???)
	 */
	@Order(3)
	public int y;
	
	/**
	 * Z (fixed???)
	 */
	@Order(4)
	public int z;
	
	/**
	 * Direction of the painting.
	 * Can't be vertical.
	 */
	@Order(5)
	@Format(FacePaintingFormat.class)
	public Face type;
}
