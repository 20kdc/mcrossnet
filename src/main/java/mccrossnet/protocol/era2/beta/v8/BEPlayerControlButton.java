package mccrossnet.protocol.era2.beta.v8;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.protocol.all.EntityStruct;

/**
 * This is yet another animation packet.
 * Probably CtoS-only but not willing to bet on it.
 */
public class BEPlayerControlButton extends EntityStruct implements Packet {
	/**
	 * The animation.
	 * Known values so far, and as with all of this, borrowed from wiki.vg
	 * (may or may not be altered if experience shows differently)
	 * 1: Sneak ON
	 * 2: Sneak OFF
	 * 3: Leaving bed (there's an encoding in BEAnimationA for this)
	 * 4: Sprint ON
	 * 5: Sprint OFF
	 */
	@Order(1)
	public byte animation;
}
