package mccrossnet.protocol.era2.beta.v8;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StandardStruct;

/**
 * Causes a block to do a thing.
 * This is block-dependent.
 * 
 * Here's a quick summary:
 * Pistons: Byte 1 is a boolean (pulling), Byte 2 is a different face enum of the form DUSWNE.
 * Chests: Byte 1 is 1, byte 2 is a boolean (open)
 * Note Block: Byte 1 is the instrument (harp, double-bass, snare, click, bass), Byte 2 is the note.
 * The only freedom here is that a note block can be forced to play an instrument it "isn't supposed to".
 */
public class BBlockDoesAThing extends StandardStruct implements Packet {
	/**
	 * X
	 */
	@Order(0)
	public int x;
	
	/**
	 * Y
	 */
	@Order(1)
	public short y;
	
	/**
	 * Z
	 */
	@Order(2)
	public int z;
	
	/**
	 * Thing being done: Major category
	 */
	@Order(3)
	public byte iA;
	
	/**
	 * Thing being done: Minor category
	 */
	@Order(4)
	public byte iB;
}
