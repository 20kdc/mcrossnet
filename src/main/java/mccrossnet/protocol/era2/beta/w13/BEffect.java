package mccrossnet.protocol.era2.beta.w13;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StandardStruct;

/**
 * Plays a sound or performs a particle effect via a complicated palette mechanism.
 * Notably, these do not perform any other modification to world state.
 * Despite their names.
 */
public class BEffect extends StandardStruct implements Packet {
	/**
	 * Categories. These.
	 * 1000: Some sort of click.
	 * 1001: Some other sort of click.
	 * 1002: Bow fire.
	 * 1003: Door open.
	 * 1004: 'hiss' of fire going out.
	 * 1005: Plays a record (subcategory = item ID, would assume 0 terminates)
	 * 2000: Smoke: Has it's own 9-direction enum.
	 * 2001: Block break (subcategory = block ID ; presumably this is sent after-the-fact)
	 */
	@Order(0)
	public int category;
	
	/**
	 * Block X of the source.
	 */
	@Order(1)
	public int x;
	
	/**
	 * Block Y of the source.
	 */
	@Order(2)
	public byte y;
	
	/**
	 * Block Z of the source.
	 */
	@Order(3)
	public int z;
	
	/**
	 * See the main category list for what this is.
	 */
	@Order(4)
	public int subcategory;
}
