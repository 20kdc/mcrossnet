package mccrossnet.protocol.era2.beta.w13;

import java.io.InputStream;
import java.io.OutputStream;

import mccrossnet.protocol.all.FireballDetailsFormat;
import mccrossnet.protocol.all.flags.BESpawnObjectFireballDetails;
import mccrossnet.protocol.all.flags.BRespawnDimension;
import mccrossnet.protocol.era2.beta.w11.ZE2V11Codec;
import mccrossnet.base.data.BaseFormat;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.formats.ByteFormat;
import mccrossnet.base.data.formats.NotPresentInThisVersionFormat;

/**
 * <a href="protocol.html">Protocol</a>
 */
public class ZE2V13Codec extends ZE2V11Codec implements BRespawnDimension, BESpawnObjectFireballDetails {
	public ZE2V13Codec() {
		// https://wiki.vg/index.php?title=Protocol&oldid=483
		// IT'S NOW oldid 483, NOT 482 FOR SOME REASON.
		registry = ZE2V13IDs.INSTANCE;
		
		// 0x09 change happens via getDefaultFormat
		// 0x17 change happens via getDefaultFormat
		attachStoc(0x3D, BEffect.class);
		attachStoc(0x83, BMapData.class);
	}
}
