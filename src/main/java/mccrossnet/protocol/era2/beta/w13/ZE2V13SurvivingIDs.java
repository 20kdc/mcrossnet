package mccrossnet.protocol.era2.beta.w13;

import mccrossnet.protocol.era2.beta.w11.ZE2V11IDs;
import mccrossnet.protocol.era2.beta.w11.ZE2V11SurvivingIDs;

public class ZE2V13SurvivingIDs extends ZE2V11SurvivingIDs {
	// https://minecraft.gamepedia.com/Java_Edition_Beta_1.6
	public static final byte B_TALL_GRASS = 31;
	public static final byte B_DEAD_PLANT = 32;
	public static final byte B_TRAPDOOR = 96;
	public static final short I_MAP = 358;
}
