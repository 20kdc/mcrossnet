package mccrossnet.protocol.era2.beta.w13;

import mccrossnet.protocol.era2.alpha.v1.ZMob49IDs;
import mccrossnet.protocol.era2.beta.w11.ZE2V11IDs;
import mccrossnet.protocol.era2.beta.w11.ZE2V11SurvivingIDs;

public class ZE2V13IDs extends ZE2V13SurvivingIDs implements ZMob49IDs {
	public static final ZE2V13IDs INSTANCE = new ZE2V13IDs();
}
