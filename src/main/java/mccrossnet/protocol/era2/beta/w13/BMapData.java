package mccrossnet.protocol.era2.beta.w13;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Format;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.data.formats.BytePrefixedByteArrayFormat;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StandardStruct;

/**
 * This is just weird.
 */
public class BMapData extends StandardStruct implements Packet {
	/**
	 * Item ID being modified.
	 */
	@Order(0)
	public short itemID;
	
	/**
	 * Item metadata (durability) being modified.
	 */
	@Order(1)
	public short itemDurability;
	
	/**
	 * This is item-dependent and has a max limit of 255 bytes.
	 * If you start babbling about a mage king, you've looked at it too long.
	 * Try not to do that.
	 */
	@Order(2)
	@Format(BytePrefixedByteArrayFormat.class)
	public byte[] name;
}
