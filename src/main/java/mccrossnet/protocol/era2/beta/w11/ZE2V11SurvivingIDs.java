package mccrossnet.protocol.era2.beta.w11;

import mccrossnet.protocol.era2.beta.w10.ZE2V10SurvivingIDs;

public class ZE2V11SurvivingIDs extends ZE2V10SurvivingIDs {
	// https://minecraft.gamepedia.com/Java_Edition_Beta_1.5

	public static final byte B_COBWEB = 30;
	public static final byte B_RAIL_POWERED = 27;
	public static final byte B_RAIL_DETECTOR = 28;
}
