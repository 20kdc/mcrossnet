package mccrossnet.protocol.era2.beta.w11;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.protocol.all.EntityPacket;
import mccrossnet.protocol.all.EntityStruct;

/**
 * Sent from server to client.
 * Has an entity ID for some reason.
 * Not sure if it wastes memory if not killed.
 * Thunderbolts! The newest mechanism for leaking your base location from BaseLeakTek!
 * ...also, really fancy and noise-making.
 * But most importantly they leak base locations.
 */
public class BEThunderbolt extends EntityStruct implements Packet {
	/**
	 * Supposed to be always true.
	 * Not a clue what it does.
	 */
	@Order(1)
	public boolean aThing = true;
	
	/**
	 * X
	 */
	@Order(2)
	public int x;
	
	/**
	 * Y
	 */
	@Order(3)
	public int y;
	
	/**
	 * Z
	 */
	@Order(4)
	public int z;
}
