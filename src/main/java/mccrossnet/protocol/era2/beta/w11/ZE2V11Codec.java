package mccrossnet.protocol.era2.beta.w11;

import java.io.InputStream;
import java.io.OutputStream;

import mccrossnet.protocol.all.flags.AConnectPacketPasswordRemoved;
import mccrossnet.protocol.all.flags.BClickWindowShift;
import mccrossnet.protocol.era2.beta.w10.ZE2V10Codec;
import mccrossnet.base.data.ValueFormat;
import mccrossnet.base.data.BaseFormat;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.formats.BooleanFormat;
import mccrossnet.base.data.formats.NotPresentInThisVersionFormat;
import mccrossnet.base.data.formats.WideStringFormat;

/**
 * <a href="protocol.html">Protocol</a>
 */
public class ZE2V11Codec extends ZE2V10Codec implements BClickWindowShift, AConnectPacketPasswordRemoved {
	
	public ZE2V11Codec() {
		// https://wiki.vg/index.php?title=Protocol&oldid=454
		registry = ZE2V11IDs.INSTANCE;
		// FROM THE FUTURE: wait, now it's 455, what the hell is going on in this place
		stringFormat = WideStringFormat.INSTANCE;
		
		// 0x01 changes, see getDefaultFormat
		attachStoc(0x47, BEThunderbolt.class);
		// 0x66 changes, see getDefaultFormat
		// BOpenWindow uses writeUTF/readUTF to prevent issues
		attachStoc(0xC8, BAddToStatistic.class);
	}
}
