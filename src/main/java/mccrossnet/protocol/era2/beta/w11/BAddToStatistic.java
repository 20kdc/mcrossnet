package mccrossnet.protocol.era2.beta.w11;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StandardStruct;

/**
 * Sent by server.
 * Adds to a client statistic (advancements, etc.)
 */
public class BAddToStatistic extends StandardStruct implements Packet {
	/**
	 * The ID of the <a href="ids.html#statistic">statistic</a>.
	 */
	@Order(0)
	public int statisticID;
	
	/**
	 * The amount to adjust the statistic by.
	 */
	@Order(1)
	public byte amount;
	
	public BAddToStatistic() {
		
	}
	
	public BAddToStatistic(int id, byte a) {
		statisticID = id;
		amount = (byte) a;
	}
}
