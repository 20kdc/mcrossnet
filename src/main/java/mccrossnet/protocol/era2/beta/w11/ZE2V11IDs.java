package mccrossnet.protocol.era2.beta.w11;

import mccrossnet.protocol.era2.alpha.v1.ZMob49IDs;
import mccrossnet.protocol.era2.beta.w10.ZE2V10SurvivingIDs;

public class ZE2V11IDs extends ZE2V11SurvivingIDs implements ZMob49IDs {
	public static final ZE2V11IDs INSTANCE = new ZE2V11IDs();
}
