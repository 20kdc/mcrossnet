package mccrossnet.protocol.era2.beta.w17;

import java.io.InputStream;
import java.io.OutputStream;

import mccrossnet.protocol.all.flags.TheAdventureUpdate;
import mccrossnet.protocol.all.markers.BOpenWindowTitle;
import mccrossnet.protocol.era2.beta.w14.ZE2V14Codec;
import mccrossnet.base.data.BaseFormat;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.formats.IntFormat;
import mccrossnet.base.data.formats.NotPresentInThisVersionFormat;

/**
 * <a href="protocol.html">Protocol</a>
 */
public class ZE2V17Codec extends ZE2V14Codec implements TheAdventureUpdate {
	public ZE2V17Codec() {
		// https://wiki.vg/index.php?title=Protocol&oldid=602
		registry = ZE2V17IDs.INSTANCE;
		// KEEP IN MIND, 15 and 16 (the two previous updates) were not covered
		//  because they're pre-releases

		// 0x00 handled via the magic flag
		// 0x01 also handled via the magic flag
		// 0x08 also handled via the magic flag
		// 0x09 also handled via the magic flag
		attachStoc(0x1A, BESpawnShinies.class);
		attachStoc(0x29, BEAddEffect.class);
		attachStoc(0x2A, BEDelEffect.class);
		attachStoc(0x2B, BPlayerExperience.class);
		// 0x64 handled via BOpenWindowTitle
		attach(0x6B, BCreativeSetInventorySlot.class);
		attachStoc(0xC9, BWhoListEntry.class);
		attachCtos(0xFE, AServerListPing.class);
	}
	
	@Override
	public <T> BaseFormat<T> getDefaultFormat(Class<T> fieldType) {
		if (fieldType == BOpenWindowTitle.class)
			return (BaseFormat<T>) stringFormat;
		return super.getDefaultFormat(fieldType);
	}
}
