package mccrossnet.protocol.era2.beta.w17;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.annotations.Format;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.protocol.all.EntityStruct;
import mccrossnet.protocol.all.vectors.IntVec3;
import mccrossnet.protocol.all.vectors.IntVec3Format;

/**
 * Spawns shinies.
 */
public class BESpawnShinies extends EntityStruct implements Packet {
	/**
	 * Position of the shinies (/32)
	 */
	@Order(1)
	@Format(IntVec3Format.class)
	public final IntVec3 position = new IntVec3();
	
	/**
	 * Amount of shinies. Defaults to 1.
	 */
	@Order(2)
	public short count = 1;
}
