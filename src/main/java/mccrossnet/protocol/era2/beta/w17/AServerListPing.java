package mccrossnet.protocol.era2.beta.w17;

import mccrossnet.protocol.all.EmptyPacket;

/**
 * States that your client is trying to perform a Beta 1.8 server list ping.
 * You will be kicked. That is on purpose; read the output and check for the section sign.
 */
public class AServerListPing extends EmptyPacket {

}
