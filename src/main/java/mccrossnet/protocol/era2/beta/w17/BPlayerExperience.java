package mccrossnet.protocol.era2.beta.w17;

import java.io.DataInputStream;
import java.io.IOException;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StandardStruct;
import mccrossnet.base.data.annotations.Order;

/**
 * Experience point stuff!
 * The levelling system is: levelXP = (targetLevel * 10)
 * Not sure how to get a form for total XP that doesn't imply iteration or recursion.
 * But if that's okay, see getTotalXPForLevel.
 */
public class BPlayerExperience extends StandardStruct implements Packet {
	/*
	 * Current XP within the level. 
	 */
	@Order(0)
	public byte currentXP;
	
	/*
	 * The current level.
	 */
	@Order(1)
	public byte currentLevel;
	
	/*
	 * Total amount of XP so far.
	 */
	@Order(2)
	public short totalXP;
	
	/**
	 * Gets the total XP for a given level.
	 * @param level
	 * @return The resulting total XP.
	 */
	public static int getTotalXPForLevel(int level) {
		int total = 0;
		while (level > 0) {
			total += level * 10;
			level--;
		}
		return total;
	}
}
