package mccrossnet.protocol.era2.beta.w17;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.StandardStruct;
import mccrossnet.base.data.annotations.Order;

/**
 * Sent from server to client to control the client's "who list".
 */
public class BWhoListEntry extends StandardStruct implements Packet {
	/**
	 * The username to add/remove.
	 */
	@Order(0)
	public String username = "Player";
	
	/**
	 * If this is an "add" operation (true) or a "remove" operation (false).
	 * Defaults to true.
	 */
	@Order(1)
	public boolean online = true;
	
	/**
	 * The displayed ping.
	 */
	@Order(2)
	public short ping;
}
