package mccrossnet.protocol.era2.beta.w17;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.protocol.all.EntityStruct;

/**
 * You can tell this is just being sketched into place to make the serialization work, can't you
 */
public class BEAddEffect extends EntityStruct implements Packet {
	/**
	 * ID of the effect.
	 */
	@Order(1)
	public byte effectID;

	/**
	 * Effect power
	 */
	@Order(2)
	public byte power;

	/**
	 * Effect time
	 */
	@Order(3)
	public short time;
}
