package mccrossnet.protocol.era2.beta.w17;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.StandardStruct;
import mccrossnet.base.data.annotations.Format;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.protocol.all.items.ICDItemStack;
import mccrossnet.protocol.all.items.neganull.ICDItemStack3ShortFormat;

/**
 * Why they didn't use the packet that already exists for setting an inventory slot,
 *  nobody knows.
 */
public class BCreativeSetInventorySlot extends StandardStruct implements Packet {
	/**
	 * Slot
	 */
	@Order(0)
	public short slot;

	/**
	 * Content
	 */
	@Order(1)
	@Format(ICDItemStack3ShortFormat.class)
	public ICDItemStack item;
}
