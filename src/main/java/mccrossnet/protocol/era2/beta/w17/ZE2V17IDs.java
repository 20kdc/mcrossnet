package mccrossnet.protocol.era2.beta.w17;

import mccrossnet.protocol.era2.beta.w14.ZE2V14IDs;
import mccrossnet.protocol.era2.beta.w14.ZE2V14SurvivingIDs;

public class ZE2V17IDs extends ZE2V14SurvivingIDs {
	public static final ZE2V17IDs INSTANCE = new ZE2V17IDs();
	// https://minecraft.gamepedia.com/Java_Edition_Beta_1.8
	public static final byte B_INFESTED = 97;
	public static final byte B_STONE_BRICK = 98;
	public static final byte B_MUSHROOM_BROWN = 99;
	public static final byte B_MUSHROOM_RED = 100;
	public static final byte B_IRON_BARS = 101;
	public static final byte B_GLASS_PANE = 102;
	public static final byte B_MELON_BLOCK = 103;
	public static final byte B_PUMPKIN_STEM = 104;
	public static final byte B_MELON_STEM = 105;
	public static final byte B_VINES = 106;
	public static final byte B_FENCE_GATE = 107;
	public static final byte B_BRICK_STAIRS = 108;
	public static final byte B_STONE_BRICK_STAIRS = 109;
	
	public static final short I_MELON_SLICE = 360;
	public static final short I_PUMPKIN_SEEDS = 361;
	public static final short I_MELON_SEEDS = 362;
	public static final short I_STEAK = 363;
	public static final short I_STEAK_COOKED = 364;
	public static final short I_CHICKEN = 365;
	public static final short I_CHICKEN_COOKED = 366;
	public static final short I_ROTTEN_FLESH = 367;
	public static final short I_ENDER_PEARL = 368;
	
	public static final byte M_ENDERMAN = 58;
	public static final byte M_CAVE_SPIDER = 59;
	public static final byte M_SILVERFISH = 60;
}
