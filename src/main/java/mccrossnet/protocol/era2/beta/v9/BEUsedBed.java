package mccrossnet.protocol.era2.beta.v9;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.protocol.all.EntityPacket;
import mccrossnet.protocol.all.EntityStruct;

/**
 * Used when an entity uses a bed.
 */
public class BEUsedBed extends EntityStruct implements Packet {
	/**
	 * Stuff and things. Might be a flag.
	 * Defaults to 0.
	 */
	@Order(1)
	public byte unknown;
	
	/**
	 * Bed X
	 */
	@Order(2)
	public int x;
	
	/**
	 * Bed Y
	 */
	@Order(3)
	public byte y;
	
	/**
	 * Bed Z
	 */
	@Order(4)
	public int z;
}
