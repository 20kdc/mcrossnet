package mccrossnet.protocol.era2.beta.v9;

import java.io.InputStream;
import java.io.OutputStream;

import mccrossnet.protocol.era2.beta.v8.ZE2V8Codec;
import mccrossnet.base.data.FormatContext;

/**
 * <a href="protocol.html">Protocol</a>
 */
public class ZE2V9Codec extends ZE2V8Codec {

	public ZE2V9Codec() {
		// https://wiki.vg/index.php?title=Protocol&oldid=368
		// now it's something like https://wiki.vg/index.php?title=Protocol&oldid=367
		// ????
		registry = ZE2V9IDs.INSTANCE;
		attachStoc(0x11, BEUsedBed.class);
		// The 0x1B packet is supposedly a non-thing, so it's NYI for now
		// if this causes issues, work out what it does first
	}

}
