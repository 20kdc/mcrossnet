package mccrossnet.protocol.era2.beta.v9;

import mccrossnet.protocol.era2.beta.v8.ZE2V8IDs;
import mccrossnet.protocol.era2.beta.v8.ZE2V8SurvivingIDs;

public class ZE2V9SurvivingIDs extends ZE2V8SurvivingIDs {
	// https://minecraft.gamepedia.com/Java_Edition_Beta_1.3
	public static final byte B_BED = 26;
	public static final byte B_REPEATER_OFF = 93;
	public static final byte B_REPEATER_ON = 94;
	
	public static final short I_BED = 355;
	public static final short I_REPEATER = 356;
}
