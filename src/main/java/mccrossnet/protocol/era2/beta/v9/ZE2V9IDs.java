package mccrossnet.protocol.era2.beta.v9;

import mccrossnet.protocol.era2.alpha.v1.ZMob49IDs;
import mccrossnet.protocol.era2.beta.v8.ZE2V8IDs;
import mccrossnet.protocol.era2.beta.v8.ZE2V8SurvivingIDs;

public class ZE2V9IDs extends ZE2V9SurvivingIDs implements ZMob49IDs {
	public static final ZE2V9IDs INSTANCE = new ZE2V9IDs();
}
