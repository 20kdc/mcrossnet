package mccrossnet.protocol.era2.beta.v7;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StandardStruct;

/**
 * Updates the text of a sign.
 * Both CtoS (when the editor closes) and StoC (any reason whatsoever).
 */
public class BUpdateSign extends StandardStruct implements Packet {
	/**
	 * X
	 */
	@Order(0)
	public int x;
	
	/**
	 * Y
	 */
	@Order(1)
	public short y;
	
	/**
	 * Z
	 */
	@Order(2)
	public int z;
	
	/**
	 * T0
	 */
	@Order(3)
	public String text0 = "";
	
	/**
	 * T1
	 */
	@Order(4)
	public String text1 = "";
	
	/**
	 * T2
	 */
	@Order(5)
	public String text2 = "";
	
	/**
	 * T3
	 */
	@Order(6)
	public String text3 = "";
}
