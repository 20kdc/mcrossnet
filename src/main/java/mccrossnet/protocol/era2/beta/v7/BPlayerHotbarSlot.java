package mccrossnet.protocol.era2.beta.v7;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StandardStruct;

/**
 * Sent by the client to change their selected hotbar slot.
 */
public class BPlayerHotbarSlot extends StandardStruct implements Packet {
	/**
	 * The slot number on the hotbar.
	 */
	@Order(0)
	public short slot;
	
	@Override
	public void readContent(FormatContext ver, DataInputStream in) throws IOException {
		slot = in.readShort();
	}

	@Override
	public void writeContent(FormatContext ver, DataOutputStream out) throws IOException {
		out.writeShort(slot);
	}

}
