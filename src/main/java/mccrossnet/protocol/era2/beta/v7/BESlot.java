package mccrossnet.protocol.era2.beta.v7;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.annotations.Format;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.protocol.all.EntityPacket;
import mccrossnet.protocol.all.EntityStruct;
import mccrossnet.protocol.all.items.ICDItemStack;
import mccrossnet.protocol.all.items.zeronull.ICDItemStackXID0NFormat;

/**
 * Equips an entity with something in a given slot.
 * It's important to note that BEHeldItem got nuked in this version (E2V7).
 * This is the StoC replacement, while BPlayerHotbarSlot replaces the CtoS side.
 */
public class BESlot extends EntityStruct implements Packet {
	/**
	 * The slot on the entity.
	 * On players, at least:
	 * slot 0: their held item.
	 * slots 1 - 4: armour
	 */
	@Order(1)
	public short slot;
	
	/**
	 * The item ID and durability.
	 */
	@Order(2)
	@Format(ICDItemStackXID0NFormat.class)
	public ICDItemStack item;
	
	public BESlot() {
		
	}
	public BESlot(int eid, short slot, ICDItemStack item) {
		entityID = eid;
		this.slot = slot;
		this.item = item;
	}
}
