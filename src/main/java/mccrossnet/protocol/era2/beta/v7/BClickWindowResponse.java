package mccrossnet.protocol.era2.beta.v7;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.protocol.all.WindowStruct;

/**
 * The server response to a client BClickWindow.
 * Also the client response to this packet (for some reason).
 */
public class BClickWindowResponse extends WindowStruct implements Packet {
	/**
	 * The action ID from BClickWindow.
	 */
	@Order(1)
	public short actionID;
	/**
	 * If the action was successfully performed or not.
	 */
	@Order(2)
	public boolean accepted;
}
