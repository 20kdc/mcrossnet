package mccrossnet.protocol.era2.beta.v7;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.protocol.all.WindowStruct;

/**
 * Indicates the progress of furnaces.
 */
public class BWindowProgress extends WindowStruct implements Packet {
	/**
	 * The "bar ID" (that is, what operation has progress occurring for it)
	 * For furnaces, 0 is the smelting progress, while 1 is the remaining fuel. 
	 */
	@Order(1)
	public short barID;
	/**
	 * How much progress in arbitrary units (possibly pixels or ticks)
	 */
	@Order(2)
	public short value;
}
