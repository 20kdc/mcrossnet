package mccrossnet.protocol.era2.beta.v7;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.protocol.all.WindowStruct;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;

/**
 * Indicates that the window should be / has been closed.
 */
public class BCloseWindow extends WindowStruct implements Packet {
	public BCloseWindow() {
	}
	public BCloseWindow(byte wid) {
		windowID = wid;
	}
}
