package mccrossnet.protocol.era2.beta.v7;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.protocol.all.WindowStruct;
import mccrossnet.protocol.all.items.ShortPrefixedICDItemStackICDArrayFormat;
import mccrossnet.protocol.all.items.ICDItemStack;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.annotations.Format;
import mccrossnet.base.data.annotations.Order;

/**
 * Sets an entire window's worth of slots.
 */
public class BSetWindowSlots extends WindowStruct implements Packet {
	/**
	 * The contents.
	 * Needs to be the size of the window.
	 * Empty slots are null.
	 */
	@Order(1)
	@Format(ShortPrefixedICDItemStackICDArrayFormat.class)
	public ICDItemStack[] content;
	
	public BSetWindowSlots() {
		
	}
	
	public BSetWindowSlots(byte wid, ICDItemStack[] items) {
		windowID = wid;
		content = items;
	}
}
