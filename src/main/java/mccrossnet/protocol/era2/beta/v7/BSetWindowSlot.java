package mccrossnet.protocol.era2.beta.v7;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.protocol.all.WindowStruct;
import mccrossnet.protocol.all.items.ICDItemStack;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.annotations.Order;

/**
 * Sets the item in a window slot.
 * Obviously, StoC.
 */
public class BSetWindowSlot extends WindowStruct implements Packet {
	/**
	 * The target slot.
	 * Cursor slot is window -999, slot -1.
	 */
	@Order(1)
	public short slot;
	
	/**
	 * The target item.
	 */
	@Order(2)
	public ICDItemStack item;

	public BSetWindowSlot() {
	}

	public BSetWindowSlot(byte wid, short s, ICDItemStack i) {
		this.windowID = wid;
		this.slot = s;
		this.item = i;
	}
}
