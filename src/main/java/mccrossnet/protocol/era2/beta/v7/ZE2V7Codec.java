package mccrossnet.protocol.era2.beta.v7;

import java.io.InputStream;
import java.io.OutputStream;

import mccrossnet.protocol.all.flags.BClickWindowShift;
import mccrossnet.protocol.all.flags.BPlayerPlacesRelocateItem;
import mccrossnet.protocol.all.items.ICDItemStack;
import mccrossnet.protocol.all.items.neganull.ICDItemStackXIFormat;
import mccrossnet.protocol.all.markers.BOpenWindowTitle;
import mccrossnet.protocol.all.markers.BPlayerHealthValue;
import mccrossnet.protocol.all.markers.BPlayerPlacesItem;
import mccrossnet.protocol.era2.alpha.v2.BPlayerInventory;
import mccrossnet.protocol.era2.alpha.v6.ZE2V6Codec;
import mccrossnet.base.data.ValueFormat;
import mccrossnet.base.data.BaseFormat;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.formats.ByteAsShortFormat;
import mccrossnet.base.data.formats.NotPresentInThisVersionFormat;
import mccrossnet.base.data.formats.ShortFormat;
import mccrossnet.base.data.formats.UTFStringFormat;

/**
 * <a href="protocol.html">Protocol</a>
 */
public class ZE2V7Codec extends ZE2V6Codec implements BPlayerPlacesRelocateItem {

	public ZE2V7Codec() {
		// https://wiki.vg/index.php?title=Protocol&oldid=206
		
		// The original 0x05, BPlayerInventory, got moved to 0x69 with some semantic alterations
		detach(0x05);
		attachStoc(0x05, BESlot.class);
		
		// 0x08 change is handled using a .before check
		// 0x0F changed in an undocumented manner
		// It's been handled through getDefaultFormat
		// 0x10 change is SORTA undocumented; it's not an update to CtoS,
		//  it's a nuke and replacement since StoC got replaced via the 0x05 packet
		detach(0x10);
		attachCtos(0x10, BPlayerHotbarSlot.class);
		detachStoc(0x11);
		// 0x15 (spawn item) stops being CtoS here.
		// It's replaced with the weird dig packet
		detachCtos(0x15);
		detach(0x3B);
		attachStoc(0x64, BOpenWindow.class);
		attach(0x65, BCloseWindow.class);
		attachCtos(0x66, BClickWindow.class);
		attachStoc(0x67, BSetWindowSlot.class);
		attachStoc(0x68, BSetWindowSlots.class);
		attachStoc(0x69, BWindowProgress.class);
		attach(0x6A, BClickWindowResponse.class);
		attach(0x82, BUpdateSign.class);
	}

	@Override
	public <T> BaseFormat<T> getDefaultFormat(Class<T> fieldType) {
		if (fieldType == BOpenWindowTitle.class)
			return (BaseFormat<T>) UTFStringFormat.INSTANCE;
		if (fieldType == BPlayerPlacesItem.class)
			return (BaseFormat<T>) getDefaultFormat(ICDItemStack.class);
		if (fieldType == BPlayerHealthValue.class)
			return (BaseFormat<T>) ShortFormat.INSTANCE;
		return super.getDefaultFormat(fieldType);
	}

}
