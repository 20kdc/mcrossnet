package mccrossnet.protocol.era2.beta.v7;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Marker;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.data.annotations.Requires;
import mccrossnet.base.core.Packet;
import mccrossnet.protocol.all.WindowStruct;
import mccrossnet.protocol.all.flags.BClickWindowShift;
import mccrossnet.protocol.all.items.ICDItemStack;
import mccrossnet.protocol.era2.alpha.v4.BEClickEntity;
import mccrossnet.base.data.FormatContext;

/**
 * Used when the client performs some action with an inventory.
 * The BClickWindow/BClickWindowResponse pairing is important for multi-user situations.
 * This probably has something to do with why the item in the slot is provided.
 * As verification that what's being picked up is actually what the client thinks is being picked up.
 */
public class BClickWindow extends WindowStruct implements Packet {
	/**
	 * Indicates the window slot being clicked.
	 * -999 is 'drop item'.
	 */
	@Order(1)
	public short slot;
	
	/**
	 * Indicates if this is a right-click.
	 */
	@Order(2)
	public boolean rightClick;
	
	/**
	 * A request/response ID for tracking by the client.
	 */
	@Order(3)
	public short actionID;
	
	/**
	 * Indicates shift-click was used.
	 * Has special behavior on earlier versions (see marker class)
	 */
	@Order(4)
	@Requires(BClickWindowShift.class)
	public boolean shift;
	
	/**
	 * The item in the slot (or null for no item).
	 */
	@Order(5)
	public ICDItemStack itemInSlot;
}
