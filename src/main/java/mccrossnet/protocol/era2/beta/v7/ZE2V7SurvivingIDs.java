
package mccrossnet.protocol.era2.beta.v7;

import mccrossnet.protocol.era2.alpha.v5.ZE2V5IDs;
import mccrossnet.protocol.era2.alpha.v5.ZE2V5SurvivingIDs;

public class ZE2V7SurvivingIDs extends ZE2V5SurvivingIDs {
	// This covers the windows in E2V7.
	public static final byte W_CHEST = 0;
	public static final byte W_INVENTORY = 1;
	public static final byte W_FURNACE = 2;
}
