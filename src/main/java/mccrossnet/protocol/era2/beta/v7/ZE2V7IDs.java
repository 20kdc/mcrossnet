package mccrossnet.protocol.era2.beta.v7;

import mccrossnet.protocol.era2.alpha.v1.ZMob48IDs;
import mccrossnet.protocol.era2.alpha.v1.ZMob49IDs;
import mccrossnet.protocol.era2.alpha.v5.ZE2V5IDs;

public class ZE2V7IDs extends ZE2V7SurvivingIDs implements ZMob48IDs, ZMob49IDs {
	public static final ZE2V7IDs INSTANCE = new ZE2V7IDs();
}
