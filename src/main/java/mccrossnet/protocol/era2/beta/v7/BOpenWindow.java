package mccrossnet.protocol.era2.beta.v7;

import java.io.DataInputStream;
import java.io.IOException;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.annotations.Format;
import mccrossnet.base.data.annotations.Marker;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.data.formats.UTFStringFormat;
import mccrossnet.protocol.all.WindowStruct;
import mccrossnet.protocol.all.markers.BOpenWindowTitle;

/**
 * Sent StoC to open a window on the client.
 */
public class BOpenWindow extends WindowStruct implements Packet {
	/**
	 * The type of window to open.
	 * See <a href="ids.html#window">the 'window' set of IDs for valid types</a>.
	 */
	@Order(1)
	public byte type;
	
	/**
	 * "Window name". (It's weird.)
	 * NOTE: This was not affected by the E2V11 update
	 * Need to work out when it WAS affected
	 * Importantly, this is supposedly intended to be readable text, where it applies.
	 */
	@Order(2)
	@Marker(BOpenWindowTitle.class)
	public String title = "Chest";
	
	/**
	 * Amount of slots in the window.
	 */
	@Order(3)
	public byte slots;

}
