package mccrossnet.protocol.era2.beta.w10;

import mccrossnet.protocol.era2.alpha.v1.ZMob49IDs;

public class ZE2V10IDs extends ZE2V10SurvivingIDs implements ZMob49IDs {
	public static final ZE2V10IDs INSTANCE = new ZE2V10IDs();
	// https://minecraft.gamepedia.com/Java_Edition_Beta_1.4
	// All the important parts of this are gutted very quickly.
	// And things go totally off the rails by release 1.3.1.
	// So let's not pretend this is surviving.
	public static final byte B_LOCKED_CHEST = 95;
}
