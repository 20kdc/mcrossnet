package mccrossnet.protocol.era2.beta.w10;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.data.annotations.Requires;
import mccrossnet.protocol.all.flags.TheAdventureUpdate;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StandardStruct;

/**
 * Sent by server to indicate something happened in the world.
 */
public class BWorldStatusUpdate extends StandardStruct implements Packet {
	/**
	 * 0: unable to enter bed
	 * 1: began raining
	 * 2: stopped raining
	 * 3: game mode has been updated
	 */
	@Order(0)
	public byte statusUpdate;
	
	/**
	 * b1.8 adds this, it's used in status 3
	 */
	@Order(1)
	@Requires(TheAdventureUpdate.class)
	public byte gameMode;
	
	public BWorldStatusUpdate() {
		
	}
	public BWorldStatusUpdate(byte b) {
		statusUpdate = b;
	}
}
