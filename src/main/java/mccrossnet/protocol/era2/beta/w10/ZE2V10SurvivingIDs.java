package mccrossnet.protocol.era2.beta.w10;

import mccrossnet.protocol.era2.beta.v9.ZE2V9IDs;
import mccrossnet.protocol.era2.beta.v9.ZE2V9SurvivingIDs;

public class ZE2V10SurvivingIDs extends ZE2V9SurvivingIDs {
	// https://minecraft.gamepedia.com/Java_Edition_Beta_1.4
	public static final short I_COOKIE = 357;
	
	public static final byte M_WOLF = 95;
}
