package mccrossnet.protocol.era2.beta.w10;

import java.io.InputStream;
import java.io.OutputStream;

import mccrossnet.protocol.era2.beta.v9.ZE2V9Codec;
import mccrossnet.protocol.era2.beta.v9.ZE2V9IDs;
import mccrossnet.base.data.FormatContext;

/**
 * <a href="protocol.html">Protocol</a>
 */
public class ZE2V10Codec extends ZE2V9Codec {

	public ZE2V10Codec() {
		// https://wiki.vg/index.php?title=Protocol&oldid=374
		registry = ZE2V10IDs.INSTANCE;
		attachStoc(0x46, BWorldStatusUpdate.class);
	}
	
}
