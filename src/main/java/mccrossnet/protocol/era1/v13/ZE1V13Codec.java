package mccrossnet.protocol.era1.v13;

import java.io.InputStream;
import java.io.OutputStream;

import mccrossnet.base.data.ValueFormat;
import mccrossnet.protocol.all.ChatText;
import mccrossnet.protocol.all.DisconnectText;
import mccrossnet.protocol.all.EntityMetadata;
import mccrossnet.protocol.all.Face;
import mccrossnet.protocol.all.FaceFormat;
import mccrossnet.protocol.all.PingKeepAlive;
import mccrossnet.protocol.all.flags.AConnectPacketPasswordRemoved;
import mccrossnet.protocol.all.flags.AConnectPacketSeedAndDimension;
import mccrossnet.protocol.all.flags.BESpawnObjectFireballDetails;
import mccrossnet.protocol.all.flags.BESpawnPlayerCurrentItem;
import mccrossnet.protocol.all.flags.BSetBlockMeta;
import mccrossnet.protocol.all.items.ICDItemStack;
import mccrossnet.protocol.all.items.neganull.ICDItemStackICByteDFormat;
import mccrossnet.protocol.all.items.neganull.ICDItemStackXICFormat;
import mccrossnet.protocol.all.items.neganull.ICDItemStackXIFormat;
import mccrossnet.protocol.all.markers.BESpawnItemInterior;
import mccrossnet.protocol.all.markers.BETeleportPosition;
import mccrossnet.protocol.all.markers.BPlayerPlacesItem;
import mccrossnet.protocol.all.markers.BSetBlockHCoordinate;
import mccrossnet.protocol.all.markers.BSetBlockVCoordinate;
import mccrossnet.protocol.all.markers.EntityID;
import mccrossnet.protocol.all.vectors.IntVec3Format;
import mccrossnet.protocol.all.vectors.ShortIntVec3Format;
import mccrossnet.protocol.era0.v7.BEDelete;
import mccrossnet.protocol.era0.v7.BELook;
import mccrossnet.protocol.era0.v7.BELookRelativeMove;
import mccrossnet.protocol.era0.v7.BERelativeMove;
import mccrossnet.protocol.era0.v7.BESpawnPlayer;
import mccrossnet.protocol.era0.v7.BETeleport;
import mccrossnet.protocol.era0.v7.BSetBlock;
import mccrossnet.protocol.era2.alpha.v1.BESpawnMob;
import mccrossnet.base.data.BaseFormat;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.formats.ByteAsIntFormat;
import mccrossnet.base.data.formats.ByteAsShortFormat;
import mccrossnet.base.data.formats.ByteFormat;
import mccrossnet.base.data.formats.IntFormat;
import mccrossnet.base.data.formats.NotPresentInThisVersionFormat;
import mccrossnet.base.data.formats.PaddedStringFormat;
import mccrossnet.base.data.formats.ShortFormat;
import mccrossnet.base.data.formats.UTFStringFormat;
import mccrossnet.base.netarch.hub.HubProtocol;
import mccrossnet.base.netarch.hub.HubSchematic;

/**
 * Alpha v1.0.15 or E1V13
 * 
 * Connection flow:
 * 
 * 1. Client sends 01 AConnectPacketCtos.
 * 2. Server sends 01 AConnectPacketStoc.
 * 3. Client immediately sends an 0D BPlayerStateCtos packet.
 *    It then endlessly sends 0A BPlayerOnGround packets.
 *    All of this should be ignored.
 * 4. In this state, the client shows "Downloading terrain...",
 *     but is technically 'initialized' (can receive map chunk data, for example).
 *    It only enters the game upon receiving a 0D BPlayerStateStoc packet.
 *    It is only at this time that it will send 0B/0C/etc. packets.
 *
 * <a href="protocol.html">Protocol</a>
 */
public class ZE1V13Codec extends HubProtocol implements BSetBlockMeta, BESpawnPlayerCurrentItem {

	public ZE1V13Codec() {
		super(UTFStringFormat.INSTANCE, ZE1V13IDs.INSTANCE);
		// https://wiki.vg/index.php?title=Protocol&oldid=110
		//  IS TOO NEW
		//  but still a valuable reference; thank you...!
		
				
		attach(0x00, PingKeepAlive.class);
		attachCtos(0x01, AConnectPacketCtos.class);
		attachStoc(0x01, AConnectPacketStoc.class);
		// 0x02 doesn't exist yet (E1V14)
		attach(0x03, ChatText.class);
		// 0x04 doesn't exist yet (E2V1)
		// 0x05 doesn't exist yet (E2V2)
		// 0x06 doesn't exist yet (E2V2)
		attach(0x0A, BPlayerOnGround.class);
		attach(0x0B, BPlayerPosition.class);
		attach(0x0C, BPlayerLook.class);
		attachCtos(0x0D, BPlayerStateCtos.class);
		attachStoc(0x0D, BPlayerStateStoc.class);
		attachCtos(0x0E, BPlayerDigsBlock.class);
		attachCtos(0x0F, BPlayerPlaces.class);
		// This packet is ALSO Ctos and Stoc.
		attach(0x10, BEHeldItem.class);
		attachStoc(0x11, BGiveItem.class);
		// It's important to note that in this protocol at least,
		//  this packet is both Ctos and Stoc, not just Stoc.
		attach(0x12, BEPlayerAction.class);
		attachStoc(0x14, BESpawnPlayer.class);
		// This is both Ctos and Stoc for reasons.
		attach(0x15, BESpawnItem.class);
		attachStoc(0x16, BEAnimateItemPickup.class);
		attachStoc(0x17, BESpawnObject.class);
		// 0x18 doesn't exist yet. (E2V1)
		attachStoc(0x1D, BEDelete.class);
		attachStoc(0x1E, BEDoAbsolutelyNothingWithEntity.class);
		attachStoc(0x1F, BERelativeMove.class);
		attachStoc(0x20, BELook.class);
		attachStoc(0x21, BELookRelativeMove.class);
		attachStoc(0x22, BETeleport.class);
		attachStoc(0x32, BChunkControl.class);
		attachStoc(0x33, BChunkData.class);
		attachStoc(0x34, BMultipleBlockChange.class);
		attachStoc(0x35, BSetBlock.class);
		// 0x3B doesn't exist yet. (E2V2)
		attach(0xFF, DisconnectText.class);
	}

	@Override
	public <T> BaseFormat<T> getDefaultFormat(Class<T> fieldType) {
		if (fieldType == EntityID.class)
			return (BaseFormat<T>) IntFormat.INSTANCE;
		if (fieldType == Face.class)
			return (BaseFormat<T>) FaceFormat.INSTANCE;
		if (fieldType == EntityMetadata[].class)
			return (BaseFormat<T>) NotPresentInThisVersionFormat.INSTANCE;
		if (fieldType == BETeleportPosition.class)
			return (BaseFormat<T>) IntVec3Format.INSTANCE;
		if (fieldType == ICDItemStack.class)
			return (BaseFormat<T>) ICDItemStackICByteDFormat.INSTANCE;
		if (fieldType == BESpawnItemInterior.class)
			return (BaseFormat<T>) ICDItemStackXICFormat.INSTANCE;
		if (fieldType == BPlayerPlacesItem.class)
			return (BaseFormat<T>) ICDItemStackXIFormat.INSTANCE;
		if (fieldType == BSetBlockHCoordinate.class)
			return (BaseFormat<T>) IntFormat.INSTANCE;
		if (fieldType == BSetBlockVCoordinate.class)
			return (BaseFormat<T>) ByteAsShortFormat.INSTANCE;
		return super.getDefaultFormat(fieldType);
	}
	
}
