package mccrossnet.protocol.era1.v13;

import mccrossnet.base.core.Packet;
import mccrossnet.protocol.all.EntityStruct;

/**
 * I'm not sure *what* this does. If it even does anything.
 * Or why anyone even knows it exists.
 * That said, I keep accidentally reviving entities.
 * I feel like that might have something to do with it.
 */
public class BEDoAbsolutelyNothingWithEntity extends EntityStruct implements Packet {
	public BEDoAbsolutelyNothingWithEntity() {
		
	}
	
	public BEDoAbsolutelyNothingWithEntity(int eid) {
		entityID = eid;
	}
}
