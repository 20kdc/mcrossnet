package mccrossnet.protocol.era1.v13;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Format;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StandardStruct;
import mccrossnet.protocol.all.items.ICDItemStack;
import mccrossnet.protocol.all.items.neganull.ICDItemStackXICDFormat;

/*
 * A remarkably specifically written packet, which is certain to be
 *  out of date by the SSI update
 */
public class BGiveItem extends StandardStruct implements Packet {
	/**
	 * The item being given.
	 */
	@Order(0)
	@Format(ICDItemStackXICDFormat.class)
	public ICDItemStack content;
	
	public BGiveItem() {
		
	}
	public BGiveItem(ICDItemStack stack) {
		content = stack;
	}
}
