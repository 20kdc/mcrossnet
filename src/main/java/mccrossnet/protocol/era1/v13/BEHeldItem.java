package mccrossnet.protocol.era1.v13;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Format;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.protocol.all.EntityPacket;
import mccrossnet.protocol.all.EntityStruct;
import mccrossnet.protocol.all.items.ICDItemStack;
import mccrossnet.protocol.all.items.zeronull.ICDItemStackXI0NFormat;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;

/**
 * This packet is both Ctos and Stoc.
 * Specifically, it's Ctos so that the client can tell the server what it's holding.
 * At least on earlier versions. This gets replaced as of E2V7.
 * See BESlot.
 */
public class BEHeldItem extends EntityStruct implements Packet {
	/**
	 * The held item ID.
	 * Only the item ID of this stack is used; this is just for convenience.
	 */
	@Order(1)
	@Format(ICDItemStackXI0NFormat.class)
	public ICDItemStack item;
	
	public BEHeldItem() {
		
	}
	
	public BEHeldItem(int eid, ICDItemStack it) {
		entityID = eid;
		item = it;
	}
}
