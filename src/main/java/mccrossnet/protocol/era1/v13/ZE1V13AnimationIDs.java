package mccrossnet.protocol.era1.v13;

public class ZE1V13AnimationIDs {
	public static final byte NONE = 0;
	public static final byte SWING = 1;
	public static final byte DAMAGE = 2;
}
