package mccrossnet.protocol.era1.v13;

import mccrossnet.base.core.Packet;
import mccrossnet.base.core.VersionSpecifyingPacket;
import mccrossnet.base.data.StandardStruct;
import mccrossnet.base.data.annotations.Avoids;
import mccrossnet.base.data.annotations.Marker;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.data.annotations.Requires;
import mccrossnet.protocol.all.flags.AConnectPacketPasswordRemoved;
import mccrossnet.protocol.all.flags.AConnectPacketSeedAndDimension;
import mccrossnet.protocol.all.flags.TheAdventureUpdate;

/**
 * In and only in E1V13, this is the initial packet from client to server for login.
 * In any later version, the handshake packets are first.
 */
public class AConnectPacketCtos extends StandardStruct implements Packet, VersionSpecifyingPacket {
	/**
	 * The protocol version of the client.
	 */
	@Order(0)
	public int protocolVersion;
	
	/**
	 * The username being logged in with.
	 */
	@Order(1)
	public String username = "Player";
	
	/**
	 * The server password (if any) being logged in with.
	 * This disappears at E2V11+
	 */
	@Order(2)
	@Avoids(AConnectPacketPasswordRemoved.class)
	public String password = "";
	
	/**
	 * Occupies the space of the 'seed' field.
	 * E2V3+
	 */
	@Order(3)
	@Requires(AConnectPacketSeedAndDimension.class)
	public long unknown1;
	
	/**
	 * E2V15+ (b1.8)
	 */
	@Order(4)
	@Requires(TheAdventureUpdate.class)
	public int unknown2;

	/**
	 * Occupies the space of the 'dimension' field.
	 * E2V3+
	 */
	@Order(5)
	@Requires(AConnectPacketSeedAndDimension.class)
	public byte unknown3;

	/**
	 * E2V15+ (b1.8)
	 */
	@Order(6)
	@Requires(TheAdventureUpdate.class)
	public byte unknown4;

	/**
	 * E2V15+ (b1.8)
	 */
	@Order(7)
	@Requires(TheAdventureUpdate.class)
	public byte unknown5;

	/**
	 * E2V15+ (b1.8)
	 */
	@Order(8)
	@Requires(TheAdventureUpdate.class)
	public byte unknown6;
	
	@Override
	public int getProtocolVersion() {
		return protocolVersion;
	}
}
