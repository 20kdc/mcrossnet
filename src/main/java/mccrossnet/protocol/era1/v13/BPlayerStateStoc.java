package mccrossnet.protocol.era1.v13;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StandardStruct;

/**
 * Teleports the player.
 * The only difference between this and BPlayerStateCtos is the order of yTop vs. y.
 */
public class BPlayerStateStoc extends StandardStruct implements Packet {
	/**
	 * X
	 */
	@Order(0)
	public double x;

	/**
	 * Y (head level)
	 */
	@Order(1)
	public double yTop;

	/**
	 * Y (ground level)
	 */
	@Order(2)
	public double y;

	/**
	 * Z
	 */
	@Order(3)
	public double z;

	/**
	 * Yaw
	 */
	@Order(4)
	public float yaw;

	/**
	 * Pitch
	 */
	@Order(5)
	public float pitch;

	/**
	 * If the player is on the ground or not
	 */
	@Order(6)
	public boolean onGround;
}
