package mccrossnet.protocol.era1.v13;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.protocol.all.Face;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StandardStruct;

/**
 * Sent by the client to update the server on it digging a block.
 */
public class BPlayerDigsBlock extends StandardStruct implements Packet {
	/**
	 * Action:
	 * 0: Started, 1: Still digging, 2: Stopped, 3: Success
	 */
	@Order(0)
	public byte action;
	
	/**
	 * X
	 */
	@Order(1)
	public int x;
	
	/**
	 * Y
	 */
	@Order(2)
	public byte y;
	
	/**
	 * Z
	 */
	@Order(3)
	public int z;
	
	/**
	 * The face being dug.
	 */
	@Order(4)
	public Face face = Face.YP;
}
