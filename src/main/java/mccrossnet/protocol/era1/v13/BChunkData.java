package mccrossnet.protocol.era1.v13;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.zip.DeflaterOutputStream;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Format;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.data.formats.IntPrefixedByteArrayFormat;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StandardStruct;

/**
 * Contains the actual data of a map chunk.
 */
public class BChunkData extends StandardStruct implements Packet {
	/**
	 * X in block coordinates (not chunk!)
	 */
	@Order(0)
	public int x;
	
	/**
	 * Y in block coordinates (not chunk!)
	 * As it doesn't make sense otherwise, defaults to 0.
	 */
	@Order(1)
	public short y = 0;
	
	/**
	 * Z in block coordinates (not chunk!)
	 */
	@Order(2)
	public int z;
	
	/**
	 * X size - 1 but should really always be 15
	 * and defaults as such
	 */
	@Order(3)
	public byte xExtent = 15;
	
	/**
	 * Y size - 1 but should really always be map height - 1
	 * and defaults to 127
	 */
	@Order(4)
	public byte yExtent = 127;

	/**
	 * Z size - 1 but should really always be 15
	 * and defaults as such
	 */
	@Order(5)
	public byte zExtent = 15;
	
	/**
	 * The full DEFLATE'd data of the chunk.
	 * The content (after decompression):
	 * 
	 * The coordinate layout is component chunks outermost, X planes, Z lines, and Y blocks.
	 * 
	 * There's 1 byte per block in this layout for block types.
	 * Then 1 nibble per block for metadata.
	 * Then 1 nibble per block for torchlight.
	 * Then 1 nibble per block for skylight.
	 * 
	 * The nibbles in each byte are *low-first*.
	 * So the lower nibble of the byte is the first (and thus actually lower) block.
	 * 
	 * Just to reiterate, these are separate components.
	 */
	@Order(6)
	@Format(IntPrefixedByteArrayFormat.class)
	public byte[] deflateData;

	/**
	 * Creates a chunk packet from the 'unpacked' form.
	 * Arguably less memory-efficient than keeping it 'correct' in the first place.
	 * But a lot easier to control.
	 * @param x Chunk X
	 * @param z Chunk Z
	 * @param blocks The block columns.
	 * @param metadata The block metadata (upper nibble ignored)
	 * @param torchlight The torchlight (upper nibble ignored)
	 * @param skylight The skylight (upper nibble ignored)
	 * @return A finished chunk packet.
	 * @throws IOException
	 */
	public static BChunkData createFromUnpacked(int x, int z, byte[] blocks, byte[] metadata, byte[] torchlight, byte[] skylight) throws IOException {
		// 128 full layers +
		// 128 half layers (64 full layers)
		//  x 3 = 192 full layers
		// = 320 full layers 
		byte[] fullData = new byte[320 * 16 * 16];
		
		System.arraycopy(blocks, 0, fullData, 0, 128 * 16 * 16);
		copyIntoNibbles(metadata, fullData, 128 * 16 * 16);
		copyIntoNibbles(torchlight, fullData, 192 * 16 * 16);
		copyIntoNibbles(skylight, fullData, 256 * 16 * 16);
		return create(x, z, fullData);
	}
	
	private static void copyIntoNibbles(byte[] src, byte[] dst, int dstIndex) {
		for (int i = 0; i < 128 * 16 * 16; i += 2)
			dst[dstIndex++] = (byte) ((src[i + 1] << 4) | (src[i] & 0x0F));
	}
	
	/**
	 * Sets up all the weird framing.
	 * @param x Chunk X
	 * @param z Chunk Z
	 * @param fullData The *pre-compression* chunk content.
	 * @return A finished packet.
	 * @throws IOException
	 */
	public static BChunkData create(int x, int z, byte[] fullData) throws IOException {
		BChunkData result = new BChunkData();
		result.x = x * 16;
		result.z = z * 16;
		result.y = 0;
		result.xExtent = 15;
		result.zExtent = 15;
		result.yExtent = 127;
		ByteArrayOutputStream def = new ByteArrayOutputStream();
		DeflaterOutputStream defos = new DeflaterOutputStream(def);
		defos.write(fullData);
		defos.close();
		result.deflateData = def.toByteArray();
		return result;
	}
}
