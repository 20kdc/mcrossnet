package mccrossnet.protocol.era1.v13;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Marker;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.protocol.all.EntityPacket;
import mccrossnet.protocol.all.EntityStruct;
import mccrossnet.protocol.all.markers.EntityID;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;

/**
 * This causes the visual 'item pickup' animation.
 * You still need to delete the entity.
 */
public class BEAnimateItemPickup extends EntityStruct implements Packet {
	/**
	 * The entity ID of the player picking up the item.
	 */
	@Order(1)
	@Marker(EntityID.class)
	public int playerID;

	public BEAnimateItemPickup() {
	}

	public BEAnimateItemPickup(int eid, int pid) {
		entityID = eid;
		playerID = pid;
	}
}
