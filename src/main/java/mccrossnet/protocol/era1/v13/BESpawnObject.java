package mccrossnet.protocol.era1.v13;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Format;
import mccrossnet.base.data.annotations.Marker;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.data.annotations.Requires;
import mccrossnet.protocol.all.EntityPacket;
import mccrossnet.protocol.all.EntityStruct;
import mccrossnet.protocol.all.FireballDetails;
import mccrossnet.protocol.all.FireballDetailsFormat;
import mccrossnet.protocol.all.flags.BESpawnObjectFireballDetails;
import mccrossnet.protocol.all.vectors.IntVec3;
import mccrossnet.protocol.all.vectors.IntVec3Format;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;

/**
 * Spawns an 'object' (a boat, for example)
 */
public class BESpawnObject extends EntityStruct implements Packet {
	/**
	 * Object type.
	 * See <a href="ids.html#object">the 'object' set of IDs for valid types</a>.
	 */
	@Order(1)
	public byte type;

	/**
	 * Position (/32)
	 */
	@Order(2)
	@Format(IntVec3Format.class)
	public final IntVec3 position = new IntVec3();
	
	/**
	 * Fireball details (as of E2V13)
	 */
	@Order(3)
	@Requires(BESpawnObjectFireballDetails.class)
	@Format(FireballDetailsFormat.class)
	public FireballDetails fireballDetails = null;
}
