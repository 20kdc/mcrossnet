package mccrossnet.protocol.era1.v13;

import mccrossnet.base.data.StandardStruct;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.annotations.Avoids;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.data.annotations.Requires;
import mccrossnet.protocol.all.flags.AConnectPacketPasswordRemoved;
import mccrossnet.protocol.all.flags.AConnectPacketSeedAndDimension;
import mccrossnet.protocol.all.flags.TheAdventureUpdate;

/**
 * This is the response packet from server to client saying "yup, you successfully logged in".
 * It mirrors the client to server packet in structure, so it has two meaningless fields.
 * 
 * It's important to note that E2V3+ presumably uses this for dimension changes.
 */
public class AConnectPacketStoc extends StandardStruct implements Packet {
	/**
	 * The player's EID.
	 */
	@Order(0)
	public int playerEID;
	
	/**
	 * Said to be meaningless.
	 */
	@Order(1)
	public String unknown1 = "";
	
	/**
	 * Said to also be meaningless.
	 * This disappears at E2V11+
	 */
	@Order(2)
	@Avoids(AConnectPacketPasswordRemoved.class)
	public String unknown2 = "";
	
	/**
	 * The world seed.
	 * E2V3+
	 */
	@Order(3)
	@Requires(AConnectPacketSeedAndDimension.class)
	public long seed;

	/**
	 * Gamemode.
	 * E2V15+ (b1.8)
	 */
	@Order(4)
	@Requires(TheAdventureUpdate.class)
	public int serverMode;

	/**
	 * The dimension the client is being moved to or is starting in.
	 * E2V3+
	 */
	@Order(5)
	@Requires(AConnectPacketSeedAndDimension.class)
	public byte dimension;

	/**
	 * The difficulty of the server.
	 * E2V15+ (b1.8)
	 */
	@Order(6)
	@Requires(TheAdventureUpdate.class)
	public byte difficulty;

	/**
	 * World Height (unsigned)
	 * E2V15+ (b1.8)
	 */
	@Order(7)
	@Requires(TheAdventureUpdate.class)
	public byte worldHeight;

	/**
	 * max players (unsigned)
	 * E2V15+ (b1.8)
	 */
	@Order(8)
	@Requires(TheAdventureUpdate.class)
	public byte maxPlayers;
}
