package mccrossnet.protocol.era1.v13;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Marker;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.data.annotations.OrderIf;
import mccrossnet.protocol.all.Face;
import mccrossnet.protocol.all.flags.BPlayerPlacesRelocateItem;
import mccrossnet.protocol.all.items.ICDItemStack;
import mccrossnet.protocol.all.markers.BPlayerPlacesItem;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StandardStruct;

/**
 * Indicates a targetted block (not the place where the user actually wants their block) and a face.
 * (0, 0, 0, YP) means the player wants a block at (0, 1, 0).
 * If everything is -1 (but particularly face), this is a special place packet.
 */
public class BPlayerPlaces extends StandardStruct implements Packet {
	/**
	 * NOTE: Item count/durability are not available before E2V7.
	 * Otherwise it's the standard ICDItemStack for that version.
	 */
	@Order(0)
	@OrderIf(flag = BPlayerPlacesRelocateItem.class, priority = 0, value = 5)
	@Marker(BPlayerPlacesItem.class)
	public ICDItemStack item;
	
	/**
	 * X
	 */
	@Order(1)
	public int x;
	
	/**
	 * Y
	 */
	@Order(2)
	public byte y;
	
	/**
	 * Z
	 */
	@Order(3)
	public int z;
	
	/**
	 * Face to offset X/Y/Z by
	 */
	@Order(4)
	public byte face;
	
	// Note that the item ends up here (order = 5) in later versions
}
