package mccrossnet.protocol.era1.v13;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StandardStruct;

/**
 * These are spammed by the client for no apparent reason.
 */
public class BPlayerOnGround extends StandardStruct implements Packet {
	/**
	 * True if the client is on the ground.
	 */
	@Order(0)
	public boolean onGround;
	
	public BPlayerOnGround() {
	}
	
	public BPlayerOnGround(boolean b) {
		onGround = b;
	}
}
