package mccrossnet.protocol.era1.v13;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.protocol.all.EntityStruct;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StandardStruct;

/**
 * This creates or deletes a chunk in client memory.
 */
public class BChunkControl extends StandardStruct implements Packet {
	/**
	 * Chunk X (x >> 4)
	 */
	@Order(0)
	public int x;
	
	/**
	 * Chunk Z (z >> 4)
	 */
	@Order(1)
	public int z;
	
	/**
	 * If the chunk should be allocated (true) or deallocated (false)
	 */
	@Order(2)
	public boolean load;
}
