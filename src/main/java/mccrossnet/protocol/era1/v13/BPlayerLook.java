package mccrossnet.protocol.era1.v13;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StandardStruct;

/**
 * Sent by the client when the player looks around without moving.
 * Can also be sent by the server (same principle as the other BPlayer-series packets)
 */
public class BPlayerLook extends StandardStruct implements Packet {
	/**
	 * Yaw
	 */
	@Order(0)
	public float yaw;
	/**
	 * Pitch
	 */
	@Order(1)
	public float pitch;
	/**
	 * True if on the ground.
	 */
	@Order(2)
	public boolean onGround; 
}
