package mccrossnet.protocol.era1.v13;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Format;
import mccrossnet.base.data.annotations.Marker;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.protocol.all.EntityPacket;
import mccrossnet.protocol.all.EntityStruct;
import mccrossnet.protocol.all.items.ICDItemStack;
import mccrossnet.protocol.all.markers.BESpawnItemInterior;
import mccrossnet.protocol.all.vectors.ByteIntVec3Format;
import mccrossnet.protocol.all.vectors.IntVec3;
import mccrossnet.protocol.all.vectors.IntVec3Format;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;

/**
 * Spawns an in-world item (a dropped one).
 * It's important to note that at least E1V13 clients send this to drop items.
 */
public class BESpawnItem extends EntityStruct implements Packet {
	/**
	 * The item itself.
	 * Before E2V8, durability is ignored entirely.
	 */
	@Order(1)
	@Marker(BESpawnItemInterior.class)
	public ICDItemStack item;
	
	/**
	 * Position (/32)
	 */
	@Order(2)
	@Format(IntVec3Format.class)
	public final IntVec3 position = new IntVec3();
	
	/**
	 * Velocity (NOTE: These are actually bytes!)
	 */
	@Order(3)
	@Format(ByteIntVec3Format.class)
	public final IntVec3 velocity = new IntVec3();
}
