package mccrossnet.protocol.era1.v13;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StandardStruct;

/**
 * A "The player hasn't looked, but has moved" update packet.
 * To be clear, this is a CtoS packet.
 * This gets a tad confusing and it might all be wrong.
 */
public class BPlayerPosition extends StandardStruct implements Packet {
	/**
	 * X
	 */
	@Order(0)
	public double x;
	
	/**
	 * Y (ground level)
	 */
	@Order(1)
	public double y;
	
	/**
	 * Y (head level)
	 */
	@Order(2)
	public double yTop;
	
	/**
	 * Z
	 */
	@Order(3)
	public double z;
	
	/**
	 * True if on the ground.
	 */
	@Order(4)
	public boolean onGround;
}
