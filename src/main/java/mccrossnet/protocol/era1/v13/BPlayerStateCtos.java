package mccrossnet.protocol.era1.v13;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StandardStruct;

/**
 * Informs the server of the player's current position/rotation/etc.
 * The only difference between this and BPlayerStateStoc is the order of y vs. yTop.
 * When in a vehicle, this acts as a vehicle control packet.
 * y and yTop are both -999, and the X/Z are set for steering the boat.
 */
public class BPlayerStateCtos extends StandardStruct implements Struct, Packet {
	/**
	 * X
	 */
	@Order(0)
	public double x;

	/**
	 * Y (ground level)
	 */
	@Order(1)
	public double y;

	/**
	 * Y (head level)
	 */
	@Order(2)
	public double yTop;

	/**
	 * Z
	 */
	@Order(3)
	public double z;

	/**
	 * Yaw
	 */
	@Order(4)
	public float yaw;

	/**
	 * Pitch
	 */
	@Order(5)
	public float pitch;

	/**
	 * If the player is on the ground or not
	 */
	@Order(6)
	public boolean onGround;
}
