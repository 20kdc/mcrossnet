package mccrossnet.protocol.era1.v13;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.protocol.all.EntityStruct;

/**
 * This is a 'packet within a packet' sort of thing that arguably didn't really need a sub-byte.
 * Both CtoS and StoC, at least at times.
 */
public class BEPlayerAction extends EntityStruct implements Packet {
	/**
	 * The animation.
	 * Known values so far, and as with all of this, borrowed from wiki.vg
	 * (may or may not be altered if experience shows differently)
	 * 0: NOP (???)
	 * 1: Swing arm (StoC & CtoS)
	 *  1 is default because of course
	 * 2: Hurt (StoC)
	 * 3: Leave bed (StoC & CtoS)
	 * 102: wiki.vg says they get this and don't know what it is
	 * 104: Sneak ON (StoC, for CtoS see BEAnimationB)
	 * 105: Sneak OFF (StoC, for CtoS see BEAnimationB)
	 */
	@Order(1)
	public byte animation = 1;
	
	public BEPlayerAction() {
		
	}
	public BEPlayerAction(int eid, byte anim) {
		entityID = eid;
		animation = anim;
	}
}
