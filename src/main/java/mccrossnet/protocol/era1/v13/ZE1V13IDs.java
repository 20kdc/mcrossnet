package mccrossnet.protocol.era1.v13;

import mccrossnet.protocol.era01.ZE0ZAlphaSurvivingIDs;

public class ZE1V13IDs extends ZE0ZAlphaSurvivingIDs {
	public static final ZE1V13IDs INSTANCE = new ZE1V13IDs();
	
	public static final byte O_BOAT = 1;
	public static final byte O_MINECART = 10;
	public static final byte O_MINECART_CHEST = 11;
	public static final byte O_MINECART_FURNACE = 12;
	// NOTE: THESE MAY HAVE TO TO BE MOVED INTO FUTURE VERSIONS AS APPLICABLE
	public static final byte O_ARROW = 60;
	public static final byte O_SNOWBALL = 61;
	public static final byte O_EGG = 62;
	public static final byte O_TNT = 50;
	public static final byte O_FALLING_BLOCK = 70;
}
