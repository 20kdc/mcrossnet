package mccrossnet.protocol.era1.v13;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import mccrossnet.base.data.Struct;
import mccrossnet.base.data.annotations.Format;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.data.formats.ShortPrefixed4ByteArrayFormat;
import mccrossnet.protocol.era0.v7.BSetBlock;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StandardStruct;

/**
 * Essentially a batch version of BSetBlock.
 * Sets a set of blocks within a chunk.
 */
public class BMultipleBlockChange extends StandardStruct implements Packet {
	/**
	 * Chunk X (x >> 4)
	 */
	@Order(0)
	public int x;
	
	/**
	 * Chunk Z (z >> 4)
	 */
	@Order(1)
	public int z;
	
	/**
	 * This is split into 3 sections. Each block adds 4 bytes in total.
	 * 
	 * The first section is coordinates, two bytes per coordinate.
	 * 
	 * The upper nibble of the first coordinate byte is X, lower Z.
	 * The second coordinate byte is Y.
	 * 
	 * The second section is block types, one byte per block.
	 * The third section is block metadata, one byte per block.
	 */
	@Order(2)
	@Format(ShortPrefixed4ByteArrayFormat.class)
	public byte[] data;
	
	/**
	 * Compiles a bunch of BSetBlocks into their components.
	 * Note that this will still use BSetBlock packets if there's no reason to use BMultipleBlockChange.
	 */
	public static Packet[] combine(Iterable<BSetBlock> parts) {
		// Bucket into chunks.
		HashMap<Long, LinkedList<BSetBlock>> chunks = new HashMap<Long, LinkedList<BSetBlock>>();
		for (BSetBlock b : parts) {
			int chunkX = b.x >> 4;
			int chunkZ = b.z >> 4;
			Long bucket = (((long) chunkX) << 32) | (((long) chunkZ) & 0xFFFFFFFFL);
			LinkedList<BSetBlock> chunk = chunks.get(bucket);
			if (chunk == null) {
				chunk = new LinkedList<BSetBlock>();
				chunks.put(bucket, chunk);
			}
			chunk.add(b);
		}
		// Actually perform the merge.
		Packet[] packets = new Packet[chunks.size()];
		int packet = 0;
		for (Map.Entry<Long, LinkedList<BSetBlock>> ent : chunks.entrySet()) {
			LinkedList<BSetBlock> blocks = ent.getValue();
			if (blocks.size() == 1) {
				packets[packet++] = blocks.getFirst();
			} else {
				long ptr = ent.getKey();
				byte[] store = new byte[blocks.size() * 4];
				int i = 0;
				int j = blocks.size() * 2;
				int k = blocks.size() * 3;
				for (BSetBlock blk : blocks) {
					store[i] = (byte) (((blk.x & 0xF) << 4) | (blk.z & 0xF));
					store[i + 1] = (byte) blk.y;
					i += 2;
					store[j] = blk.type;
					j++;
					store[k] = blk.meta;
					k++;
				}
				BMultipleBlockChange bm = new BMultipleBlockChange();
				bm.x = (int) (ptr >> 32);
				bm.z = (int) (ptr & 0xFFFFFFFFL);
				bm.data = store;
				packets[packet++] = bm;
			}
		}
		// assert packet == packets.length;
		return packets;
	}
}
