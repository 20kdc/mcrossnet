package mccrossnet.protocol.era1.v14;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.StandardStruct;
import mccrossnet.base.data.annotations.Order;

/**
 * This is the handshake packet introduced in ZE1V14.
 * It must be the very first packet sent, and is sent by the client.
 * There is a distinct lack of a protocol version in this packet. 
 */
public class AHandshakeCtos extends StandardStruct implements Packet {
	/**
	 * The username of the connecting player.
	 * Defaults to "Player".
	 */
	@Order(0)
	public String username = "Player";
}
