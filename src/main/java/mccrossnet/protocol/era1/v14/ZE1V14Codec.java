package mccrossnet.protocol.era1.v14;

import java.io.InputStream;
import java.io.OutputStream;

import mccrossnet.protocol.era1.v13.ZE1V13Codec;
import mccrossnet.base.data.FormatContext;

/**
 * Alpha v1.0.16 or E1V14
 * <a href="protocol.html">Protocol</a>
 */
public class ZE1V14Codec extends ZE1V13Codec {

	public ZE1V14Codec() {
		attachCtos(0x02, AHandshakeCtos.class);
		attachStoc(0x02, AHandshakeStoc.class);
	}

}
