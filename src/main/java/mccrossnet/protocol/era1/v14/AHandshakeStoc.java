package mccrossnet.protocol.era1.v14;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.StandardStruct;
import mccrossnet.base.data.annotations.Order;

/**
 * This is the handshake packet introduced in ZE1V14. 
 */
public class AHandshakeStoc extends StandardStruct implements Packet {
	/**
	 * Given auth type "-", disables authentication.
	 * Given auth type "+", the client sends the server password.
	 * 
	 * Otherwise, the client is supposed to do authentication.
	 * That doesn't exist anymore.
	 * 
	 * That considered, defaults to "-".
	 */
	@Order(0)
	public String authType = "-";
}
