package mccrossnet.protocol.all;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.ValueFormat;

/**
 * Format for the Face enum as a single byte.
 * Quick overview of the values: [YM, YP, ZM, ZP, XM, XP].
 */
public class FaceFormat implements ValueFormat<Face> {
	public static final FaceFormat INSTANCE = new FaceFormat();
	
	@Override
	public void write(FormatContext ver, DataOutputStream dos, Face value) throws IOException {
		dos.write(value.ordinal());
	}

	@Override
	public Face read(FormatContext ver, DataInputStream dis) throws IOException {
		return Face.byOrdinal[dis.readByte()];
	}
	
}
