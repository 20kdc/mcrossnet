/**
 * This contains fake "marker" types for use with the mccrossnet.base.core.data.Marker annotation.
 * This is useful to clean up certain version-dependent situations.
 */
package mccrossnet.protocol.all.markers;