package mccrossnet.protocol.all.markers;

/**
 * Vertical coordinate for BSetBlock.
 * Simply put, the difference between Classic and Alpha.
 */
public interface BSetBlockVCoordinate {

}
