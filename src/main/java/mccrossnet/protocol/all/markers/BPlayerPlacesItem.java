package mccrossnet.protocol.all.markers;

/**
 * Before E2V7, it's just the item ID.
 * Otherwise, it has to act equivalent to an unqualified ICDItemStack.
 */
public interface BPlayerPlacesItem {

}
