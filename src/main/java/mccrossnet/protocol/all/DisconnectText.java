package mccrossnet.protocol.all;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.StandardStruct;
import mccrossnet.base.data.annotations.Order;

/**
 * A packet that represents the sender disconnecting.
 */
public class DisconnectText extends StandardStruct implements Packet {
	/**
	 * Disconnection text for the receiver to log (CtoS) or inform the client (StoC). 
	 */
	@Order(0)
	public String text = "Sender didn't set disconnect text, shame on them";

	public DisconnectText() {
	}
	public DisconnectText(String s) {
		text = s;
	}
}
