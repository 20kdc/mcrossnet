package mccrossnet.protocol.all;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.StandardStruct;
import mccrossnet.base.data.annotations.Order;

/**
 * A packet with text in it.
 */
public class ChatText extends StandardStruct implements Packet {
	/**
	 * The text to display or being sent by the client.
	 * The exact format details depend on the version of Minecraft.
	 * In Beta 1.8.1, the maximum length the client accepts is 119 characters.
	 * Presumably, the logic is:
	 *  16 username characters
	 *  3 formatting characters
	 *  100 message characters
	 */
	@Order(0)
	public String text = "";
	
	public ChatText() {
	}
	public ChatText(String s) {
		text = s;
	}
}
