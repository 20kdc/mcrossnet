package mccrossnet.protocol.all.items;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.ValueFormat;
import mccrossnet.protocol.all.items.neganull.ICDItemStackICDFormat;
import mccrossnet.base.data.FormatContext;

/**
 * This is a short followed by that many slots.
 * This always uses the full (not byte-durability) format, which is as follows:
 * Short (Item ID), and if that's not -1, byte (count), short (durability).
 */
public class ShortPrefixedICDItemStackICDArrayFormat implements ValueFormat<ICDItemStack[]> {
	public static final ShortPrefixedICDItemStackICDArrayFormat INSTANCE = new ShortPrefixedICDItemStackICDArrayFormat();
	
	@Override
	public ICDItemStack[] read(FormatContext ver, DataInputStream in) throws IOException {
		ICDItemStack[] content = new ICDItemStack[in.readShort() & 0xFFFF];
		// are these supposed to have true for 2nd param?
		// it feels inconsistent, but then again this all is
		for (int i = 0; i < content.length; i++)
			content[i] = ICDItemStackICDFormat.INSTANCE.read(ver, in);
		return content;
	}

	@Override
	public void write(FormatContext ver, DataOutputStream out, ICDItemStack[] value) throws IOException {
		out.writeShort(value.length);
		for (int i = 0; i < value.length; i++)
			ICDItemStackICDFormat.INSTANCE.write(ver, out, value[i]);
	}
}
