package mccrossnet.protocol.all.items;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.protocol.all.items.neganull.ICDItemStackICByteDFormat;
import mccrossnet.protocol.all.items.neganull.ICDItemStackICDFormat;

/*
 * Immutable.
 * Represents an item stack from Alpha up until item NBT happened. 
 * ICD: ID, Count, Durability.
 * NOTE: The Alpha way of storing "null items", protocol-wise, is -1,
 *  with no additional data.
 * Having no additional data suggests that it should be stored as null,
 *  because otherwise Problematic Nonsense shows up.
 */
public final class ICDItemStack {
	public final short id;
	public final byte count;
	public final short durability;
	
	public ICDItemStack(short i, byte c, short d) {
		id = i;
		count = c;
		durability = d;
	}
	
	public static ICDItemStack simple(int id, int count) {
		return new ICDItemStack((short) id, (byte) count, (short) 0);
	}
	
	@Override
	public String toString() {
		return "{" + id + " " + durability + " x " + count + "}";
	}
}
