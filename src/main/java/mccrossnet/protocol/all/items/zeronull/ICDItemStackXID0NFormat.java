package mccrossnet.protocol.all.items.zeronull;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.ValueFormat;
import mccrossnet.protocol.all.items.ICDItemStack;

/**
 * Yet another weird situation.
 * This is two shorts: item ID and durability.
 * However, unlike usual where it's -1, null (no item) uses an ID of zero.
 */
public class ICDItemStackXID0NFormat implements ValueFormat<ICDItemStack> {
	public static final ICDItemStackXID0NFormat INSTANCE = new ICDItemStackXID0NFormat();

	@Override
	public void write(FormatContext ver, DataOutputStream dos, ICDItemStack value) throws IOException {
		if (value != null) {
			dos.writeShort(value.id);
			dos.writeShort(value.durability);
		} else {
			dos.writeShort(0);
			dos.writeShort(0);
		}
	}

	@Override
	public ICDItemStack read(FormatContext ver, DataInputStream dis) throws IOException {
		short a = dis.readShort();
		short b = dis.readShort();
		if (a == 0)
			return null;
		return new ICDItemStack(a, (byte) 1, b);
	}

}

