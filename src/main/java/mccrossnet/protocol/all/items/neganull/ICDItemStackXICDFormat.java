package mccrossnet.protocol.all.items.neganull;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.ValueFormat;
import mccrossnet.protocol.all.items.ICDItemStack;
import mccrossnet.base.data.FormatContext;

/**
 * Used under weird conditions (BGiveItem).
 * Short (Item ID), byte (count), short (durability).
 * No optional business.
 */
public class ICDItemStackXICDFormat implements ValueFormat<ICDItemStack> {
	public static final ICDItemStackXICDFormat INSTANCE = new ICDItemStackXICDFormat();
	
	@Override
	public ICDItemStack read(FormatContext ver, DataInputStream dis) throws IOException {
		short itemID = dis.readShort();
		byte count = dis.readByte();
		short d = dis.readShort();
		if (itemID < 0)
			return null;
		return new ICDItemStack(itemID, count, d);
	}

	@Override
	public void write(FormatContext ver, DataOutputStream dos, ICDItemStack value) throws IOException {
		if (value == null) {
			dos.writeShort(-1);
			dos.writeByte(0);
			dos.writeShort(0);
		} else {
			dos.writeShort(value.id);
			dos.writeByte(value.count);
			dos.writeShort(value.durability);
		}
	}

}
