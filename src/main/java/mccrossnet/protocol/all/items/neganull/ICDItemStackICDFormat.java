package mccrossnet.protocol.all.items.neganull;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.ValueFormat;
import mccrossnet.protocol.all.items.ICDItemStack;
import mccrossnet.base.data.FormatContext;

/**
 * This format is for E2V8+ ICDItemStacks which use a short for durability rather than a byte.
 * Short (Item ID), and if that's not -1, byte (count), short (durability).
 */
public class ICDItemStackICDFormat implements ValueFormat<ICDItemStack>  {
	public static final ICDItemStackICDFormat INSTANCE = new ICDItemStackICDFormat();
	
	@Override
	public ICDItemStack read(FormatContext ver, DataInputStream in) throws IOException {
		short id = in.readShort();
		if (id < 0)
			return null;
		byte count = in.readByte();
		short durability;
		durability = in.readShort();
		return new ICDItemStack(id, count, durability);
	}

	@Override
	public void write(FormatContext ver, DataOutputStream out, ICDItemStack value) throws IOException {
		if (value == null) {
			out.writeShort(-1);
		} else {
			out.writeShort(value.id);
			out.writeByte(value.count);
			out.writeShort(value.durability);
		}
	}

}
