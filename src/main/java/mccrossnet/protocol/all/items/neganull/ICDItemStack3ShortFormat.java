package mccrossnet.protocol.all.items.neganull;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.ValueFormat;
import mccrossnet.protocol.all.items.ICDItemStack;

/**
 * Yet another wacky format for these things...
 */
public class ICDItemStack3ShortFormat implements ValueFormat<ICDItemStack> {
	public static final ICDItemStack3ShortFormat INSTANCE = new ICDItemStack3ShortFormat();
	
	@Override
	public ICDItemStack read(FormatContext ver, DataInputStream dis) throws IOException {
		short a = dis.readShort();
		short b = dis.readShort();
		short c = dis.readShort();
		if (a < 0)
			return null;
		return new ICDItemStack(a, (byte) b, c);
	}

	@Override
	public void write(FormatContext ver, DataOutputStream dos, ICDItemStack value) throws IOException {
		if (value == null) {
			dos.writeShort(-1);
			dos.writeShort(0);
			dos.writeShort(0);
		} else {
			dos.writeShort(value.id);
			dos.writeShort(value.count);
			dos.writeShort(value.durability);
		}
	}

}
