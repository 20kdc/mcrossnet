package mccrossnet.protocol.all.items.neganull;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.ValueFormat;
import mccrossnet.protocol.all.items.ICDItemStack;
import mccrossnet.base.data.FormatContext;

/**
 * Used for ICDItemStack.
 * Short (Item ID), and if that's not -1, byte (count), byte (durability).
 * Note that durability's a byte; this is fixed in E2V8.
 */
public class ICDItemStackICByteDFormat implements ValueFormat<ICDItemStack> {
	public static final ICDItemStackICByteDFormat INSTANCE = new ICDItemStackICByteDFormat();
	
	@Override
	public ICDItemStack read(FormatContext ver, DataInputStream in) throws IOException {
		short id = in.readShort();
		if (id < 0)
			return null;
		byte count = in.readByte();
		short durability;
		durability = (short) (in.readByte() & 0xFF);
		return new ICDItemStack(id, count, durability);
	}

	@Override
	public void write(FormatContext ver, DataOutputStream out, ICDItemStack value) throws IOException {
		if (value == null) {
			out.writeShort(-1);
		} else {
			out.writeShort(value.id);
			out.writeByte(value.count);
			out.writeByte(value.durability);
		}
	}

}
