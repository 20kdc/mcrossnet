package mccrossnet.protocol.all.items.neganull;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.ValueFormat;
import mccrossnet.protocol.all.items.ICDItemStack;
import mccrossnet.base.data.FormatContext;

/**
 * Used under weird conditions.
 * Just a short (item ID), nothing else.
 */
public class ICDItemStackXIFormat implements ValueFormat<ICDItemStack> {
	public static final ICDItemStackXIFormat INSTANCE = new ICDItemStackXIFormat();
	
	@Override
	public ICDItemStack read(FormatContext ver, DataInputStream dis) throws IOException {
		short itemID = dis.readShort();
		if (itemID < 0)
			return null;
		return ICDItemStack.simple(itemID, 1);
	}

	@Override
	public void write(FormatContext ver, DataOutputStream dos, ICDItemStack value) throws IOException {
		if (value == null) {
			dos.writeShort(-1);
		} else {
			dos.writeShort(value.id);
		}
	}

}
