package mccrossnet.protocol.all.flags;

/**
 * To be more precise, this is used for the jump from E2V14 (b1.7.x) to E2V17 (b1.8),
 *  or https://wiki.vg/index.php?title=Protocol&oldid=602
 *  assuming they don't break the link again.
 * 
 * Because I think either MediaWiki or SOMETHING is out to ruin this project,
 *  here's a backup for comparison when finding it in the log:
 * 
 * curprev 03:34, 21 September 2011‎ Barneygale talk contribs‎ m 125,929 bytes +26‎ →‎Server to Client
 */
public interface TheAdventureUpdate {

}
