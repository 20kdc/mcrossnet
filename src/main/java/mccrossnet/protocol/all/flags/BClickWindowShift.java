package mccrossnet.protocol.all.flags;

/**
 * Before E2V11: Not present. Read as false. Ignored for writing.
 * Otherwise   : Present. BooleanFormat.
 */
public interface BClickWindowShift {
}
