package mccrossnet.protocol.all.flags;

/**
 * Before E2V11: Whatever the string format is
 * Otherwise   : Not present. Read as "", not written.
 */
public interface AConnectPacketPasswordRemoved {

}
