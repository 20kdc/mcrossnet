package mccrossnet.protocol.all.flags;

/**
 * Before E2V5: Not available in this version, don't write or read, assume false 
 * Otherwise: BooleanFormat
 */
public interface BEClickEntityLeft {
}
