package mccrossnet.protocol.all.flags;

/**
 * Before E2V13, doesn't exist.
 * Otherwise, does, and is just FireballDetailsFormat.
 */
public interface BESpawnObjectFireballDetails {
}
