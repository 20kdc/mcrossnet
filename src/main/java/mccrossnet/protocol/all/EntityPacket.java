package mccrossnet.protocol.all;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.Struct; import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;

/*
 * NOTE: The format of this affects the format details of
 *  all of the EntityPacket users, so DO NOT CHANGE IT!
 * EVER!
 */
public class EntityPacket implements Struct, Packet {
	public int id; 
	
	@Override
		public String toString() {
			return super.toString() + "[" + id + "]";
		}
	
	@Override
	public void readContent(FormatContext ver, DataInputStream in) throws IOException {
		id = in.readInt();
	}

	@Override
	public void writeContent(FormatContext ver, DataOutputStream out) throws IOException {
		out.writeInt(id);
	}

}
