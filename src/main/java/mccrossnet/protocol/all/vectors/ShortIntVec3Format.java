package mccrossnet.protocol.all.vectors;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StructFormat;

/**
 * A format for an IntVec3 that is 3 shorts (x, y, z).
 */
public class ShortIntVec3Format implements StructFormat<IntVec3> {
	public static final ShortIntVec3Format INSTANCE = new ShortIntVec3Format();
	
	@Override
	public void write(FormatContext ver, DataOutputStream dos, IntVec3 value) throws IOException {
		dos.writeShort(value.x);
		dos.writeShort(value.y);
		dos.writeShort(value.z);
	}

	@Override
	public void read(FormatContext ver, DataInputStream dis, IntVec3 value) throws IOException {
		value.x = dis.readShort();
		value.y = dis.readShort();
		value.z = dis.readShort();
	}

	@Override
	public String clarify(IntVec3 value) {
		return "";
	}

}
