package mccrossnet.protocol.all.vectors;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StructFormat;

/**
 * The normal format for an IntVec3: 3 ints (x, y, z).
 */
public class IntVec3Format implements StructFormat<IntVec3> {
	public static final IntVec3Format INSTANCE = new IntVec3Format();

	@Override
	public void write(FormatContext ver, DataOutputStream dos, IntVec3 value) throws IOException {
		dos.writeInt(value.x);
		dos.writeInt(value.y);
		dos.writeInt(value.z);
	}

	@Override
	public void read(FormatContext ver, DataInputStream dis, IntVec3 value) throws IOException {
		value.x = dis.readInt();
		value.y = dis.readInt();
		value.z = dis.readInt();
	}

	@Override
	public String clarify(IntVec3 value) {
		return "";
	}

}
