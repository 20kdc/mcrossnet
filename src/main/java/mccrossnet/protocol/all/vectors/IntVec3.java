package mccrossnet.protocol.all.vectors;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * This is a 3-component fixed-point (/32) vector.
 * Mutable, so don't go passing references around.
 */
public class IntVec3 {
	/**
	 * Position
	 */
	public int x, y, z;
	
	/**
	 * Sets this IntVec3 to the value in another IntVec3.
	 * @param other The source.
	 */
	public void set(IntVec3 other) {
		x = other.x;
		y = other.y;
		z = other.z;
	}
	
	/**
	 * Multiplies the input by 32, then sets the content of this IntVec3.
	 */
	public void setFixed(double xO, double yO, double zO) {
		x = (int) (xO * 32);
		y = (int) (yO * 32);
		z = (int) (zO * 32);
	}
	
	public void write(DataOutputStream dos) throws IOException {
		dos.writeInt(x);
		dos.writeInt(y);
		dos.writeInt(z);
	}
	
	public void read(DataInputStream in) throws IOException {
		x = in.readInt();
		y = in.readInt();
		z = in.readInt();
	}
	
	@Override
	public String toString() {
		return "{" + x + ", " + y + ", " + z + "}";
	}
}
