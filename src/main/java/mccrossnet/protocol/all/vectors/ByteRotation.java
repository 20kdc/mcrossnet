package mccrossnet.protocol.all.vectors;

import mccrossnet.base.data.StandardStruct;
import mccrossnet.base.data.annotations.Order;

/**
 * The standard-ish "byte rotation".
 */
public class ByteRotation extends StandardStruct {
	/**
	 * Yaw.
	 * 0 is facing along negative Z.
	 * Increasing turns to the (player-wise) right.
	 * The unit of rotation is '256 == 360 degrees'.
	 */
	@Order(0)
	public byte yaw;
	
	/**
	 * Pitch.
	 * 0 is straight ahead.
	 * 64 is down, while -64 is up (it's the same unit of rotation as yaw).
	 * Going further than these limits creates invalid head rotations.
	 * The unit of rotation is '256 == 360 degrees'.
	 */
	@Order(1)
	public byte pitch;
	
	/**
	 * Sets yaw and pitch.
	 * @param yaw Yaw
	 * @param pitch Pitch
	 */
	public void set(byte yaw, byte pitch) {
		this.yaw = yaw;
		this.pitch = pitch;
	}
}
