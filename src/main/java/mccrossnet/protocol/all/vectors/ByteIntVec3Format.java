package mccrossnet.protocol.all.vectors;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StructFormat;

/**
 * A format for an IntVec3 that is 3 bytes (x, y, z).
 */
public class ByteIntVec3Format implements StructFormat<IntVec3> {
	public static final ByteIntVec3Format INSTANCE = new ByteIntVec3Format();
	
	@Override
	public void write(FormatContext ver, DataOutputStream dos, IntVec3 value) throws IOException {
		dos.writeByte(value.x);
		dos.writeByte(value.y);
		dos.writeByte(value.z);
	}

	@Override
	public void read(FormatContext ver, DataInputStream dis, IntVec3 value) throws IOException {
		value.x = dis.readByte();
		value.y = dis.readByte();
		value.z = dis.readByte();
	}

	@Override
	public String clarify(IntVec3 value) {
		return "";
	}

}
