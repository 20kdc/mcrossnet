package mccrossnet.protocol.all;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.StandardStruct;
import mccrossnet.base.data.annotations.Marker;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.data.annotations.Requires;
import mccrossnet.protocol.all.flags.TheAdventureUpdate;

/**
 * E0V7: ID 01, sent from server to client
 * E1V13: ID 00, sent from client to server
 *  safe to send back though
 * E2V15: ID 00, sent from server to client and client is expected to echo.
 * Has it's own sub-ID, presumably to get more precise ping information.
 */
public class PingKeepAlive extends StandardStruct implements Packet {
	/**
	 * E2V15+: The ID to tell which ping is which.
	 */
	@Order(0)
	@Requires(TheAdventureUpdate.class)
	public int id = 0;
}
