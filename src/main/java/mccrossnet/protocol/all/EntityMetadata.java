package mccrossnet.protocol.all;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.LinkedList;

import mccrossnet.base.data.formats.NotPresentInThisVersionFormat;
import mccrossnet.protocol.all.items.ICDItemStack;

/**
 * "Could they have not just used NBT" - The Format
 * I'll document this at some point in EntityMetadataFormat after I clean this code up.
 */
public class EntityMetadata {
	public static final EntityMetadata[] NONE = new EntityMetadata[0];
	
	public byte descriptor;
	public int intValue;
	public float floatValue;
	public String stringValue;
	public ICDItemStack itemValue;
	public int vValueX, vValueY, vValueZ;
	
	public static EntityMetadata[] read(DataInputStream in) throws IOException {
		LinkedList<EntityMetadata> metadata = new LinkedList<>();
		while (true) {
			byte res = in.readByte();
			if (res == 0x7F)
				break;
			int type = res & 0xE0;
			EntityMetadata m = new EntityMetadata();
			m.descriptor = res;
			if (type == 0x00) {
				m.intValue = in.readByte();
			} else if (type == 0x20) {
				m.intValue = in.readShort();
			} else if (type == 0x40) {
				m.intValue = in.readInt();
			} else if (type == 0x60) {
				m.floatValue = in.readFloat();
			} else if (type == 0x80) {
				m.stringValue = in.readUTF();
			} else if (type == 0xA0) {
				short id = in.readShort();
				byte count = in.readByte();
				short durability = in.readShort();
				m.itemValue = new ICDItemStack(id, count, durability);
			} else if (type == 0xC0) {
				// THIS MAY BE VERSION-DEPENDENT
				// Someone didn't log the change in the protocol changelog
				m.vValueX = in.readInt();
				m.vValueY = in.readInt();
				m.vValueZ = in.readInt();
			} else {
				throw new IOException("unable to handle mdt " + type);
			}
			metadata.add(m);
		}
		return metadata.toArray(new EntityMetadata[0]);
	}
	public static void write(DataOutputStream out, EntityMetadata[] meta) throws IOException {
		for (EntityMetadata m : meta) {
			int type = m.descriptor & 0xE0;
			out.writeByte(m.descriptor);
			if (type == 0x00) {
				out.writeByte(m.intValue);
			} else if (type == 0x20) {
				out.writeShort(m.intValue);
			} else if (type == 0x40) {
				out.writeInt(m.intValue);
			} else if (type == 0x60) {
				out.writeFloat(m.floatValue);
			} else if (type == 0x80) {
				out.writeUTF(m.stringValue);
			} else if (type == 0xA0) {
				out.writeShort(m.itemValue.id);
				out.writeByte(m.itemValue.count);
				out.writeShort(m.itemValue.durability);
			} else if (type == 0xC0) {
				// THIS MAY BE VERSION-DEPENDENT
				// Someone didn't log the change in the protocol changelog
				out.writeInt(m.vValueX);
				out.writeInt(m.vValueY);
				out.writeInt(m.vValueZ);
			} else {
				throw new IOException("unable to handle mdt " + type);
			}
		}
		out.write(0x7F);
	}
}
