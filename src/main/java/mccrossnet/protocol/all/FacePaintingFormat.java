package mccrossnet.protocol.all;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.ValueFormat;

/**
 * The direction of a painting uses a different set to other directions.
 * Starting from 0, ZM, XM, ZP, XP.
 * It's also an int.
 */
public class FacePaintingFormat implements ValueFormat<Face> {
	public static final FacePaintingFormat INSTANCE = new FacePaintingFormat();
	
	@Override
	public void write(FormatContext ver, DataOutputStream dos, Face value) throws IOException {
		if (value == Face.ZM)
			dos.writeInt(0);
		else if (value == Face.XM)
			dos.writeInt(1);
		else if (value == Face.ZP)
			dos.writeInt(2);
		else if (value == Face.XP)
			dos.writeInt(3);
		else
			throw new RuntimeException("invalid painting face " + value);
	}

	@Override
	public Face read(FormatContext ver, DataInputStream dis) throws IOException {
		int b = dis.readInt();
		Face[] faces = new Face[] {
				Face.ZM,
				Face.XM,
				Face.ZP,
				Face.XP
		};
		return faces[b];
	}
}
