package mccrossnet.protocol.all;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.Struct;
import mccrossnet.base.data.FormatContext;

/**
 * This indicates the packet is empty for use in the documentation system.
 * It's also an efficient implementation of such an empty packet.
 */
public class EmptyPacket implements Struct, Packet {

	@Override
	public void readContent(FormatContext ver, DataInputStream in) throws IOException {
	}

	@Override
	public void writeContent(FormatContext ver, DataOutputStream out) throws IOException {
	}
	
}
