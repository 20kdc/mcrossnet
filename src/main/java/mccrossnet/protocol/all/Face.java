package mccrossnet.protocol.all;

// A facing direction as used in the 0x0E and 0x0F packets of e1v13
public enum Face {
	// NOTE: The numeric values of these are important.
	YM, YP, ZM, ZP, XM, XP;
	public static final Face[] byOrdinal = values();
}
