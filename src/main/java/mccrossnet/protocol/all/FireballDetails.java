package mccrossnet.protocol.all;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.Struct;

/**
 * This structure exists to prevent BESpawnObject from being a mess.
 * Please deal with it.
 * It's an int followed by, if that int is not zero, 3 shorts.
 */
public class FireballDetails {
	public int fireballSenderEID;
	public short fireballWX;
	public short fireballWY;
	public short fireballWZ;
}
