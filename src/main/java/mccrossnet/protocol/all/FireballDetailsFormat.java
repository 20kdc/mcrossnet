package mccrossnet.protocol.all;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.ValueFormat;

/**
 * It's an int followed by, if that int is not zero, 3 shorts.
 * If it is zero, the canonical form of this value is null.
 */
public class FireballDetailsFormat implements ValueFormat<FireballDetails> {
	public static final FireballDetailsFormat INSTANCE = new FireballDetailsFormat();
	
	@Override
	public void write(FormatContext ver, DataOutputStream out, FireballDetails value) throws IOException {
		if (value == null) {
			out.writeInt(0);
		} else {
			out.writeInt(value.fireballSenderEID);
			if (value.fireballSenderEID != 0) {
				out.writeShort(value.fireballWX);
				out.writeShort(value.fireballWY);
				out.writeShort(value.fireballWZ);
			}
		}
	}

	@Override
	public FireballDetails read(FormatContext ver, DataInputStream in) throws IOException {
		int eid = in.readInt();
		if (eid == 0)
			return null;
		FireballDetails fd = new FireballDetails();
		fd.fireballSenderEID = eid;
		fd.fireballWX = in.readShort();
		fd.fireballWY = in.readShort();
		fd.fireballWZ = in.readShort();
		return fd;
	}
}
