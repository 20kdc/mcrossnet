package mccrossnet.protocol.all;

import mccrossnet.base.data.StandardStruct;
import mccrossnet.base.data.annotations.Marker;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.protocol.all.markers.EntityID;

/**
 * This is the base struct for any packet that's prefixed with an entity ID.
 * Obviously, changing this is a bad idea.
 */
public class EntityStruct extends StandardStruct {
	/**
	 * The ID of the entity that is being affected.
	 * In some cases, these packets affect the local client, or are sent client-to-server.
	 * 
	 * In Classic, the "ID" -1 is used for this.
	 * Later versions tell the player their entity ID. 
	 */
	@Order(0)
	@Marker(EntityID.class)
	public int entityID = -1;
}
