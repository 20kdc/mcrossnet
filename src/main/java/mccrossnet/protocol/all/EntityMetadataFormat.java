package mccrossnet.protocol.all;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.ValueFormat;

/**
 * Encoder/decoder for EntityMetadata.
 * Honestly, read the EntityMetadata class, it's complicated.
 */
public class EntityMetadataFormat implements ValueFormat<EntityMetadata[]> {
	public static final EntityMetadataFormat INSTANCE = new EntityMetadataFormat();

	@Override
	public void write(FormatContext ver, DataOutputStream dos, EntityMetadata[] value) throws IOException {
		EntityMetadata.write(dos, value);
	}

	@Override
	public EntityMetadata[] read(FormatContext ver, DataInputStream dis) throws IOException {
		return EntityMetadata.read(dis);
	}
	
	
}
