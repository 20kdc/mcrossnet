package mccrossnet.protocol.all;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.StandardStruct;
import mccrossnet.base.data.annotations.Order;

/**
 * Base class of window-related packets.
 * These all start with the target window ID.
 */
public class WindowStruct extends StandardStruct {
	/**
	 * The window ID. This is weird.
	 * 0 is player inventory, anything above that's an arbitrary ID (?),
	 *  negative numbers are special.
	 * 
	 * The player inventory window has 45 slots.
	 * 5 crafting slots (result first), followed by 4 armour slots, followed by 36 main slots. 
	 * 
	 * Outside of that ordering, the rest are in the 'normal' order (N rows of M items)
	 */
	@Order(0)
	public byte windowID;
}
