package mccrossnet.protocol.era0.v7;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.StandardStruct;
import mccrossnet.base.data.annotations.Marker;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.data.annotations.Requires;
import mccrossnet.protocol.all.flags.BESpawnPlayerCurrentItem;

/**
 * Server-to-client message to indicate a player has joined.
 */
public class BESpawnPlayer extends BETeleport implements Packet {
	/**
	 * The username of the created player.
	 */
	@Order(1)
	public String username = "Player";
	
	/**
	 * The current item the player is holding.
	 * Not supported for Classic.
	 */
	@Order(4)
	@Requires(BESpawnPlayerCurrentItem.class)
	public short heldItem;
}
