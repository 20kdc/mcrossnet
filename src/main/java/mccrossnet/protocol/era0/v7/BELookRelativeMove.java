package mccrossnet.protocol.era0.v7;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.StandardStruct;
import mccrossnet.base.data.annotations.Format;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.protocol.all.EntityStruct;
import mccrossnet.protocol.all.vectors.ByteIntVec3Format;
import mccrossnet.protocol.all.vectors.ByteRotation;
import mccrossnet.protocol.all.vectors.IntVec3;

/**
 * This is a server-to-client relative position update.
 * So long as local physics isn't a thing, it doesn't risk desyncs.
 */
public class BELookRelativeMove extends EntityStruct implements Packet {
	/**
	 * Position Delta (in 32ths of a block)
	 */
	@Order(1)
	@Format(ByteIntVec3Format.class)
	public final IntVec3 positionDelta = new IntVec3();
	
	/**
	 * Rotation (absolute)
	 */
	@Order(2)
	public final ByteRotation rotation = new ByteRotation();
}
