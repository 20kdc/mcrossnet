package mccrossnet.protocol.era0.v7;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.StandardStruct;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.protocol.all.EntityStruct;

/**
 * Chat messages in Classic are just like in other versions.
 * But they're pretending not to be.
 * Essentially, you're supposed to provide the sending player entity ID.
 * But I'm not strictly sure that actually matters... at all.
 */
public class BPlayerMessage extends EntityStruct implements Packet {
	/**
	 * The text of the chat message.
	 * Defaults to something silly.
	 * The '&' character followed by a hexadecimal letter sets text colour.
	 * This can crash clients if these are not checked by the server.
	 */
	@Order(1)
	public String text = "GOTE: Gotten Over That Episode (of [insert show here])";
}
