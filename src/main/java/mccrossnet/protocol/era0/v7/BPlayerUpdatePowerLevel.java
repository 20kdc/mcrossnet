package mccrossnet.protocol.era0.v7;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.StandardStruct;
import mccrossnet.base.data.annotations.Order;

/**
 * Sent from server to update a client's power level.
 */
public class BPlayerUpdatePowerLevel extends StandardStruct implements Packet {
	/**
	 * The new power level. 100 is op, 0 is normal.
	 */
	@Order(0)
	public byte powerLevel;
}
