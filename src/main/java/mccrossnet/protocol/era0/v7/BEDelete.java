package mccrossnet.protocol.era0.v7;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.StandardStruct;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.protocol.all.EntityStruct;

/**
 * This deletes the entity with the given ID.
 */
public class BEDelete extends EntityStruct implements Packet {
	public BEDelete() {
		
	}
	
	public BEDelete(int id) {
		entityID = id;
	}
}
