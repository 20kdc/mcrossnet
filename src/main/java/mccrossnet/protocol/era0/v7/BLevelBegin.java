package mccrossnet.protocol.era0.v7;

import mccrossnet.protocol.all.EmptyPacket;

/**
 * Begins a level loading session.
 * (Progress bar, clears buffer, etc.)
 */
public class BLevelBegin extends EmptyPacket {
}
