package mccrossnet.protocol.era0.v7;

import mccrossnet.protocol.era1.v13.ZE1V13IDs;

public class ZE0V7IDs extends ZE0V7SurvivingIDs {
	public static final ZE0V7IDs INSTANCE = new ZE0V7IDs();
	
	public static final byte B_WOOL_C0 = 21;
	public static final byte B_WOOL_C1 = 22;
	public static final byte B_WOOL_C2 = 23;
	public static final byte B_WOOL_C3 = 24;
	public static final byte B_WOOL_C4 = 25;
	public static final byte B_WOOL_C5 = 26;
	public static final byte B_WOOL_C6 = 27;
	public static final byte B_WOOL_C7 = 28;
	public static final byte B_WOOL_C8 = 29;
	public static final byte B_WOOL_C9 = 30;
	public static final byte B_WOOL_CA = 31;
	public static final byte B_WOOL_CB = 32;
	public static final byte B_WOOL_CC = 33;
	public static final byte B_WOOL_CD = 34;
	// This is aliased to just WOOL for compatibility
	public static final byte B_WOOL_CE = 35;
	public static final byte B_WOOL_CF = 36;
}
