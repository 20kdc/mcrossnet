package mccrossnet.protocol.era0.v7;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.zip.GZIPOutputStream;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.StandardStruct;
import mccrossnet.base.data.annotations.Format;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.data.formats.FixedSizeByteArrayFormat;

/**
 * The server sends a part of the world this way.
 * 
 * The format of the world is an int-prefixed byte array.
 * Each byte is a single block.
 * 
 * Blocks are laid out as Y planes, of Z rows, of X bytes, the direction always being positive.
 * 
 * Then the whole thing is GZIP'd and split into these BLevelData chunks.
 * 
 */
public class BLevelData extends StandardStruct implements Packet {
	/**
	 * This is the amount of the data array used.
	 */
	@Order(0)
	public short dataUsage;
	
	/**
	 * Some data to append onto the world data buffer.
	 * Always 1024 bytes, so to save much trouble this is treated struct-like.
	 */
	@Order(1)
	@Format(FixedSizeByteArrayFormat.class)
	public final byte[] data = new byte[1024];
	
	/**
	 * Progress (from 0 to 100)
	 */
	@Order(2)
	public byte progress;
	
	/**
	 * Performs prefix, GZIP and split.
	 * @param data The raw (unGZIP'd) block data
	 * @return The resulting chunks.
	 */
	public static BLevelData[] compressAndSplit(byte[] data) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			GZIPOutputStream gos = new GZIPOutputStream(baos);
			new DataOutputStream(gos).writeInt(data.length);
			gos.write(data);
			gos.close();
		} catch (IOException ioe) {
			throw new RuntimeException(ioe);
		}
		byte[] total = baos.toByteArray();
		LinkedList<BLevelData> chunks = new LinkedList<>();
		for (int i = 0; i < total.length; i += 1024) {
			int remaining = total.length - i;
			if (remaining > 1024)
				remaining = 1024;
			BLevelData chunk = new BLevelData();
			System.arraycopy(total, i, chunk.data, 0, remaining);
			chunk.dataUsage = (short) remaining;
			chunk.progress = (byte) ((i * 100) / total.length);
			chunks.add(chunk);
		}
		return chunks.toArray(new BLevelData[0]);
	}
}
