package mccrossnet.protocol.era0.v7;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.StandardStruct;
import mccrossnet.base.data.annotations.Order;

/**
 * Sent from client to server to indicate placing or breaking a block.
 */
public class BPlayerAction extends StandardStruct implements Packet {
	/**
	 * X of the position to alter.
	 * Note that there's no "face" variable; this is the actual target.
	 */
	@Order(0)
	public short x;

	/**
	 * Y of the position to alter.
	 * Note that there's no "face" variable; this is the actual target.
	 */
	@Order(1)
	public short y;

	/**
	 * Z of the position to alter.
	 * Note that there's no "face" variable; this is the actual target.
	 */
	@Order(2)
	public short z;
	
	/**
	 * True if a block is being placed at that location.
	 */
	@Order(3)
	public boolean place;

	/**
	 * This is the kind of block held by the player at the time.
	 * IF WRITING A CLIENT: THIS CAN HAVE SPECIAL SEMANTICS IF BREAKING A BLOCK.
	 * For further information, please see copygirl's video:
	 * https://www.youtube.com/watch?v=4PBIqoBP_y4
	 *  as linked at:
	 * https://notch.tumblr.com/post/926969603/copyboys-been-working-on-a-crazy-sweet-mod-for
	 */
	@Order(4)
	public byte heldBlock;
}
