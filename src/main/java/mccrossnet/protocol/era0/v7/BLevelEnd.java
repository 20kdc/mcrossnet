package mccrossnet.protocol.era0.v7;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.StandardStruct;
import mccrossnet.base.data.annotations.Order;

/**
 * Sent from server to client to finish loading the world.
 */
public class BLevelEnd extends StandardStruct implements Packet {
	public BLevelEnd() {
		
	}
	
	public BLevelEnd(short i, short j, short k) {
		x = i;
		y = j;
		z = k;
	}

	/**
	 * X size of the world in blocks.
	 */
	@Order(0)
	public short x;
	
	/**
	 * Y size of the world in blocks.
	 */
	@Order(1)
	public short y;
	
	/**
	 * Z size of the world in blocks.
	 */
	@Order(2)
	public short z;
}
