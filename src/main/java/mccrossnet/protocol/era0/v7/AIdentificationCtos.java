package mccrossnet.protocol.era0.v7;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.core.VersionSpecifyingPacket;
import mccrossnet.base.data.StandardStruct;
import mccrossnet.base.data.Struct;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.annotations.Order;

/**
 * This is the first packet sent. It's sent from the client to the server to self-identify and such.
 * NOTE: A server may supposedly send this to change the text above the progress bar at any time.
 */
public class AIdentificationCtos extends StandardStruct implements Packet, VersionSpecifyingPacket {
	/**
	 * The protocol version.
	 * Should be 7, but defaults to 0 so things won't break if support for older versions is added.
	 */
	@Order(0)
	public byte protocolVersion;
	
	/**
	 * The username of the player.
	 */
	@Order(1)
	public String username = "Player";

	/**
	 * Authentication key.
	 */
	@Order(2)
	public String authKey = "";

	/**
	 * Not really applicable here.
	 */
	@Order(3)
	public byte unused;

	@Override
	public int getProtocolVersion() {
		return protocolVersion;
	}

}
