package mccrossnet.protocol.era0.v7;

import mccrossnet.base.core.Packet;
import mccrossnet.base.core.Protocol;
import mccrossnet.base.data.annotations.Format;
import mccrossnet.base.data.annotations.Marker;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.protocol.all.EntityStruct;
import mccrossnet.protocol.all.markers.BETeleportPosition;
import mccrossnet.protocol.all.vectors.ByteRotation;
import mccrossnet.protocol.all.vectors.IntVec3;
import mccrossnet.protocol.era1.v13.ZE1V13Codec;
import mccrossnet.workbench.E0V7Workbench;

/**
 * In Classic, this is similar to BPlayerMessage.
 * Alpha and onwards have dedicated packets for player control, so this is remote entities only.
 */
public class BETeleport extends EntityStruct implements Packet {
	
	// Gap at Order(1), see BPlayerCreate!
	
	/**
	 * Position (in 32ths of a block)
	 * 
	 * In Classic, this is camera position.
	 * Others have estimated that the amount to nudge upwards is 51/32 units.
	 * 
	 * In Alpha, this is foot position.
	 */
	@Order(2)
	@Marker(BETeleportPosition.class)
	public final IntVec3 position = new IntVec3();
	
	/**
	 * Rotation
	 */
	@Order(3)
	public final ByteRotation rotation = new ByteRotation();
	
	/**
	 * Adjusts the packet for Classic positioning.
	 * @param ver The protocol to adjust for.
	 */
	public void adjust(Protocol ver) {
		if (!(ver instanceof ZE1V13Codec))
			position.y += 51;
	}
}
