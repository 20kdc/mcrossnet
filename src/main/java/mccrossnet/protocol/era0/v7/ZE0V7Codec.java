package mccrossnet.protocol.era0.v7;

import mccrossnet.base.data.BaseFormat;
import mccrossnet.base.data.formats.ByteAsIntFormat;
import mccrossnet.base.data.formats.NotPresentInThisVersionFormat;
import mccrossnet.base.data.formats.PaddedStringFormat;
import mccrossnet.base.data.formats.ShortAsIntFormat;
import mccrossnet.base.data.formats.ShortFormat;
import mccrossnet.base.netarch.hub.HubProtocol;
import mccrossnet.protocol.all.DisconnectText;
import mccrossnet.protocol.all.PingKeepAlive;
import mccrossnet.protocol.all.flags.BESpawnPlayerCurrentItem;
import mccrossnet.protocol.all.markers.BETeleportPosition;
import mccrossnet.protocol.all.markers.BSetBlockHCoordinate;
import mccrossnet.protocol.all.markers.BSetBlockVCoordinate;
import mccrossnet.protocol.all.markers.EntityID;
import mccrossnet.protocol.all.vectors.ShortIntVec3Format;

/**
 * This is Classic. As in, actual Classic.
 * It's the last version of it, anyway.
 * Support for older versions seemed a tad fruitless.
 * Anyway, here's how things go from a server's perspective:
 * 
 * You receive the AIdentificationCtos.
 * You send back an AIdentificationStoc.
 * You send the level.
 * And only then do you spawn the player using an EID -1 BESpawnPlayer.
 * 
 * <a href="protocol.html">Protocol</a>
 */
public class ZE0V7Codec extends HubProtocol {

	public ZE0V7Codec() {
		super(PaddedStringFormat.INSTANCE, ZE0V7IDs.INSTANCE);
		
		// CtoS (with adjustments where replicated)
		attachCtos(0x00, AIdentificationCtos.class);
		attachCtos(0x05, BPlayerAction.class);
		attach(0x08, BETeleport.class);
		attach(0x0D, BPlayerMessage.class);
		
		// StoC
		attachStoc(0x00, AIdentificationStoc.class);
		attachStoc(0x01, PingKeepAlive.class);
		attachStoc(0x02, BLevelBegin.class);
		attachStoc(0x03, BLevelData.class);
		attachStoc(0x04, BLevelEnd.class);
		attachStoc(0x06, BSetBlock.class);
		attachStoc(0x07, BESpawnPlayer.class);
		// 0x08 was already covered
		attachStoc(0x09, BELookRelativeMove.class);
		attachStoc(0x0A, BERelativeMove.class);
		attachStoc(0x0B, BELook.class);
		attachStoc(0x0C, BEDelete.class);
		// 0x0D was already covered
		attachStoc(0x0E, DisconnectText.class);
		attachStoc(0x0F, BPlayerUpdatePowerLevel.class);
	}

	@Override
	public <T> BaseFormat<T> getDefaultFormat(Class<T> fieldType) {
		if (fieldType == EntityID.class)
			return (BaseFormat<T>) ByteAsIntFormat.INSTANCE;
		if (fieldType == BETeleportPosition.class)
			return (BaseFormat<T>) ShortIntVec3Format.INSTANCE;
		if (fieldType == BSetBlockHCoordinate.class)
			return (BaseFormat<T>) ShortAsIntFormat.INSTANCE;
		if (fieldType == BSetBlockVCoordinate.class)
			return (BaseFormat<T>) ShortFormat.INSTANCE;
		return super.getDefaultFormat(fieldType);
	}
}
