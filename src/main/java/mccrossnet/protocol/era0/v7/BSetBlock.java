package mccrossnet.protocol.era0.v7;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.StandardStruct;
import mccrossnet.base.data.annotations.Marker;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.data.annotations.Requires;
import mccrossnet.protocol.all.flags.BSetBlockMeta;
import mccrossnet.protocol.all.markers.BSetBlockHCoordinate;
import mccrossnet.protocol.all.markers.BSetBlockVCoordinate;

/**
 * Sent from server to client to set a block.
 */
public class BSetBlock extends StandardStruct implements Packet {
	/**
	 * X
	 */
	@Order(0)
	@Marker(BSetBlockHCoordinate.class)
	public int x;
	
	/**
	 * Y
	 */
	@Order(1)
	@Marker(BSetBlockVCoordinate.class)
	public short y;
	
	/**
	 * Z
	 */
	@Order(2)
	@Marker(BSetBlockHCoordinate.class)
	public int z;
	
	/**
	 * The type of block. (See ZE0V7IDs.)
	 */
	@Order(3)
	public byte type;

	/**
	 * Block metadata
	 */
	@Order(4)
	@Requires(BSetBlockMeta.class)
	public byte meta;
	
	public BSetBlock() {
		
	}
	
	public BSetBlock(int x, short y, int z, byte type, byte meta) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.type = type;
		this.meta = meta;
	}
}
