package mccrossnet.protocol.era0.v7;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.core.VersionSpecifyingPacket;
import mccrossnet.base.data.StandardStruct;
import mccrossnet.base.data.Struct;
import mccrossnet.base.core.Packet;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.annotations.Order;

/**
 * The server's confirmation that the client has successfully connected.
 * A server may send this to change the text above the progress bar at any time.
 */
public class AIdentificationStoc extends StandardStruct implements Packet, VersionSpecifyingPacket {
	/**
	 * Protocol version.
	 * Would presumably be set to 7, but defaults to 0 to avoid assumptions.
	 */
	@Order(0)
	public byte protocolVersion;
	
	/**
	 * The name of the server (as displayed on the first status line).
	 * Defaults to something silly.
	 */
	@Order(1)
	public String serverName = "Obelisk (Side B)";
	
	/**
	 * The MOTD of the server (as displayed on the second status line).
	 * Defaults to something silly.
	 */
	@Order(2)
	public String serverMOTD = "The light of a lantern shall never find this place.";

	/**
	 * If 100, the receiving player is an operator.
	 */
	@Order(3)
	public byte powerLevel;

	@Override
	public int getProtocolVersion() {
		return protocolVersion;
	}

}
