package mccrossnet.protocol.era0.v7;

import mccrossnet.base.core.IDRegistry;
import mccrossnet.base.valarch.FieldStoreIDRegistry;

/*
 * These are all IDs from E0V7 that survived until the Flattening.
 */
public class ZE0V7SurvivingIDs extends FieldStoreIDRegistry {
	public static final byte B_AIR = 0;
	public static final byte B_STONE = 1;
	public static final byte B_GRASS = 2;
	public static final byte B_DIRT = 3;
	public static final byte B_COBBLE = 4;
	public static final byte B_WOOD = 5;
	public static final byte B_SAPLING = 6;
	public static final byte B_BEDROCK = 7;
	public static final byte B_WATER_FLOW = 8;
	public static final byte B_WATER = 9;
	public static final byte B_LAVA_FLOW = 10;
	public static final byte B_LAVA = 11;
	public static final byte B_SAND = 12;
	public static final byte B_GRAVEL = 13;
	public static final byte B_GOLD_ORE = 14;
	public static final byte B_IRON_ORE = 15;
	public static final byte B_COAL_ORE = 16;
	public static final byte B_LOG = 17;
	public static final byte B_LEAVES = 18;
	public static final byte B_SPONGE = 19;
	public static final byte B_GLASS = 20;
	// NOTE: Colour is dependent on which version!
	public static final byte B_WOOL = 35;
	public static final byte B_DANDELION = 37;
	public static final byte B_ROSE = 38;
	public static final byte B_MUSHROOM_BROWN = 39;
	public static final byte B_MUSHROOM_RED = 40;
	public static final byte B_GOLD_BLOCK = 41;
	public static final byte B_IRON_BLOCK = 42;
	public static final byte B_SLAB_DOUBLE = 43;
	public static final byte B_SLAB = 44;
	public static final byte B_BRICKS = 45;
	public static final byte B_TNT = 46;
	public static final byte B_BOOKSHELF = 47;
	public static final byte B_MOSSY_COBBLE = 48;
	public static final byte B_OBSIDIAN = 49;
}
