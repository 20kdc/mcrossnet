package mccrossnet.protocol.era0.v7;

import mccrossnet.base.core.Packet;
import mccrossnet.base.data.StandardStruct;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.protocol.all.EntityStruct;
import mccrossnet.protocol.all.vectors.ByteRotation;

/**
 * This is a server-to-client rotation-only update.
 */
public class BELook extends EntityStruct implements Packet {
	/**
	 * Absolute rotation.
	 */
	@Order(1)
	public final ByteRotation rotation = new ByteRotation();
}
