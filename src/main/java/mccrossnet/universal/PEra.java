package mccrossnet.universal;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * This represents a given "era"; that is, an issue of protocol numbers.
 * When protocol numbers repeat, that means a new era has been entered.
 * Arguably, E0 and E1 numbering was never truly incompatible:
 * The last E0 number was 7, and the first *known* E1 number was 10.
 * However, since the details are essentially lost to time, & they're different generations...
 * It's better this way.
 */
public enum PEra {
	// Note: Index is expected to match these numbers for sanity. Not strictly necessary.
	// But indexes must be in order because this is used for version checks in certain packets
	//  where the format changed for no reason.
	E0("Classic"),
	// -- e01 goes here, but has no protocol versions --
	E1("Early Alpha"),
	E2("Pre-Netty"),
	E3("Netty");
	
	public final String name;
	final LinkedList<PVersion> versionsInternal = new LinkedList<>();
	public final List<PVersion> versions = Collections.unmodifiableList(versionsInternal);
	
	private PEra(String n) {
		name = n;
	}
	
	public PVersion version(int v) {
		for (PVersion ver : versions)
			if (ver.version == v)
				return ver;
		return null;
	}
	
	public boolean before(PEra other) {
		return other.ordinal() > ordinal();
	}
	
	static {
		PVersion.attach();
	}
}
