package mccrossnet.universal;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.SequenceInputStream;

import mccrossnet.base.DebugInputStream;
import mccrossnet.base.TapeRecorderInputStream;
import mccrossnet.base.core.Connection;
import mccrossnet.base.core.UnableToLocateCodecVSP;
import mccrossnet.base.core.VersionSpecifyingPacket;
import mccrossnet.base.data.formats.PaddedStringFormat;
import mccrossnet.base.data.formats.UTFStringFormat;
import mccrossnet.base.data.formats.WideStringFormat;
import mccrossnet.protocol.era0.v7.AIdentificationCtos;
import mccrossnet.protocol.era1.v13.AConnectPacketCtos;
import mccrossnet.protocol.era1.v14.AHandshakeCtos;
import mccrossnet.protocol.era1.v14.AHandshakeStoc;

/**
 * This class does arcane magic in order to detect a connecting client's version.
 * However, to do so, it also may have to complete the handshake.
 * The state the client is left in is protocol-specific.
 * However, the client will be identified in as few packets as possible.
 */
public class Handshaker {
	
	/**
	 * The era (needed to disambiguate certain connection packets that lived across eras)
	 */
	public PEra era;
	
	/**
	 * Determines if the client uses wide strings.
	 */
	private boolean usesWideStrings = false;
	
	/**
	 * If an AHandshake packet was involved in the handshake, it is stored here.
	 */
	public AHandshakeCtos handshakePacket;
	
	/**
	 * This is the final packet the Handshaker acquired,
	 *  which specifies the version.
	 * Which packet is dependent on what, exactly, happened.
	 */
	public VersionSpecifyingPacket connectPacket;

	private final DataOutputStream out;
	
	// The complex code is below, so put utilities first
	
	public int getVersionID() {
		return connectPacket.getProtocolVersion();
	}

	public PVersion getVersion() {
		return era.version(getVersionID());
	}
	
	// NOTE: This can send disconnects for versions that don't have PVersion codecs.
	public void sendDisconnect(String text) throws IOException {
		if (era == PEra.E0) {
			out.writeByte(0x0E);
			PaddedStringFormat.INSTANCE.write(null, out, text);
			out.flush();
		} else if ((era == PEra.E1) || (era == PEra.E2)) {
			out.writeByte(0xFF);
			if (usesWideStrings) {
				WideStringFormat.INSTANCE.write(null, out, text);
			} else {
				UTFStringFormat.INSTANCE.write(null, out, text);
			}
			out.flush();
		} else if (era == PEra.E3) {
			throw new IOException("No handling for E3 discon yet");
		}
	}
	
	
	// Performs the handshake.
	// The handshake results are returned in the form of a situation-specific Object.
	public Handshaker(InputStream im, OutputStream om) throws IOException {
		TapeRecorderInputStream tape = new TapeRecorderInputStream(im);
		DataInputStream in = new DataInputStream(tape);
		// Not sure Minecraft 'likes' interrupted packets.
		// Rather than risk it, use a massive buffer and flush everything.
		out = new DataOutputStream(new BufferedOutputStream(om, 65536));
		
		byte position0Byte = in.readByte();
		if (position0Byte == 0) {
			// This is a Classic client's AIdentification packet.
			era = PEra.E0;
			AIdentificationCtos e0IDPacket = new AIdentificationCtos();
			connectPacket = e0IDPacket;
			e0IDPacket.readContent(PVersion.E0V7.codec, in);
		} else if (position0Byte == 1) {
			// This is E1V13's AConnectPacket.
			era = PEra.E1;
			AConnectPacketCtos p = new AConnectPacketCtos();
			connectPacket = p;
			p.readContent(PVersion.E1V13.codec, in);
		} else if (position0Byte == 2) {
			// This is E1V14 or any E2 client.
			// But somewhere after E2V29 (12w15a), and before/at E2V39 (12w30c),
			//  the handshake format changed from a single string (with varying content)
			//  to a more complex split format.
			// Luckily, there is no good reason for the string to ever exceed the
			//  amount of characters it would take for this to be truly ambiguous.
			// Start recording so the packet content can be read properly.
			tape.startPreread();
			// If this is the single string, this is a ULB (so, must be under 29)
			// Otherwise, it's a protocol version.
			byte protocolVersionOrULB = in.readByte();
			if ((protocolVersionOrULB >= 0) && (protocolVersionOrULB < 29)) {
				// Old (that was upper length byte)
				// E1V14 to E2V29
				
				// E1V14 (a1.0.16) exists and overlaps E2V14 (b1.7.3).
				// But these use different AHandshake packets.
				// (Which is something that has to be checked for anyway...)
				byte getB, getC;
				getB = in.readByte();
				if ((protocolVersionOrULB == 0) && (getB == 0)) {
					// Ambiguous and invalid
					// The string type can't be determined, so error immediately
					// If something goes wrong with this it's your own fault!
					era = PEra.E1;
					sendDisconnect("Empty username in handshake (can't determine string char width)");
					throw new IOException("empty username in handshake");
				}
				getC = in.readByte();
				// Work out details from that...
				usesWideStrings = getC == 0;
				PVersion versionForHandshakes = usesWideStrings ? PVersion.E2V11 : PVersion.E1V14;
				// Rewind so that we can read the whole handshake packet as it's supposed to be.
				tape.endPreread();
				handshakePacket = new AHandshakeCtos();
				handshakePacket.readContent(versionForHandshakes.codec, in);
				// And send our own...
				// Mojang servers are down until the end of time, go unauthenticated
				out.write(2);
				versionForHandshakes.codec.stringFormat.write(versionForHandshakes.codec, out, "-");
				out.flush();

				// Unfortunately THIS STILL ISN'T ENOUGH to work out the version!
				// We need a connectPacket for that.
				// And since we need the version before we even have the packet,
				//  we need to pre-read the whole thing, then feed it to the Connection.
				tape.startPreread();
				byte result = in.readByte();
				if (result != 0x01) {
					sendDisconnect("Send an 0x01 packet after getting 0x02 response, please. (e1/e2 w=" + usesWideStrings + ")");
					throw new IOException("didn't get a connect packet, got " + result + " (wide: " + usesWideStrings + ")");
				}

				// However, we're stuck needing to get this into a packet,
				//  which means we need to run through the protocol database and create a codec.
				// Then we need to perform a juggling act in order to feed the data in correctly,
				//  because of the necessary peeking to get that version.
				int protocolVersion = in.readInt();
				
				// From the protocol version & wide flag, work out the era
				if (usesWideStrings) {
					era = PEra.E2;
				} else {
					// if protocolVersion >= 11 here, it can't be E2, because those use wide strings
					era = protocolVersion >= 11 ? PEra.E1 : PEra.E2;
				}
				
				// Work out if we can handle the connection packet or not.
				// If we can't, bow out 'gracefully';
				//  we're got the information, but the state is undefined
				
				PVersion target = era.version(protocolVersion);
				// System.out.println(target);
				if ((target == null) || (target.codec == null)) {
					connectPacket = new UnableToLocateCodecVSP(protocolVersion);
				} else {
					// End the pre-read, reverting to the byte at the start of the connection packet.
					// This implies the type byte is in place, which is going to be useful... immediately!
					tape.endPreread();
					Connection c = target.codec.makeConnection(in, out, true);
					connectPacket = (VersionSpecifyingPacket) c.read();
				}
			} else {
				// New (that was protocol version)
				// E2V30 to before E3
				era = PEra.E2;
				usesWideStrings = true;
				sendDisconnect("No support yet for E2V30+ handshake");
				throw new IOException("too new right now");
			}
		} else if (position0Byte == (byte) 0xFE) {
			era = PEra.E2;
			usesWideStrings = true;
			sendDisconnect("server list ping not supported");
			throw new IOException("server list ping not supported");
		} else {
			// This is the Netty Rewrite
			era = PEra.E3;
			// -- disconnect or server ping --
			String text = "{\"text\":\"NETTY NOT SUPPORTED\",\"description\":\"NETTY NOT SUPPORTED\"}";
			out.writeByte(text.length() + 2);
			out.writeByte(0x00);
			out.writeByte(text.length());
			out.writeBytes(text);
			// -- client ping response --
			out.writeByte(0x09);
			out.writeByte(0x01);
			out.writeLong(0);
			out.flush();
			try {
				// otherwise there are massive bugs
				Thread.sleep(5000);
			} catch (InterruptedException e) {
			}
			throw new IOException("this is probably the Netty rewrite");
		}
	}
}
