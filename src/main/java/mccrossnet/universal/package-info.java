/**
 * Essentially, consider the contents of this package a sort of "extension library".
 * Ideally, it'd *actually be* an extension library.
 */
package mccrossnet.universal;