package mccrossnet.universal;

import mccrossnet.base.netarch.hub.HubProtocol;
import mccrossnet.protocol.era0.v7.ZE0V7Codec;
import mccrossnet.protocol.era1.v13.ZE1V13Codec;
import mccrossnet.protocol.era1.v14.ZE1V14Codec;
import mccrossnet.protocol.era2.alpha.v1.ZE2V1Codec;
import mccrossnet.protocol.era2.alpha.v2.ZE2V2Codec;
import mccrossnet.protocol.era2.alpha.v3.ZE2V3Codec;
import mccrossnet.protocol.era2.alpha.v4.ZE2V4Codec;
import mccrossnet.protocol.era2.alpha.v5.ZE2V5Codec;
import mccrossnet.protocol.era2.alpha.v6.ZE2V6Codec;
import mccrossnet.protocol.era2.beta.v7.ZE2V7Codec;
import mccrossnet.protocol.era2.beta.v8.ZE2V8Codec;
import mccrossnet.protocol.era2.beta.v9.ZE2V9Codec;
import mccrossnet.protocol.era2.beta.w10.ZE2V10Codec;
import mccrossnet.protocol.era2.beta.w11.ZE2V11Codec;
import mccrossnet.protocol.era2.beta.w13.ZE2V13Codec;
import mccrossnet.protocol.era2.beta.w14.ZE2V14Codec;
import mccrossnet.protocol.era2.beta.w17.ZE2V17Codec;

public enum PVersion {
	// THESE MUST BE IN INCREASING ORDER
	// E0
	E0V7("c0.30_01c", 0, 7, new ZE0V7Codec()),
	// E1
	// --- THESE VERSIONS REQUIRE LEGACY MERGE SORT "-Djava.util.Arrays.useLegacyMergeSort=true" ---
	// --- THESE VERSIONS HAVE THE 'LIGHTING CRASH' (which makes them essentially unusable) ---
	E1V13("Alpha v1.0.15", 1, 13, new ZE1V13Codec()),
	E1V14("Alpha v1.0.16", 1, 14, new ZE1V14Codec()),
	E2V1("Alpha v1.0.17", 2, 1, new ZE2V1Codec()),
	E2V2("Alpha v1.1.x", 2, 2, new ZE2V2Codec()),
	E2V3("Alpha v1.2.0 / .1", 2, 3, new ZE2V3Codec()),
	E2V4("Alpha v1.2.2", 2, 4, new ZE2V4Codec()),
	// --- THE LIGHTING CRASH IS FIXED HERE ---
	E2V5("Alpha v1.2.3 (not _05)", 2, 5, new ZE2V5Codec()),
	E2V6("Alpha v1.2.3_05 - .6", 2, 6, new ZE2V6Codec()),
	E2V7("Beta v1.0 / .1 (not _02)", 2, 7, new ZE2V7Codec()),
	E2V8("Beta v1.1_02 / .2", 2, 8, new ZE2V8Codec()),
	E2V9("Beta v1.3", 2, 9, new ZE2V9Codec()),
	E2V10("Beta v1.4", 2, 10, new ZE2V10Codec()),
	E2V11("Beta v1.5", 2, 11, new ZE2V11Codec()),
	// test build (basically a snapshot) skipped
	E2V13("Beta v1.6.x", 2, 13, new ZE2V13Codec()),
	E2V14("Beta v1.7.x", 2, 14, new ZE2V14Codec()),
	// prereleases skipped
	E2V17("Beta v1.8.x", 2, 17, new ZE2V17Codec()),
	// --- NYI ---
	E2V22("1.0.0", 2, 22, null),
	E2V23("1.1", 2, 23, null),
	E2V28("1.2.0 - .3", 2, 28, null),
	E2V29("1.2.4 - .5", 2, 29, null),
	E2V39("1.3.1 - .2", 2, 39, null),
	E2V47("1.4.2", 2, 47, null),
	E2V49("1.4.4 / .5", 2, 49, null),
	E2V51("1.4.6 / .7", 2, 51, null),
	E2V60("1.5.0 / .1", 2, 60, null),
	E2V61("1.5.2", 2, 61, null),
	E2V73("1.6.1", 2, 73, null),
	E2V74("1.6.2", 2, 74, null),
	E2V78("1.6.4", 2, 78, null),
	// SKIPPED A LOT OF STUFF TO SAVE TIME IN THIS EARLY VERSION
	E3V4("1.7.2 - .5", 3, 5, null);

	public final String name;
	public final PEra era;
	public final int version;
	public final HubProtocol codec;
	
	private PVersion(String name, int era, int version, HubProtocol codec) {
		this.name = name;
		this.era = PEra.values()[era];
		this.version = version;
		this.codec = codec;
		this.era.versionsInternal.add(this);
	}
	
	static void attach() {
		// triggers class load from PEra
	}
	
	// It might be a good idea to phase this out from usage within packets
	public boolean before(PVersion other) {
		return before(other.era, other.version);
	}
	
	public boolean before(PEra otherEra, int otherVersion) {
		if (era.before(otherEra))
			return true;
		if (otherEra.before(era))
			return false;
		return version < otherVersion;
	}
}
