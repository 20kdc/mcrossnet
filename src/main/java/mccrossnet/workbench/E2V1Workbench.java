package mccrossnet.workbench;

import java.io.IOException;

import mccrossnet.base.core.Connection;
import mccrossnet.base.core.Packet;
import mccrossnet.protocol.era2.alpha.v1.BESpawnMob;
import mccrossnet.protocol.era2.alpha.v2.BSpawnPosition;
import mccrossnet.protocol.era2.alpha.v5.BPlayerHealth;
import mccrossnet.protocol.era2.alpha.v5.BRespawnCtos;
import mccrossnet.protocol.era2.alpha.v5.BRespawnStoc;
import mccrossnet.universal.Handshaker;

public class E2V1Workbench extends E1V14Workbench {

	public E2V1Workbench(Connection codec, Handshaker startState) {
		super(codec, startState);
		commands.put("summon", new Command() {
			int dyn = EID.SC_DYNAMIC_MIN;
			@Override
			public void run(String... args) throws IOException {
				BESpawnMob mob = new BESpawnMob();
				mob.position.setFixed(8, 70, 8);
				mob.entityID = dyn++;
				mob.type = (byte) Integer.parseInt(args[0]);
				codec.write(mob);
			}
			
			@Override
			public String getHelp() {
				return "summons a mob by network ID";
			}
		});
	}

}
