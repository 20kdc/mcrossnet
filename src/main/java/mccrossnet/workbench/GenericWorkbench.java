package mccrossnet.workbench;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import mccrossnet.base.core.Connection;
import mccrossnet.base.core.Packet;
import mccrossnet.protocol.all.PingKeepAlive;
import mccrossnet.protocol.era0.v7.BETeleport;
import mccrossnet.protocol.era0.v7.ZE0V7IDs;
import mccrossnet.protocol.era1.v13.BPlayerOnGround;
import mccrossnet.protocol.era2.beta.w13.BEffect;
import mccrossnet.protocol.era2.beta.w13.ZE2V13Codec;
import mccrossnet.protocol.era2.beta.w13.ZE2V13IDs;
import mccrossnet.universal.Handshaker;
import mccrossnet.workbench.scenario.parts.ChatScenarioStep;
import mccrossnet.workbench.scenario.parts.PacketScenarioStep;
import mccrossnet.workbench.scenario.parts.ScenarioStep;

/*
 * Represents a generic "workbench" player connection.
 */
public abstract class GenericWorkbench {
	public final WorkbenchCoverageSpy codec;
	public Handshaker startState;
	private final BlockingQueue<Packet> incomingPacketDeque = new ArrayBlockingQueue<Packet>(32);
	public final HashMap<String, Command> commands = new HashMap<>();
	public final HashMap<String, LinkedList<ScenarioStep>> scenarioes = new HashMap<>();
	private LinkedList<ScenarioStep> scenarioRuntime = new LinkedList<>();
	private boolean scenarioRuntimeBegun = false;
	
	/**
	 * Little security measure.
	 * If false, RobotScenarioStep merely asks the player to do what it wants.
	 */
	public boolean localSecure = false;
	
	public static final byte[] WORLD_CONTENT, WORLD_SKYLIGHT;
	
	static {
		// prep world chunk
		byte[] data = new byte[128 * 16 * 16];
		byte[] light = new byte[128 * 16 * 16];
		for (int p = 0; p < data.length; p += 128) {
			data[p] = ZE0V7IDs.B_BEDROCK;
			for (int l = 1; l < 64; l++)
				data[p + l] = ZE0V7IDs.B_DIRT;
			data[p + 64] = ZE0V7IDs.B_GRASS;
			for (int l = 65; l < 128; l++)
				light[p + l] = 15;
		}
		WORLD_CONTENT = data;
		WORLD_SKYLIGHT = light;
	}
	
	public GenericWorkbench(Connection c, Handshaker s) {
		codec = new WorkbenchCoverageSpy(c);
		startState = s;
		commands.put("help", new Command() {
			public void run(String... args) throws IOException {
				for (String key : commands.keySet())
					sendChatMessage(key + " " + commands.get(key).getHelp());
			}
			
			public String getHelp() {
				return "Shows all commands.";
			}
		});
		commands.put("list", new Command() {
			public void run(String... args) throws IOException {
				for (String s : scenarioes.keySet())
					sendChatMessage(s);
			}
			
			public String getHelp() {
				return "Shows all tests.";
			}
		});
		commands.put("launch", new Command() {
			public void run(String... args) throws IOException {
				if (args.length != 1)
					return;
				LinkedList<ScenarioStep> sc = scenarioes.get(args[0]);
				if (sc != null) {
					scenarioRuntime.addAll(sc);
				} else {
					sendChatMessage("That scenario is not supported.");
				}
			}
			
			public String getHelp() {
				return "Launches a test.";
			}
		});
		commands.put("battery", new Command() {
			public void run(String... args) throws IOException {
				for (LinkedList<ScenarioStep> key : scenarioes.values())
					scenarioRuntime.addAll(key);
				scenarioRuntime.add(new ScenarioStep() {
					@Override
					public void begin(GenericWorkbench bench) throws IOException {
						for (String s : codec.coverageHint())
							sendChatMessage(s);
					}

					@Override
					public boolean tick(GenericWorkbench bench) throws IOException {
						return true;
					}
					
					@Override
					public void handle(GenericWorkbench bench, Packet o) throws IOException {
					}
				});
			}
			
			public String getHelp() {
				return "Launches all tests.";
			}
		});
	}

	/**
q	 * Automatically begins if that's not done already
	 * @throws IOException
	 */
	private ScenarioStep scenarioAutoBegin() throws IOException {
		if (!scenarioRuntime.isEmpty()) {
			ScenarioStep step = scenarioRuntime.getFirst();
			if (!scenarioRuntimeBegun) {
				step.begin(this);
				scenarioRuntimeBegun = true;
			}
			return step;
		}
		return null;
	}

	public final void run() throws IOException {
		AtomicBoolean killswitch = new AtomicBoolean();
		new Thread() {
			public final boolean filterDebug(Object o) {
				if (o instanceof BPlayerOnGround)
					return true;
				if (o instanceof PingKeepAlive)
					return true;
				if (o instanceof BETeleport)
					return true;
				return false;
			}
			@Override
			public final void run() {
				try {
					while (!killswitch.get()) {
						Packet o = codec.read();
						if (!filterDebug(o))
							System.out.println(o);
						incomingPacketDeque.put(o);
					}
				} catch (Exception e) {
					e.printStackTrace();
					killswitch.set(true);
				}
			}
		}.start();
		try {
			begin();
			while (!killswitch.get()) {
				Packet o = null;
				try {
					o = incomingPacketDeque.poll(50, TimeUnit.MILLISECONDS);
				} catch (InterruptedException e) {
				}
				if (o != null)
					handle(o);
				while (true) {
					ScenarioStep step = scenarioAutoBegin();
					if (step == null)
						break;
					if (step.tick(this)) {
						scenarioRuntime.removeFirst();
						scenarioRuntimeBegun = false;
					} else {
						break;
					}
				}
				sendKeepAlive(o);
			}
		} finally {
			killswitch.set(true);
		}
	}
	
	public abstract void begin() throws IOException;
	public final void handle(Packet o) throws IOException {
		ScenarioStep step = scenarioAutoBegin();
		if (step != null) {
			step.handle(this, o);
		} else {
			executeLimboMode(o);
		}
		distribute(o);
	}
	protected abstract void distribute(Packet o) throws IOException;
	protected abstract void executeLimboMode(Packet o) throws IOException;
	public abstract void sendKeepAlive(Packet cause) throws IOException;
	public abstract void sendChatMessage(String text) throws IOException;
	
	public final void receiveChatMessage(String text) throws IOException {
		String[] components = text.split(" ");
		Command cmd = commands.get(components[0]);
		if (cmd == null) {
			sendChatMessage("Unknown command '" + components[0] + "' (try 'help')");
		} else {
			sendChatMessage("'" + components[0] + "'...");
			String[] args = new String[components.length - 1];
			System.arraycopy(components, 1, args, 0, args.length);
			try {
				cmd.run(args);
				sendChatMessage("done!");
			} catch (Exception e2) {
				e2.printStackTrace();
				sendChatMessage("w-why?");
			}
		}
	}
	
	public interface Command {
		void run(String... args) throws IOException;
		String getHelp();
	}
}
