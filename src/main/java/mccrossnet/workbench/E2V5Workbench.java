package mccrossnet.workbench;

import java.io.IOException;

import mccrossnet.base.core.Connection;
import mccrossnet.base.core.Packet;
import mccrossnet.protocol.era2.alpha.v5.BPlayerHealth;
import mccrossnet.protocol.era2.alpha.v5.BRespawnCtos;
import mccrossnet.protocol.era2.alpha.v5.BRespawnStoc;
import mccrossnet.universal.Handshaker;

public class E2V5Workbench extends E2V2Workbench {

	public E2V5Workbench(Connection codec, Handshaker startState) {
		super(codec, startState);
	}
	
	@Override
	public void distribute(Packet o) throws IOException {
		super.distribute(o);
		handleRespawn(o);
	}
	
	public void handleRespawn(Object o) throws IOException {
		if (o instanceof BRespawnCtos) {
			// NOTE: This sends the player to spawn automatically
			codec.write(new BRespawnStoc());
		}
	}
}
