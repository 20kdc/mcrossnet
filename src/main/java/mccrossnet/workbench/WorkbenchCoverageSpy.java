package mccrossnet.workbench;

import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;

import mccrossnet.base.core.Connection;
import mccrossnet.base.core.Packet;
import mccrossnet.base.core.Protocol;
import mccrossnet.base.netarch.hub.HubConnection;
import mccrossnet.base.netarch.hub.HubSchematic;
import mccrossnet.protocol.all.ChatText;
import mccrossnet.protocol.all.DisconnectText;
import mccrossnet.protocol.all.PingKeepAlive;
import mccrossnet.protocol.era0.v7.AIdentificationCtos;
import mccrossnet.protocol.era0.v7.AIdentificationStoc;
import mccrossnet.protocol.era1.v13.AConnectPacketCtos;
import mccrossnet.protocol.era1.v13.AConnectPacketStoc;
import mccrossnet.protocol.era1.v14.AHandshakeCtos;
import mccrossnet.protocol.era1.v14.AHandshakeStoc;
import mccrossnet.protocol.era2.beta.w17.AServerListPing;

/**
 * Attempts to work out the test coverage.
 */
public class WorkbenchCoverageSpy implements Connection {
	public final Connection underlying;
	public final HashSet<Class> received = new HashSet<Class>();
	public final HashSet<Class> sent = new HashSet<Class>();
	
	public WorkbenchCoverageSpy(Connection c) {
		underlying = c;
	}
	
	@Override
	public Protocol getProtocol() {
		return underlying.getProtocol();
	}

	private boolean doesNotCount(Class c) {
		if (c == PingKeepAlive.class)
			return true;
		if (c == DisconnectText.class)
			return true;
		if (c == AConnectPacketCtos.class)
			return true;
		if (c == AConnectPacketStoc.class)
			return true;
		if (c == AIdentificationCtos.class)
			return true;
		if (c == AIdentificationStoc.class)
			return true;
		if (c == AHandshakeCtos.class)
			return true;
		if (c == AHandshakeStoc.class)
			return true;
		if (c == AServerListPing.class)
			return true;
		return false;
	}
	
	@Override
	public Packet read() throws IOException {
		Packet p = underlying.read();
		if (p != null)
			if (!doesNotCount(p.getClass()))
				received.add(p.getClass());
		return p;
	}

	@Override
	public void write(Packet packet) throws IOException {
		if (!doesNotCount(packet.getClass()))
			sent.add(packet.getClass());
		underlying.write(packet);
	}
	
	public void checkTypeHalf(String text, LinkedList<String> list, Class[] cls, HashSet<Class> clsSet) {
		int total = 0;
		for (int i = 0; i < cls.length; i++)
			if (cls[i] != null)
				if (!doesNotCount(cls[i]))
					total++;
		list.add(text + " " + clsSet.size() + " / " + total + ", missing:");
		String totalMissing = "";
		for (int i = 0; i < cls.length; i++)
			if (cls[i] != null)
				if (!doesNotCount(cls[i]))
					if (!clsSet.contains(cls[i]))
						totalMissing += " " + cls[i].getSimpleName();
		while (!totalMissing.isEmpty()) {
			int split = 119;
			if (totalMissing.length() < split)
				split = totalMissing.length();
			list.add(totalMissing.substring(0, split));
			totalMissing = totalMissing.substring(split);
		}
	}
	
	public String[] coverageHint() {
		LinkedList<String> list = new LinkedList<String>();
		if (underlying instanceof HubConnection) {
			HubSchematic sch = (HubSchematic) underlying.getProtocol();
			checkTypeHalf("Read", list, sch.packetTypesCtos, received);
			checkTypeHalf("Sent", list, sch.packetTypesStoc, sent);
		} else {
			list.add("Connection base not supported");
			list.add("Read types: " + received.size());
			list.add("Sent types: " + sent.size());
		}
		return list.toArray(new String[0]);
	}
}
