package mccrossnet.workbench;

import java.io.IOException;

import mccrossnet.base.core.Connection;
import mccrossnet.base.core.Packet;
import mccrossnet.protocol.era2.alpha.v2.BSpawnPosition;
import mccrossnet.protocol.era2.alpha.v5.BPlayerHealth;
import mccrossnet.protocol.era2.alpha.v5.BRespawnCtos;
import mccrossnet.protocol.era2.alpha.v5.BRespawnStoc;
import mccrossnet.universal.Handshaker;

public class E2V2Workbench extends E2V1Workbench {

	public E2V2Workbench(Connection codec, Handshaker startState) {
		super(codec, startState);
	}
	
	@Override
	public void begin() throws IOException {
		super.begin();
		codec.write(new BSpawnPosition(8, 64, 8));
	}

}
