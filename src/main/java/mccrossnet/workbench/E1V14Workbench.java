package mccrossnet.workbench;

import java.io.IOException;

import mccrossnet.base.core.Connection;
import mccrossnet.protocol.era1.v14.AHandshakeStoc;
import mccrossnet.universal.Handshaker;

public class E1V14Workbench extends E1V13Workbench {

	public E1V14Workbench(Connection codec, Handshaker startState) {
		super(codec, startState);
	}

	@Override
	public void begin() throws IOException {
		codec.write(new AHandshakeStoc());
		// same connect packet
		super.begin();
	}
}
