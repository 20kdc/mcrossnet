package mccrossnet.workbench;

/**
 * A set of EIDs.
 */
public class EID {
	public static final int PLAYER = 1;
	public static final int SC_OTHERPLAYER_MCTESTER = 2;
	public static final int SC_BOAT_BOAT = 3;
	public static final int SC_ITEM_ITEM = 4;
	public static final int SC_CREEPER_CREEPER = 5;
	public static final int SC_HEALTH_THUNDERBOLT = 6;
	public static final int SC_FOUNTAIN_PAINTING = 7;
	
	public static final int SC_FOUNTAIN_XP_MIN = 10000;
	public static final int SC_FOUNTAIN_XP_COUNT = 100;

	// may need to rework this at some point
	public static final int SC_DYNAMIC_MIN = 20000;
}
