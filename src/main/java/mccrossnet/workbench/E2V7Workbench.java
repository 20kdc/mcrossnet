package mccrossnet.workbench;

import java.io.IOException;

import mccrossnet.base.core.Connection;
import mccrossnet.base.core.Packet;
import mccrossnet.protocol.era2.beta.v7.BClickWindow;
import mccrossnet.protocol.era2.beta.v7.BClickWindowResponse;
import mccrossnet.universal.Handshaker;

public class E2V7Workbench extends E2V5Workbench {

	public E2V7Workbench(Connection codec, Handshaker startState) {
		super(codec, startState);
	}

	@Override
	public void distribute(Packet o) throws IOException {
		super.distribute(o);
		if (o instanceof BClickWindow) {
			BClickWindowResponse r = new BClickWindowResponse();
			r.accepted = true;
			r.actionID = ((BClickWindow) o).actionID;
			r.windowID = ((BClickWindow) o).windowID;
			codec.write(r);
		}
	}
}
