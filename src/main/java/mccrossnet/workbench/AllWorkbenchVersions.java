package mccrossnet.workbench;

import java.lang.reflect.Constructor;
import java.util.LinkedList;

import mccrossnet.base.core.Connection;
import mccrossnet.protocol.all.ChatText;
import mccrossnet.protocol.era1.v13.ZE1V13Codec;
import mccrossnet.protocol.era2.alpha.v1.ZE2V1Codec;
import mccrossnet.protocol.era2.alpha.v2.ZE2V2Codec;
import mccrossnet.protocol.era2.alpha.v5.ZE2V5Codec;
import mccrossnet.protocol.era2.beta.v7.ZE2V7Codec;
import mccrossnet.protocol.era2.beta.w17.ZE2V17Codec;
import mccrossnet.universal.Handshaker;
import mccrossnet.universal.PVersion;
import mccrossnet.workbench.scenario.BlockPlaceDigScenario;
import mccrossnet.workbench.scenario.BoatScenario;
import mccrossnet.workbench.scenario.CookingScenario;
import mccrossnet.workbench.scenario.CreeperScenario;
import mccrossnet.workbench.scenario.FountainScenario;
import mccrossnet.workbench.scenario.HealthScenario;
import mccrossnet.workbench.scenario.ItemScenario;
import mccrossnet.workbench.scenario.OtherPlayerScenario;
import mccrossnet.workbench.scenario.TimeScenario;
import mccrossnet.workbench.scenario.parts.ChatScenarioStep;
import mccrossnet.workbench.scenario.parts.ScenarioStep;

/**
 * This is used as a holding cell for information being removed from PVersion.
 */
public class AllWorkbenchVersions {

	/**
	 * Creates a workbench.
	 * 
	 * @param pv Version.
	 * @param createCodec Connection to attach the workbench to.
	 * @param result Handshaker result.
	 * @return A workbench or null if not possible.
	 */
	public static GenericWorkbench createWorkbench(PVersion pv, Connection createCodec, Handshaker result) {
		if (pv.before(PVersion.E1V13))
			return new E0V7Workbench(createCodec, result);
		if (pv.before(PVersion.E1V14))
			return new E1V13Workbench(createCodec, result);
		if (pv.before(PVersion.E2V2))
			return new E1V14Workbench(createCodec, result);
		if (pv.before(PVersion.E2V5))
			return new E2V2Workbench(createCodec, result);
		if (pv.before(PVersion.E2V7))
			return new E2V5Workbench(createCodec, result);
		if (pv.before(PVersion.E2V17))
			return new E2V7Workbench(createCodec, result);
		return new E2V17Workbench(createCodec, result);
	}
	
	public static void installScenario(GenericWorkbench bench) {
		bench.scenarioes.put("otherPlayer", OtherPlayerScenario.generate(bench));
		bench.scenarioes.put("block", BlockPlaceDigScenario.generate(bench));
		if (bench.codec.getProtocol() instanceof ZE1V13Codec) {
			bench.scenarioes.put("boat", BoatScenario.generate(bench));
			bench.scenarioes.put("item", ItemScenario.generate(bench));
			bench.scenarioes.put("fountain", FountainScenario.generate(bench));
		}
		if (bench.codec.getProtocol() instanceof ZE2V1Codec) {
			bench.scenarioes.put("time", TimeScenario.generate(bench));
			bench.scenarioes.put("creeper", CreeperScenario.generate(bench));
		}
		if (bench.codec.getProtocol() instanceof ZE2V5Codec)
			bench.scenarioes.put("health", HealthScenario.generate(bench));
		if (bench.codec.getProtocol() instanceof ZE2V7Codec)
			bench.scenarioes.put("cooking", CookingScenario.generate(bench));
	}

}
