package mccrossnet.workbench;

import java.io.IOException;
import java.util.Random;

import mccrossnet.base.core.Connection;
import mccrossnet.base.core.Packet;
import mccrossnet.protocol.all.ChatText;
import mccrossnet.protocol.all.Face;
import mccrossnet.protocol.all.PingKeepAlive;
import mccrossnet.protocol.all.items.ICDItemStack;
import mccrossnet.protocol.era0.v7.BELook;
import mccrossnet.protocol.era0.v7.BERelativeMove;
import mccrossnet.protocol.era0.v7.BESpawnPlayer;
import mccrossnet.protocol.era0.v7.BETeleport;
import mccrossnet.protocol.era0.v7.BSetBlock;
import mccrossnet.protocol.era0.v7.ZE0V7IDs;
import mccrossnet.protocol.era1.v13.AConnectPacketStoc;
import mccrossnet.protocol.era1.v13.BEAnimateItemPickup;
import mccrossnet.protocol.era1.v13.BEPlayerAction;
import mccrossnet.protocol.era1.v13.BEHeldItem;
import mccrossnet.protocol.era1.v13.BESpawnItem;
import mccrossnet.protocol.era1.v13.BESpawnObject;
import mccrossnet.protocol.era1.v13.BGiveItem;
import mccrossnet.protocol.era1.v13.BChunkControl;
import mccrossnet.protocol.era1.v13.BChunkData;
import mccrossnet.protocol.era1.v13.BPlayerDigsBlock;
import mccrossnet.protocol.era1.v13.BPlayerOnGround;
import mccrossnet.protocol.era1.v13.BPlayerPlaces;
import mccrossnet.protocol.era1.v13.BPlayerStateStoc;
import mccrossnet.protocol.era1.v13.ZE1V13IDs;
import mccrossnet.universal.Handshaker;
import mccrossnet.workbench.GenericWorkbench.Command;

public class E1V13Workbench extends GenericWorkbench {
	
	public E1V13Workbench(Connection codec, Handshaker startState) {
		super(codec, startState);
	}

	@Override
	public void begin() throws IOException {
		AConnectPacketStoc acp = new AConnectPacketStoc();
		// client doesn't really care about any of this
		// except player EID
		acp.playerEID = 1;
		codec.write(acp);
		Random r = new Random();
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				BChunkControl mcc = new BChunkControl();
				mcc.x = i;
				mcc.z = j;
				mcc.load = true;
				codec.write(mcc);
				byte[] empty = new byte[WORLD_CONTENT.length];
				BChunkData bmcd = BChunkData.createFromUnpacked(i, j, WORLD_CONTENT, empty, empty, WORLD_SKYLIGHT);
				codec.write(bmcd);
			}
		}
		BPlayerStateStoc stoc = new BPlayerStateStoc();
		stoc.x = 8;
		stoc.y = 66;
		stoc.yTop = 67.5;
		stoc.z = 8;
		stoc.pitch = 0;
		stoc.yaw = 0;
		stoc.onGround = false;
		codec.write(stoc);
	}
	
	@Override
	public void sendKeepAlive(Packet cause) throws IOException {
		codec.write(new PingKeepAlive());
	}
	
	@Override
	public void distribute(Packet o) throws IOException {
		if (o instanceof ChatText) {
			String text = ((ChatText) o).text;
			receiveChatMessage(text);
		}
	}
	
	@Override
	public void sendChatMessage(String text) throws IOException {
		codec.write(new ChatText(text));
	}

	@Override
	protected void executeLimboMode(Packet o) throws IOException {
		if (o instanceof BPlayerPlaces) {
			BPlayerPlaces bpp = (BPlayerPlaces) o;
			if ((bpp.item != null) && (bpp.item.id < 256)) {
				BSetBlock bsb = new BSetBlock();
				bsb.type = (byte) bpp.item.id;
				bsb.meta = (byte) bpp.item.durability;
				bsb.x = bpp.x;
				bsb.y = bpp.y;
				bsb.z = bpp.z;
				if (bpp.face == Face.XM.ordinal())
					bsb.x--;
				if (bpp.face == Face.XP.ordinal())
					bsb.x++;
				if (bpp.face == Face.YM.ordinal())
					bsb.y--;
				if (bpp.face == Face.YP.ordinal())
					bsb.y++;
				if (bpp.face == Face.ZM.ordinal())
					bsb.z--;
				if (bpp.face == Face.ZP.ordinal())
					bsb.z++;
				codec.write(bsb);
			}
		} else if (o instanceof BPlayerDigsBlock) {
			BPlayerDigsBlock pdb = (BPlayerDigsBlock) o;
			BSetBlock bsb = new BSetBlock();
			bsb.x = pdb.x;
			bsb.y = pdb.y;
			bsb.z = pdb.z;
			bsb.type = 0;
			bsb.meta = 0;
			codec.write(bsb);
		}
	}
}
