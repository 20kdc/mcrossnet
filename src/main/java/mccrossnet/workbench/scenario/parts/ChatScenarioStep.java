package mccrossnet.workbench.scenario.parts;

import java.io.IOException;

import mccrossnet.base.core.Packet;
import mccrossnet.workbench.GenericWorkbench;

/**
 * Represents a scenario step where chat goes to the player.
 */
public class ChatScenarioStep implements ScenarioStep {
	public String text;
	
	public ChatScenarioStep(String string) {
		text = string;
	}

	@Override
	public void begin(GenericWorkbench bench) throws IOException {
		bench.sendChatMessage(text);
	}

	@Override
	public boolean tick(GenericWorkbench bench) throws IOException {
		return true;
	}

	@Override
	public void handle(GenericWorkbench bench, Packet o) throws IOException {
		
	}
}
