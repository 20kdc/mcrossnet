package mccrossnet.workbench.scenario.parts;

import mccrossnet.base.core.Packet;
import mccrossnet.workbench.GenericWorkbench;

/**
 * Causes the scenario to sleep.
 */
public class SleepScenarioStep implements ScenarioStep {

	public long when;
	
	public final long howLong;
	
	public SleepScenarioStep(long l) {
		howLong = l;
	}
	
	@Override
	public void begin(GenericWorkbench base) {
		when = System.currentTimeMillis() + howLong;
	}
	
	@Override
	public boolean tick(GenericWorkbench base) {
		return System.currentTimeMillis() >= when;
	}

	@Override
	public void handle(GenericWorkbench base, Packet o) {
		
	}

}
