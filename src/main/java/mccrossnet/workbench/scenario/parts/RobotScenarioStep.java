package mccrossnet.workbench.scenario.parts;

import java.awt.DisplayMode;
import java.awt.GraphicsEnvironment;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;

import mccrossnet.base.core.Packet;
import mccrossnet.protocol.era2.alpha.v4.ZE2V4Codec;
import mccrossnet.protocol.era2.beta.w13.ZE2V13Codec;
import mccrossnet.protocol.era2.beta.w14.ZE2V14Codec;
import mccrossnet.workbench.GenericWorkbench;

/**
 * (mis)-uses an AWT Robot.
 * Waits half a second to ensure any preparation steps are complete,
 *  then performs the action and immediately advances to ensure resulting packets are captured.
 */
public class RobotScenarioStep implements ScenarioStep {
	private long timer;
	public final RobotAction task;
	
	public RobotScenarioStep(RobotAction t) {
		task = t;
	}
	
	@Override
	public void begin(GenericWorkbench bench) throws IOException {
		timer = System.currentTimeMillis();
		if (!bench.localSecure) {
			bench.sendChatMessage(task.text);
			bench.sendChatMessage("You have 10 seconds.");
		}
	}

	@Override
	public boolean tick(GenericWorkbench bench) throws IOException {
		if (timer == -1)
			return false;
		long now = System.currentTimeMillis();
		if (bench.localSecure) {
			if (now < timer + 500)
				return false;
			try {
				Robot r = new Robot();
				DisplayMode dm = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode();
				int centreX = (dm.getWidth() / 2);
				int centreY = (dm.getHeight() / 2);
				int offsetX = centreX - (1366 / 2);
				int offsetY = centreY - (768 / 2);
				
				int invKey = (bench.codec.getProtocol() instanceof ZE2V13Codec) ? KeyEvent.VK_E : KeyEvent.VK_I;
				
				switch (task) {
				case LeftClick:
					r.mousePress(InputEvent.BUTTON1_MASK);
					r.mouseRelease(InputEvent.BUTTON1_MASK);
					break;
				case RightClick:
					r.mousePress(InputEvent.BUTTON3_MASK);
					r.mouseRelease(InputEvent.BUTTON3_MASK);
					break;
				case Drop:
					r.keyPress(KeyEvent.VK_Q);
					r.keyRelease(KeyEvent.VK_Q);
					break;
				case Press1:
					r.keyPress(KeyEvent.VK_1);
					r.keyRelease(KeyEvent.VK_1);
					break;
				case Press2:
					r.keyPress(KeyEvent.VK_2);
					r.keyRelease(KeyEvent.VK_2);
					break;
				case TransferSlot1ToSlot2:
					r.keyPress(invKey);
					r.keyRelease(invKey);
					r.delay(100);
					r.mouseMove(offsetX + 540, offsetY + 506);
					r.delay(100);
					r.mousePress(InputEvent.BUTTON1_MASK);
					r.mouseRelease(InputEvent.BUTTON1_MASK);
					r.delay(100);
					r.mouseMove(offsetX + 577, offsetY + 506);
					r.delay(100);
					r.mousePress(InputEvent.BUTTON1_MASK);
					r.mouseRelease(InputEvent.BUTTON1_MASK);
					r.delay(100);
					r.keyPress(KeyEvent.VK_ESCAPE);
					r.keyRelease(KeyEvent.VK_ESCAPE);
					break;
				case BreakSign:
					r.mousePress(InputEvent.BUTTON1_MASK);
					r.delay(2000);
					r.mouseRelease(InputEvent.BUTTON1_MASK);
					break;
				case FillInSign:
					r.keyPress(KeyEvent.VK_A);
					r.keyRelease(KeyEvent.VK_A);
					r.mouseMove(offsetX + 686, offsetY + 514);
					r.delay(100);
					r.mousePress(InputEvent.BUTTON1_MASK);
					r.mouseRelease(InputEvent.BUTTON1_MASK);
					break;
				case Respawn:
					r.mouseMove(offsetX + 686, offsetY + 415);
					r.delay(100);
					r.mousePress(InputEvent.BUTTON1_MASK);
					r.mouseRelease(InputEvent.BUTTON1_MASK);
					break;
				default:
					throw new RuntimeException("Whoops!");
				}
			} catch (Exception e) {
				e.printStackTrace();
				bench.localSecure = false;
				begin(bench);
				return false;
			}
		} else {
			if (now < timer + 10000)
				return false;
			// done!
		}
		return true;
	}

	@Override
	public void handle(GenericWorkbench bench, Packet o) throws IOException {
		
	}

}
