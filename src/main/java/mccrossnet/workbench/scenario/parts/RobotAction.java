package mccrossnet.workbench.scenario.parts;

/**
 * The set of test actions that can be done by the robot, or by a human if necessary.
 */
public enum RobotAction {
	LeftClick("Please left-click."),
	RightClick("Please right-click."),
	TransferSlot1ToSlot2("Please transfer the contents of slot 1 to slot 2."),
	Press1("Please select slot 1."),
	Press2("Please select slot 2."),
	Drop("Please drop your current item."),
	FillInSign("Please fill in the sign and click 'Done'."),
	Respawn("Please respawn."),
	BreakSign("Please break the sign."),
	BreakClassic("Please break the block.");
	public final String text;
	
	RobotAction(String s) {
		text = s;
	}
}
