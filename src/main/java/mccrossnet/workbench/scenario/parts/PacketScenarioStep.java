package mccrossnet.workbench.scenario.parts;

import java.io.IOException;

import mccrossnet.base.core.Packet;
import mccrossnet.workbench.GenericWorkbench;

/**
 * Scenario step to emit a specific packet.
 */
public class PacketScenarioStep implements ScenarioStep {
	public final Packet packet;

	public PacketScenarioStep(Packet p) {
		packet = p;
	}
	
	@Override
	public void begin(GenericWorkbench bench) throws IOException {
		bench.codec.write(packet);
	}

	@Override
	public boolean tick(GenericWorkbench bench) {
		return true;
	}

	@Override
	public void handle(GenericWorkbench bench, Packet o) {
		
	}

}
