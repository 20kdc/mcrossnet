package mccrossnet.workbench.scenario.parts;

import java.io.IOException;

import mccrossnet.base.core.Packet;
import mccrossnet.workbench.GenericWorkbench;

/**
 * Represents a running step of a scenario.
 */
public interface ScenarioStep {
	/**
	 * Begins this ScenarioStep.
	 * @throws IOException 
	 */
	public void begin(GenericWorkbench bench) throws IOException;
	/**
	 * Ticks this ScenarioStep.
	 * @return True when the step has completed.
	 * @throws IOException 
	 */
	public boolean tick(GenericWorkbench bench) throws IOException;
	/**
	 * Called when a packet is received during a scenario.
	 * @param o The packet.
	 * @throws IOException 
	 */
	public void handle(GenericWorkbench bench, Packet o) throws IOException;
}
