package mccrossnet.workbench.scenario.parts;

import java.io.IOException;

import mccrossnet.base.core.Packet;
import mccrossnet.protocol.era0.v7.BETeleport;
import mccrossnet.protocol.era1.v13.BPlayerStateStoc;
import mccrossnet.protocol.era1.v13.ZE1V13Codec;
import mccrossnet.workbench.GenericWorkbench;

/**
 * Gets the player into a specific position.
 * This is the pragmatic way; BoatScenario won't use this for coverage reasons.
 */
public class TeleportScenarioStep implements ScenarioStep {
	/**
	 * Bit awkward, but as a starting format it's fine
	 */
	public final BPlayerStateStoc intendedState = new BPlayerStateStoc();
	
	public TeleportScenarioStep(float x, float y, float z, float pitch, float yaw) {
		intendedState.x = x;
		intendedState.y = y;
		intendedState.yTop = y + 1.6;
		intendedState.z = z;
		intendedState.pitch = pitch;
		intendedState.yaw = yaw;
	}
	
	@Override
	public void begin(GenericWorkbench bench) throws IOException {
		if (bench.codec.getProtocol() instanceof ZE1V13Codec) {
			bench.codec.write(intendedState);
		} else {
			BETeleport tp = new BETeleport();
			tp.position.setFixed(intendedState.x, intendedState.y, intendedState.z);
			tp.rotation.pitch = (byte) (intendedState.pitch / 360);
			tp.rotation.yaw = (byte) (intendedState.yaw / 360);
			tp.adjust(bench.codec.getProtocol());
			bench.codec.write(tp);
		}
	}

	@Override
	public boolean tick(GenericWorkbench bench) throws IOException {
		return true;
	}

	@Override
	public void handle(GenericWorkbench bench, Packet o) throws IOException {
	}

}
