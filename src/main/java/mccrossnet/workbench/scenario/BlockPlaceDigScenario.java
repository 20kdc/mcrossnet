package mccrossnet.workbench.scenario;

import java.util.LinkedList;

import mccrossnet.protocol.all.items.ICDItemStack;
import mccrossnet.protocol.era0.v7.BSetBlock;
import mccrossnet.protocol.era0.v7.ZE0V7IDs;
import mccrossnet.protocol.era1.v13.BGiveItem;
import mccrossnet.protocol.era1.v13.ZE1V13Codec;
import mccrossnet.protocol.era1.v13.ZE1V13IDs;
import mccrossnet.protocol.era2.beta.v7.BSetWindowSlot;
import mccrossnet.protocol.era2.beta.v7.ZE2V7Codec;
import mccrossnet.workbench.GenericWorkbench;
import mccrossnet.workbench.scenario.parts.PacketScenarioStep;
import mccrossnet.workbench.scenario.parts.RobotAction;
import mccrossnet.workbench.scenario.parts.RobotScenarioStep;
import mccrossnet.workbench.scenario.parts.ScenarioStep;
import mccrossnet.workbench.scenario.parts.SleepScenarioStep;
import mccrossnet.workbench.scenario.parts.TeleportScenarioStep;

/**
 * A scenario where a block is placed and then dug.
 */
public class BlockPlaceDigScenario {
	public static LinkedList<ScenarioStep> generate(GenericWorkbench bench) {
		LinkedList<ScenarioStep> list = new LinkedList<ScenarioStep>();
		list.add(new TeleportScenarioStep(1.51f, 67, 1.5f, 45, 135));
		list.add(new SleepScenarioStep(500));
		if (bench.codec.getProtocol() instanceof ZE2V7Codec) {
			list.add(new PacketScenarioStep(new BSetWindowSlot((byte) 0, (short) 36, ICDItemStack.simple(ZE1V13IDs.I_SIGN, 1))));
		} else if (bench.codec.getProtocol() instanceof ZE1V13Codec) {
			list.add(new PacketScenarioStep(new BGiveItem(ICDItemStack.simple(ZE1V13IDs.I_SIGN, 1))));
		}
		list.add(new RobotScenarioStep(RobotAction.Press1));
		list.add(new RobotScenarioStep(RobotAction.RightClick));
		list.add(new SleepScenarioStep(100));
		if (bench.codec.getProtocol() instanceof ZE1V13Codec) {
			list.add(new PacketScenarioStep(new BSetBlock(0, (short) 65, 0, ZE1V13IDs.B_SIGN_STANDING, (byte) 13)));
			list.add(new RobotScenarioStep(RobotAction.FillInSign));
		} else {
			list.add(new PacketScenarioStep(new BSetBlock(0, (short) 65, 0, ZE0V7IDs.B_GLASS, (byte) 0)));
		}
		list.add(new SleepScenarioStep(100));
		if (bench.codec.getProtocol() instanceof ZE2V7Codec) {
			// This is just good cleanup policy on later versions
			list.add(new PacketScenarioStep(new BSetWindowSlot((byte) 0, (short) 36, null)));
			list.add(new RobotScenarioStep(RobotAction.BreakSign));
		} else if (bench.codec.getProtocol() instanceof ZE1V13Codec) {
			list.add(new RobotScenarioStep(RobotAction.BreakSign));
		} else {
			list.add(new RobotScenarioStep(RobotAction.LeftClick));
		}
		list.add(new SleepScenarioStep(100));
		list.add(new PacketScenarioStep(new BSetBlock(0, (short) 65, 0, ZE0V7IDs.B_AIR, (byte) 0)));
		return list;
	}
}
