package mccrossnet.workbench.scenario;

import java.util.LinkedList;

import mccrossnet.base.core.Packet;
import mccrossnet.base.core.Protocol;
import mccrossnet.protocol.era0.v7.BEDelete;
import mccrossnet.protocol.era0.v7.BETeleport;
import mccrossnet.protocol.era0.v7.BSetBlock;
import mccrossnet.protocol.era1.v13.BMultipleBlockChange;
import mccrossnet.protocol.era1.v13.ZE1V13IDs;
import mccrossnet.protocol.era2.alpha.v2.ZE2V2Codec;
import mccrossnet.protocol.era2.alpha.v5.BPlayerHealth;
import mccrossnet.protocol.era2.beta.v7.BCloseWindow;
import mccrossnet.protocol.era2.beta.v7.BOpenWindow;
import mccrossnet.protocol.era2.beta.v7.BUpdateSign;
import mccrossnet.protocol.era2.beta.v7.BWindowProgress;
import mccrossnet.protocol.era2.beta.v7.ZE2V7Codec;
import mccrossnet.protocol.era2.beta.v7.ZE2V7IDs;
import mccrossnet.protocol.era2.beta.v8.BBlockDoesAThing;
import mccrossnet.protocol.era2.beta.v8.ZE2V8Codec;
import mccrossnet.protocol.era2.beta.v8.ZE2V8IDs;
import mccrossnet.protocol.era2.beta.w11.BAddToStatistic;
import mccrossnet.protocol.era2.beta.w11.BEThunderbolt;
import mccrossnet.protocol.era2.beta.w11.ZE2V11Codec;
import mccrossnet.protocol.era2.beta.w17.BESpawnShinies;
import mccrossnet.protocol.era2.beta.w17.BPlayerExperience;
import mccrossnet.protocol.era2.beta.w17.BWhoListEntry;
import mccrossnet.protocol.era2.beta.w17.ZE2V17Codec;
import mccrossnet.workbench.EID;
import mccrossnet.workbench.GenericWorkbench;
import mccrossnet.workbench.scenario.parts.ChatScenarioStep;
import mccrossnet.workbench.scenario.parts.PacketScenarioStep;
import mccrossnet.workbench.scenario.parts.RobotAction;
import mccrossnet.workbench.scenario.parts.RobotScenarioStep;
import mccrossnet.workbench.scenario.parts.ScenarioStep;
import mccrossnet.workbench.scenario.parts.SleepScenarioStep;
import mccrossnet.workbench.scenario.parts.TeleportScenarioStep;

/**
 * Window packets.
 */
public class CookingScenario {
	public static LinkedList<ScenarioStep> generate(GenericWorkbench bench) {
		LinkedList<ScenarioStep> list = new LinkedList<ScenarioStep>();
		BOpenWindow wnd = new BOpenWindow();
		wnd.windowID = 1;
		wnd.slots = 39;
		wnd.type = ZE2V7IDs.W_FURNACE;
		wnd.title = "Cooking";
		list.add(new PacketScenarioStep(wnd));
		BWindowProgress wp = new BWindowProgress();
		wp.windowID = 1;
		wp.barID = 1;
		wp.value = 100;
		list.add(new PacketScenarioStep(wp));
		list.add(new SleepScenarioStep(1000));
		list.add(new PacketScenarioStep(new BCloseWindow((byte) 1)));
		return list;
	}
}
