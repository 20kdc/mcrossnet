package mccrossnet.workbench.scenario;

import java.util.LinkedList;

import mccrossnet.protocol.all.items.ICDItemStack;
import mccrossnet.protocol.era0.v7.BEDelete;
import mccrossnet.protocol.era0.v7.BELook;
import mccrossnet.protocol.era0.v7.BELookRelativeMove;
import mccrossnet.protocol.era0.v7.BERelativeMove;
import mccrossnet.protocol.era0.v7.BESpawnPlayer;
import mccrossnet.protocol.era0.v7.BETeleport;
import mccrossnet.protocol.era0.v7.BSetBlock;
import mccrossnet.protocol.era0.v7.ZE0V7IDs;
import mccrossnet.protocol.era1.v13.BEPlayerAction;
import mccrossnet.protocol.era1.v13.BEHeldItem;
import mccrossnet.protocol.era1.v13.BESpawnObject;
import mccrossnet.protocol.era1.v13.BPlayerLook;
import mccrossnet.protocol.era1.v13.BPlayerPosition;
import mccrossnet.protocol.era1.v13.ZE1V13Codec;
import mccrossnet.protocol.era1.v13.ZE1V13IDs;
import mccrossnet.protocol.era2.alpha.v4.BEAttachEntity;
import mccrossnet.protocol.era2.alpha.v4.ZE2V4Codec;
import mccrossnet.protocol.era2.beta.v7.BESlot;
import mccrossnet.protocol.era2.beta.v7.ZE2V7Codec;
import mccrossnet.workbench.EID;
import mccrossnet.workbench.GenericWorkbench;
import mccrossnet.workbench.scenario.parts.ChatScenarioStep;
import mccrossnet.workbench.scenario.parts.PacketScenarioStep;
import mccrossnet.workbench.scenario.parts.RobotAction;
import mccrossnet.workbench.scenario.parts.RobotScenarioStep;
import mccrossnet.workbench.scenario.parts.ScenarioStep;
import mccrossnet.workbench.scenario.parts.SleepScenarioStep;

/**
 * Flying boat test!
 */
public class BoatScenario {
	public static LinkedList<ScenarioStep> generate(GenericWorkbench bench) {
		LinkedList<ScenarioStep> list = new LinkedList<ScenarioStep>();
		
		BESpawnObject boat = new BESpawnObject();
		boat.entityID = EID.SC_BOAT_BOAT;
		boat.type = ZE1V13IDs.O_BOAT;
		boat.position.setFixed(0, 65, 0);
		list.add(new PacketScenarioStep(boat));

		// Handle it this way for better coverage
		BPlayerLook bpl = new BPlayerLook();
		bpl.pitch = 22.5f;
		bpl.yaw = 135;
		list.add(new PacketScenarioStep(bpl));
		BPlayerPosition bpp = new BPlayerPosition();
		bpp.x = 2;
		bpp.y = 67;
		bpp.yTop = 68.5;
		bpp.z = 2;
		list.add(new PacketScenarioStep(bpp));

		// It's useful to do this left-click because it causes a BEAnimationA packet.
		list.add(new RobotScenarioStep(RobotAction.LeftClick));
		list.add(new RobotScenarioStep(RobotAction.RightClick));
		// we should get the mounted packet, but some versions don't do it.
		// just sleep through it.
		list.add(new SleepScenarioStep(1000));
		if (bench.codec.getProtocol() instanceof ZE2V4Codec)
			list.add(new PacketScenarioStep(new BEAttachEntity(EID.PLAYER, boat.entityID)));

		// Use BETeleport for this since OtherPlayer tests relative stuff
		for (int i = 0; i < 8; i++) {
			list.add(new SleepScenarioStep(125));
			BETeleport tp = new BETeleport();
			tp.entityID = boat.entityID;
			tp.position.setFixed(i, 66 + i, i);
			tp.rotation.yaw = (byte) (i * 10);
			list.add(new PacketScenarioStep(tp));
		}
		
		// But adding this bit to OtherPlayer would make it too long so
		for (int i = 0; i < 80; i++) {
			list.add(new SleepScenarioStep(50));
			BELookRelativeMove tp = new BELookRelativeMove();
			tp.entityID = boat.entityID;
			tp.positionDelta.setFixed(0, 0, 0.5);
			tp.rotation.yaw = (byte) (i * 3);
			list.add(new PacketScenarioStep(tp));
		}
		
		list.add(new SleepScenarioStep(1000));
		list.add(new PacketScenarioStep(new BEDelete(boat.entityID)));
		return list;
	}
}
