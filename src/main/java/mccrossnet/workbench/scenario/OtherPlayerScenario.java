package mccrossnet.workbench.scenario;

import java.util.LinkedList;

import mccrossnet.protocol.all.items.ICDItemStack;
import mccrossnet.protocol.era0.v7.BEDelete;
import mccrossnet.protocol.era0.v7.BELook;
import mccrossnet.protocol.era0.v7.BERelativeMove;
import mccrossnet.protocol.era0.v7.BESpawnPlayer;
import mccrossnet.protocol.era0.v7.BSetBlock;
import mccrossnet.protocol.era0.v7.ZE0V7IDs;
import mccrossnet.protocol.era1.v13.BEPlayerAction;
import mccrossnet.protocol.era1.v13.BEHeldItem;
import mccrossnet.protocol.era1.v13.BPlayerLook;
import mccrossnet.protocol.era1.v13.BPlayerPosition;
import mccrossnet.protocol.era1.v13.ZE1V13Codec;
import mccrossnet.protocol.era1.v13.ZE1V13IDs;
import mccrossnet.protocol.era2.beta.v7.BESlot;
import mccrossnet.protocol.era2.beta.v7.ZE2V7Codec;
import mccrossnet.protocol.era2.beta.v9.BEUsedBed;
import mccrossnet.protocol.era2.beta.v9.ZE2V9Codec;
import mccrossnet.protocol.era2.beta.v9.ZE2V9IDs;
import mccrossnet.protocol.era2.beta.w14.ZE2V14Codec;
import mccrossnet.protocol.era2.beta.w17.BEAddEffect;
import mccrossnet.workbench.E1V13Workbench;
import mccrossnet.workbench.EID;
import mccrossnet.workbench.GenericWorkbench;
import mccrossnet.workbench.scenario.parts.PacketScenarioStep;
import mccrossnet.workbench.scenario.parts.ScenarioStep;
import mccrossnet.workbench.scenario.parts.SleepScenarioStep;
import mccrossnet.workbench.scenario.parts.TeleportScenarioStep;

/**
 * A pretty simple scenario for testing MP.
 */
public class OtherPlayerScenario {
	public static LinkedList<ScenarioStep> generate(GenericWorkbench bench) {
		LinkedList<ScenarioStep> list = new LinkedList<ScenarioStep>();
		
		list.add(new TeleportScenarioStep(8, 66, 8, 0, 135));
		
		BESpawnPlayer player = new BESpawnPlayer();
		player.entityID = EID.SC_OTHERPLAYER_MCTESTER;
		player.position.setFixed(0, 65, 0);
		player.adjust(bench.codec.getProtocol());
		player.rotation.pitch = 0;
		player.rotation.yaw = 0;
		player.username = "McTester";
		player.heldItem = ZE1V13IDs.I_BOOK;
		list.add(new PacketScenarioStep(player));
		
		// -- Armour --
		if (bench.codec.getProtocol() instanceof ZE2V7Codec) {
			list.add(new PacketScenarioStep(new BESlot(player.entityID, (short) 1, ICDItemStack.simple(ZE1V13IDs.I_IRON_A0, 1))));
			list.add(new PacketScenarioStep(new BESlot(player.entityID, (short) 2, ICDItemStack.simple(ZE1V13IDs.I_IRON_A1, 1))));
			list.add(new PacketScenarioStep(new BESlot(player.entityID, (short) 3, ICDItemStack.simple(ZE1V13IDs.I_IRON_A2, 1))));
			list.add(new PacketScenarioStep(new BESlot(player.entityID, (short) 4, ICDItemStack.simple(ZE1V13IDs.I_IRON_A3, 1))));
		}
		// -- Move --
		
		for (int j = 0; j < 4; j++) {
			BERelativeMove rel = new BERelativeMove();
			rel.entityID = player.entityID;
			if (j == 0) {
				rel.positionDelta.x = 16;
			} else if (j == 1) {
				rel.positionDelta.z = 16;
			} else if (j == 2) {
				rel.positionDelta.x = -16;
			} else if (j == 3) {
				rel.positionDelta.z = -16;
			}
			for (int i = 0; i < 31; i++) {
				list.add(new SleepScenarioStep(50));
				list.add(new TeleportScenarioStep(8, 66, 8, 0, 135 + (((i + (j * 32)) / 128.0f) * 360)));
				list.add(new PacketScenarioStep(rel));
			}
		}

		list.add(new TeleportScenarioStep(2, 66, 2, 0, 135));
		
		for (int i = 0; i < 5; i++) {
			list.add(new SleepScenarioStep(1000));
			BELook lookUp = new BELook();
			lookUp.entityID = player.entityID;
			if (i == 0) {
				// UP
				lookUp.rotation.set((byte) 0, (byte) 64);
			} else if (i == 1) {
				// DOWN
				lookUp.rotation.set((byte) 0, (byte) -64);
			} else if (i == 2) {
				// LEFT (from your perspective)
				lookUp.rotation.set((byte) 64, (byte) 0);
			} else if (i == 3) {
				// RIGHT
				lookUp.rotation.set((byte) -64, (byte) 0);
			} else if (i == 4) {
				// END
				lookUp.rotation.set((byte) 0, (byte) 0);
			}
			list.add(new PacketScenarioStep(lookUp));
		}

		list.add(new TeleportScenarioStep(2, 66, 4, 22, 150));
		
		// Ok, what about removing & placing a block?
		list.add(new SleepScenarioStep(1000));
		if (bench.codec.getProtocol() instanceof ZE1V13Codec)
			list.add(new PacketScenarioStep(new BEPlayerAction(player.entityID, (byte) 1)));
		list.add(new PacketScenarioStep(new BSetBlock(0, (short) 64, 2, (byte) 0, (byte) 0)));
		list.add(new SleepScenarioStep(1000));
		// Generate the specific 'change held item' packet for a given version
		if (bench.codec.getProtocol() instanceof ZE2V7Codec) {
			list.add(new PacketScenarioStep(new BESlot(player.entityID, (short) 0, ICDItemStack.simple(ZE1V13IDs.B_GRASS, 1))));
		} else if (bench.codec.getProtocol() instanceof ZE1V13Codec) {
			list.add(new PacketScenarioStep(new BEHeldItem(player.entityID, ICDItemStack.simple(ZE1V13IDs.B_GRASS, 1))));
		}
		list.add(new SleepScenarioStep(1000));
		if (bench.codec.getProtocol() instanceof ZE1V13Codec)
			list.add(new PacketScenarioStep(new BEPlayerAction(player.entityID, (byte) 1)));
		list.add(new PacketScenarioStep(new BSetBlock(0, (short) 64, 2, ZE0V7IDs.B_GRASS, (byte) 0)));
		list.add(new SleepScenarioStep(1000));
		// -- Bed --
		if (bench.codec.getProtocol() instanceof ZE2V9Codec) {
			list.add(new PacketScenarioStep(new BSetBlock(0, (short) 65, 0, ZE2V9IDs.B_BED, (byte) 0)));
			list.add(new PacketScenarioStep(new BSetBlock(0, (short) 65, 1, ZE2V9IDs.B_BED, (byte) 8)));
			list.add(new SleepScenarioStep(1000));
			BEUsedBed bed = new BEUsedBed();
			bed.entityID = player.entityID;
			bed.x = 0;
			bed.y = (byte) 65;
			bed.z = 0;
			list.add(new PacketScenarioStep(bed));
			list.add(new SleepScenarioStep(1000));
			list.add(new PacketScenarioStep(new BSetBlock(0, (short) 65, 0, ZE2V9IDs.B_AIR, (byte) 0)));
			list.add(new PacketScenarioStep(new BSetBlock(0, (short) 65, 1, ZE2V9IDs.B_AIR, (byte) 0)));
		}
		// --
		list.add(new PacketScenarioStep(new BEDelete(player.entityID)));
		
		return list;
	}
}
