package mccrossnet.workbench.scenario;

import java.util.LinkedList;

import mccrossnet.base.core.Packet;
import mccrossnet.protocol.all.EntityMetadata;
import mccrossnet.protocol.era0.v7.BEDelete;
import mccrossnet.protocol.era0.v7.BETeleport;
import mccrossnet.protocol.era0.v7.BSetBlock;
import mccrossnet.protocol.era1.v13.BMultipleBlockChange;
import mccrossnet.protocol.era1.v13.ZE1V13IDs;
import mccrossnet.protocol.era2.alpha.v1.BESpawnMob;
import mccrossnet.protocol.era2.alpha.v1.ZE2V1IDs;
import mccrossnet.protocol.era2.alpha.v2.ZE2V2Codec;
import mccrossnet.protocol.era2.alpha.v4.BEVelocity;
import mccrossnet.protocol.era2.alpha.v4.ZE2V4Codec;
import mccrossnet.protocol.era2.alpha.v5.BEMobAction;
import mccrossnet.protocol.era2.alpha.v5.BPlayerHealth;
import mccrossnet.protocol.era2.alpha.v5.ZE2V5Codec;
import mccrossnet.protocol.era2.alpha.v5.ZE2V5IDs;
import mccrossnet.protocol.era2.alpha.v6.BExplosion;
import mccrossnet.protocol.era2.alpha.v6.ZE2V6Codec;
import mccrossnet.protocol.era2.beta.v7.BUpdateSign;
import mccrossnet.protocol.era2.beta.v7.ZE2V7Codec;
import mccrossnet.protocol.era2.beta.v8.BEMetadata;
import mccrossnet.protocol.era2.beta.v8.ZE2V8Codec;
import mccrossnet.protocol.era2.beta.w14.ZE2V14IDs;
import mccrossnet.protocol.era2.beta.w17.BESpawnShinies;
import mccrossnet.protocol.era2.beta.w17.BPlayerExperience;
import mccrossnet.protocol.era2.beta.w17.ZE2V17Codec;
import mccrossnet.workbench.EID;
import mccrossnet.workbench.GenericWorkbench;
import mccrossnet.workbench.scenario.parts.PacketScenarioStep;
import mccrossnet.workbench.scenario.parts.RobotAction;
import mccrossnet.workbench.scenario.parts.RobotScenarioStep;
import mccrossnet.workbench.scenario.parts.ScenarioStep;
import mccrossnet.workbench.scenario.parts.SleepScenarioStep;
import mccrossnet.workbench.scenario.parts.TeleportScenarioStep;

/**
 * Even more creeper fun.
 */
public class CreeperScenario {
	public static LinkedList<ScenarioStep> generate(GenericWorkbench bench) {
		LinkedList<ScenarioStep> list = new LinkedList<ScenarioStep>();
		BESpawnMob mob = new BESpawnMob();
		mob.entityID = EID.SC_CREEPER_CREEPER;
		mob.type = ZE2V1IDs.M_CREEPER;
		mob.position.setFixed(1, 66, 1);
		list.add(new PacketScenarioStep(mob));
		if (bench.codec.getProtocol() instanceof ZE2V4Codec) {
			BEVelocity vel = new BEVelocity();
			vel.entityID = mob.entityID;
			vel.velocity.setFixed(0, 16, 0);
			list.add(new PacketScenarioStep(vel));
		}
		list.add(new TeleportScenarioStep(2, 66, 2, 0, 135));
		list.add(new SleepScenarioStep(1000));
		if (bench.codec.getProtocol() instanceof ZE2V5Codec) {
			BEMobAction moba = new BEMobAction();
			moba.entityID = mob.entityID;
			boolean canExplode = bench.codec.getProtocol() instanceof ZE2V6Codec;
			moba.command = (byte) (canExplode ? 4 : 3);
			list.add(new PacketScenarioStep(moba));
			if (canExplode) {
				// Is metadata required?
				if (bench.codec.getProtocol() instanceof ZE2V8Codec) {
					for (int i = 0; i < 8; i++) {
						list.add(new SleepScenarioStep(50));
						BEMetadata m = new BEMetadata();
						m.entityID = mob.entityID;
						EntityMetadata md = new EntityMetadata();
						md.descriptor = (byte) 0x10;
						md.intValue = (i * 16) + 15;
						m.metadata = new EntityMetadata[] {md};
						list.add(new PacketScenarioStep(m));
					}
				} else {
					list.add(new SleepScenarioStep(500));
				}
				BExplosion be = new BExplosion();
				be.power = 128;
				be.vectors = new byte[3];
				be.x = 1;
				be.y = 66;
				be.z = 1;
				list.add(new PacketScenarioStep(be));
			}
		} else {
			list.add(new SleepScenarioStep(1000));
		}
		list.add(new PacketScenarioStep(new BEDelete(mob.entityID)));
		return list;
	}
}
