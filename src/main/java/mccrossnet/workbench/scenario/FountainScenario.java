package mccrossnet.workbench.scenario;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;

import mccrossnet.base.core.Packet;
import mccrossnet.protocol.all.Face;
import mccrossnet.protocol.era0.v7.BEDelete;
import mccrossnet.protocol.era0.v7.BETeleport;
import mccrossnet.protocol.era0.v7.BSetBlock;
import mccrossnet.protocol.era1.v13.BMultipleBlockChange;
import mccrossnet.protocol.era1.v13.ZE1V13IDs;
import mccrossnet.protocol.era2.alpha.v1.BTimeUpdate;
import mccrossnet.protocol.era2.alpha.v2.BTileEntity;
import mccrossnet.protocol.era2.alpha.v2.ZE2V2Codec;
import mccrossnet.protocol.era2.beta.v7.BUpdateSign;
import mccrossnet.protocol.era2.beta.v7.ZE2V7Codec;
import mccrossnet.protocol.era2.beta.v8.BESpawnPainting;
import mccrossnet.protocol.era2.beta.v8.ZE2V8Codec;
import mccrossnet.protocol.era2.beta.w10.BWorldStatusUpdate;
import mccrossnet.protocol.era2.beta.w10.ZE2V10Codec;
import mccrossnet.protocol.era2.beta.w13.BEffect;
import mccrossnet.protocol.era2.beta.w13.ZE2V13Codec;
import mccrossnet.protocol.era2.beta.w13.ZE2V13IDs;
import mccrossnet.protocol.era2.beta.w17.BESpawnShinies;
import mccrossnet.protocol.era2.beta.w17.BPlayerExperience;
import mccrossnet.protocol.era2.beta.w17.ZE2V17Codec;
import mccrossnet.workbench.EID;
import mccrossnet.workbench.GenericWorkbench;
import mccrossnet.workbench.scenario.parts.PacketScenarioStep;
import mccrossnet.workbench.scenario.parts.ScenarioStep;
import mccrossnet.workbench.scenario.parts.SleepScenarioStep;
import mccrossnet.workbench.scenario.parts.TeleportScenarioStep;

/**
 * Fountain of XP!
 */
public class FountainScenario {
	public static LinkedList<ScenarioStep> generate(GenericWorkbench bench) {
		LinkedList<ScenarioStep> list = new LinkedList<ScenarioStep>();
		if (bench.codec.getProtocol() instanceof ZE2V13Codec) {
			// jukebox stuff
			BEffect bps = new BEffect();
			bps.category = 1005;
			bps.subcategory = ZE2V13IDs.I_MUSIC_CAT;
			bps.x = 7;
			bps.y = 68;
			bps.z = 7;
			list.add(new PacketScenarioStep(bps));
		}
		LinkedList<BSetBlock> blocks = new LinkedList<BSetBlock>();
		for (int i = 1; i <= 14; i++) {
			for (int j = 1; j <= 14; j++) {
				blocks.add(new BSetBlock(i, (short) 65, j, ZE1V13IDs.B_COBBLE, (byte) 0));
				if ((i == 1) || (j == 1) || (i == 14) || (j == 14)) {
					// nothing else on this layer
				} else if ((i == 2) || (i == 13) || (j == 2) || (j == 13)) {
					blocks.add(new BSetBlock(i, (short) 66, j, ZE1V13IDs.B_COBBLE, (byte) 0));
				} else if (((i == 7) || (i == 8)) && ((j == 7) || (j == 8))) {
					for (int k = 65; k <= 67; k++)
						blocks.add(new BSetBlock(i, (short) k, j, ZE1V13IDs.B_COBBLE, (byte) 0));
				} else {
					blocks.add(new BSetBlock(i, (short) 66, j, ZE1V13IDs.B_WATER, (byte) 0));
				}
			}
		}
		// Sign stuff
		if (bench.codec.getProtocol() instanceof ZE2V2Codec)
			blocks.add(new BSetBlock(7, (short) 68, 7, ZE1V13IDs.B_SIGN_STANDING, (byte) 6));
		// actual emit
		for (Packet p : BMultipleBlockChange.combine(blocks))
			list.add(new PacketScenarioStep(p));
		// More sign stuff
		if (bench.codec.getProtocol() instanceof ZE2V7Codec) {
			BUpdateSign sign = new BUpdateSign();
			sign.text0 = "Hug?";
			sign.text1 = "Hug!";
			sign.text2 = "I am hug sign.";
			sign.text3 = "FEAR ME.";
			sign.x = 7;
			sign.y = 68;
			sign.z = 7;
			list.add(new PacketScenarioStep(sign));
		} else if (bench.codec.getProtocol() instanceof ZE2V2Codec) {
			HashMap<String, Object> te = new HashMap<String, Object>();
			te.put("id", "Sign");
			te.put("x", 7);
			te.put("y", 68);
			te.put("z", 7);
			te.put("Text1", "Wow!");
			te.put("Text2", "Old version!");
			te.put("Text3", "Different words.");
			te.put("Text4", "STILL SCARY.");
			list.add(new PacketScenarioStep(BTileEntity.create(7, 68, 7, te)));
		}
		if (bench.codec.getProtocol() instanceof ZE2V8Codec) {
			// Painting
			BESpawnPainting p = new BESpawnPainting();
			p.entityID = EID.SC_FOUNTAIN_PAINTING;
			p.name = "Sunset";
			p.type = Face.XM;
			p.x = 7;
			p.y = 67;
			p.z = 7;
			list.add(new PacketScenarioStep(p));
		}
		// Now to let the player see it
		list.add(new TeleportScenarioStep(2, 68, 2, 0, -45));
		if (bench.codec.getProtocol() instanceof ZE2V17Codec) {
			for (int i = 0; i < EID.SC_FOUNTAIN_XP_COUNT; i++) {
				// Well, this works but they don't seem to get positioned correctly.
				BESpawnShinies ss = new BESpawnShinies();
				ss.position.setFixed(8, 69, 8);
				ss.entityID = EID.SC_FOUNTAIN_XP_MIN + i;
				ss.count = 1;
				list.add(new PacketScenarioStep(ss));
				BETeleport tp = new BETeleport();
				tp.position.set(ss.position);
				tp.entityID = ss.entityID;
				list.add(new PacketScenarioStep(tp));
				list.add(new SleepScenarioStep(50));
			}
			int level = 0;
			short totalXP = 0;
			short levelXP = 0;
			for (int i = 0; i < EID.SC_FOUNTAIN_XP_COUNT; i++) {
				list.add(new PacketScenarioStep(new BEDelete(EID.SC_FOUNTAIN_XP_MIN + i)));
				list.add(new SleepScenarioStep(50));
				
				totalXP++;
				int tmpTotal = BPlayerExperience.getTotalXPForLevel(level + 1);
				if (totalXP >= tmpTotal) {
					level++;
					levelXP = (short) tmpTotal;
				}
				
				BPlayerExperience be = new BPlayerExperience();
				be.currentXP = (byte) (totalXP - levelXP);
				be.currentLevel = (byte) level;
				be.totalXP = totalXP;
				list.add(new PacketScenarioStep(be));
			}
		} else {
			list.add(new SleepScenarioStep(5000));
		}
		// get rid of the fountain
		// NOTE: This only works because I know there's >1 block
		//  in involved chunks!!!
		// Otherwise the original combine will use references to the original packets.
		for (BSetBlock p : blocks)
			p.type = (byte) 0;
		for (Packet p : BMultipleBlockChange.combine(blocks))
			list.add(new PacketScenarioStep(p));
		if (bench.codec.getProtocol() instanceof ZE2V8Codec)
			list.add(new PacketScenarioStep(new BEDelete(EID.SC_FOUNTAIN_PAINTING)));
		if (bench.codec.getProtocol() instanceof ZE2V13Codec) {
			// jukebox stuff
			BEffect bps = new BEffect();
			bps.category = 1005;
			bps.subcategory = 0;
			bps.x = 7;
			bps.y = 68;
			bps.z = 7;
			list.add(new PacketScenarioStep(bps));
		}
		return list;
	}
}
