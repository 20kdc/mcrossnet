package mccrossnet.workbench.scenario;

import java.util.LinkedList;

import mccrossnet.protocol.all.items.ICDItemStack;
import mccrossnet.protocol.era0.v7.BEDelete;
import mccrossnet.protocol.era1.v13.BEAnimateItemPickup;
import mccrossnet.protocol.era1.v13.BEDoAbsolutelyNothingWithEntity;
import mccrossnet.protocol.era1.v13.BESpawnItem;
import mccrossnet.protocol.era1.v13.BGiveItem;
import mccrossnet.protocol.era1.v13.BPlayerOnGround;
import mccrossnet.protocol.era2.alpha.v2.BPlayerInventory;
import mccrossnet.protocol.era2.beta.v7.BSetWindowSlot;
import mccrossnet.protocol.era2.beta.v7.BSetWindowSlots;
import mccrossnet.protocol.era2.beta.v7.ZE2V7Codec;
import mccrossnet.protocol.era2.beta.v8.BEPlayerControlButton;
import mccrossnet.protocol.era2.beta.w11.BAddToStatistic;
import mccrossnet.workbench.EID;
import mccrossnet.workbench.GenericWorkbench;
import mccrossnet.workbench.scenario.parts.PacketScenarioStep;
import mccrossnet.workbench.scenario.parts.RobotAction;
import mccrossnet.workbench.scenario.parts.RobotScenarioStep;
import mccrossnet.workbench.scenario.parts.ScenarioStep;
import mccrossnet.workbench.scenario.parts.SleepScenarioStep;
import mccrossnet.workbench.scenario.parts.TeleportScenarioStep;

/**
 * Places an item, the player "picks it up", slot stuff is tested.
 * The item is finally dropped and obliterated.
 */
public class ItemScenario {
	public static LinkedList<ScenarioStep> generate(GenericWorkbench bench) {
		LinkedList<ScenarioStep> list = new LinkedList<ScenarioStep>();
		
		list.add(new TeleportScenarioStep(2, 66, 2, 45, 135));
		list.add(new RobotScenarioStep(RobotAction.Press1));
		
		BESpawnItem item = new BESpawnItem();
		item.entityID = EID.SC_ITEM_ITEM;
		item.item = ICDItemStack.simple(1, 2);
		item.position.setFixed(0, 66, 0);
		list.add(new PacketScenarioStep(item));
		
		list.add(new PacketScenarioStep(new BEDoAbsolutelyNothingWithEntity(item.entityID)));
		list.add(new PacketScenarioStep(new BPlayerOnGround(false)));
		
		list.add(new SleepScenarioStep(2000));
		list.add(new PacketScenarioStep(new BEAnimateItemPickup(item.entityID, EID.PLAYER)));
		list.add(new SleepScenarioStep(100));
		list.add(new PacketScenarioStep(new BEDelete(item.entityID)));
		
		// This is the version-dependent bit.
		if (bench.codec.getProtocol() instanceof ZE2V7Codec) {
			ICDItemStack[] items = new ICDItemStack[45];
			items[36] = item.item;
			list.add(new PacketScenarioStep(new BSetWindowSlots((byte) 0, items)));
		} else {
			BPlayerInventory bpi = new BPlayerInventory();
			bpi.inventoryType = -1;
			bpi.content = new ICDItemStack[36];
			list.add(new PacketScenarioStep(bpi));
			list.add(new PacketScenarioStep(new BGiveItem(item.item)));
		}
		
		list.add(new RobotScenarioStep(RobotAction.TransferSlot1ToSlot2));
		list.add(new RobotScenarioStep(RobotAction.Press2));
		// just to test a little theory I have
		// +Z
		list.add(new TeleportScenarioStep(2, 65, 2, 0, 0));
		list.add(new RobotScenarioStep(RobotAction.Drop));
		list.add(new SleepScenarioStep(100));
		// -X
		list.add(new TeleportScenarioStep(2, 65, 2, 0, 90));
		list.add(new RobotScenarioStep(RobotAction.Drop));
		list.add(new SleepScenarioStep(100));
		list.add(new RobotScenarioStep(RobotAction.Press1));
		
		// Dropping is also kinda version-dependent, we need to emulate the drop "happening"
		if (bench.codec.getProtocol() instanceof ZE2V7Codec)
			list.add(new PacketScenarioStep(new BSetWindowSlot((byte) 0, (short) 37, null)));
		
		return list;
	}
}
