package mccrossnet.workbench.scenario;

import java.util.LinkedList;

import mccrossnet.protocol.era0.v7.BSetBlock;
import mccrossnet.protocol.era2.alpha.v1.BTimeUpdate;
import mccrossnet.protocol.era2.beta.v9.ZE2V9Codec;
import mccrossnet.protocol.era2.beta.v9.ZE2V9IDs;
import mccrossnet.protocol.era2.beta.w10.BWorldStatusUpdate;
import mccrossnet.protocol.era2.beta.w10.ZE2V10Codec;
import mccrossnet.workbench.GenericWorkbench;
import mccrossnet.workbench.scenario.parts.PacketScenarioStep;
import mccrossnet.workbench.scenario.parts.RobotAction;
import mccrossnet.workbench.scenario.parts.RobotScenarioStep;
import mccrossnet.workbench.scenario.parts.ScenarioStep;
import mccrossnet.workbench.scenario.parts.SleepScenarioStep;
import mccrossnet.workbench.scenario.parts.TeleportScenarioStep;

/**
 * Runs a day-night cycle.
 */
public class TimeScenario {
	public static LinkedList<ScenarioStep> generate(GenericWorkbench bench) {
		LinkedList<ScenarioStep> list = new LinkedList<ScenarioStep>();
		list.add(new TeleportScenarioStep(1.5f, 67, 1.5f, 45, 134));
		for (int i = 0; i < 75; i++) {
			if (bench.codec.getProtocol() instanceof ZE2V10Codec) {
				if (i == 20) {
					list.add(new PacketScenarioStep(new BWorldStatusUpdate((byte) 1)));
				} else if (i == 40) {
					list.add(new PacketScenarioStep(new BWorldStatusUpdate((byte) 2)));
				}
			}
			list.add(new PacketScenarioStep(new BTimeUpdate(i * 320)));
			list.add(new SleepScenarioStep(40));
		}
		return list;
	}
}
