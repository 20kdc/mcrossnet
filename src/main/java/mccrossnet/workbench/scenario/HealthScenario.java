package mccrossnet.workbench.scenario;

import java.util.LinkedList;

import mccrossnet.base.core.Packet;
import mccrossnet.base.core.Protocol;
import mccrossnet.protocol.era0.v7.BEDelete;
import mccrossnet.protocol.era0.v7.BETeleport;
import mccrossnet.protocol.era0.v7.BSetBlock;
import mccrossnet.protocol.era1.v13.BMultipleBlockChange;
import mccrossnet.protocol.era1.v13.ZE1V13IDs;
import mccrossnet.protocol.era2.alpha.v2.ZE2V2Codec;
import mccrossnet.protocol.era2.alpha.v5.BPlayerHealth;
import mccrossnet.protocol.era2.beta.v7.BUpdateSign;
import mccrossnet.protocol.era2.beta.v7.ZE2V7Codec;
import mccrossnet.protocol.era2.beta.v8.BBlockDoesAThing;
import mccrossnet.protocol.era2.beta.v8.ZE2V8Codec;
import mccrossnet.protocol.era2.beta.v8.ZE2V8IDs;
import mccrossnet.protocol.era2.beta.w11.BAddToStatistic;
import mccrossnet.protocol.era2.beta.w11.BEThunderbolt;
import mccrossnet.protocol.era2.beta.w11.ZE2V11Codec;
import mccrossnet.protocol.era2.beta.w14.ZE2V14Codec;
import mccrossnet.protocol.era2.beta.w17.BEAddEffect;
import mccrossnet.protocol.era2.beta.w17.BEDelEffect;
import mccrossnet.protocol.era2.beta.w17.BESpawnShinies;
import mccrossnet.protocol.era2.beta.w17.BPlayerExperience;
import mccrossnet.protocol.era2.beta.w17.BWhoListEntry;
import mccrossnet.protocol.era2.beta.w17.ZE2V17Codec;
import mccrossnet.workbench.EID;
import mccrossnet.workbench.GenericWorkbench;
import mccrossnet.workbench.scenario.parts.ChatScenarioStep;
import mccrossnet.workbench.scenario.parts.PacketScenarioStep;
import mccrossnet.workbench.scenario.parts.RobotAction;
import mccrossnet.workbench.scenario.parts.RobotScenarioStep;
import mccrossnet.workbench.scenario.parts.ScenarioStep;
import mccrossnet.workbench.scenario.parts.SleepScenarioStep;
import mccrossnet.workbench.scenario.parts.TeleportScenarioStep;

/**
 * A scenario to test health & respawning.
 * Also, creepypasta.
 * Also, note blocks.
 */
public class HealthScenario { 
	private static int[] ominous = new int[] {
			 0, -1, -1, -1,
			 0, -1, -1,  0,
			-1,  3,
	};
	public static LinkedList<ScenarioStep> generate(GenericWorkbench bench) {
		LinkedList<ScenarioStep> list = new LinkedList<ScenarioStep>();
		list.add(new ChatScenarioStep("<Herobrine> HAHAHAHAHAHA"));
		list.add(new PacketScenarioStep(new BPlayerHealth((short) 0)));
		if (bench.codec.getProtocol() instanceof ZE2V11Codec) {
			// Can't really confirm this...
			list.add(new PacketScenarioStep(new BAddToStatistic(2022, (byte) 1)));
			// Also, make one of these just to cover the packet
			BEThunderbolt bet = new BEThunderbolt();
			bet.aThing = true;
			bet.entityID = EID.SC_HEALTH_THUNDERBOLT;
			bet.x = 7;
			bet.y = 66;
			bet.z = 7;
			list.add(new PacketScenarioStep(bet));
			list.add(new SleepScenarioStep(500));
			list.add(new PacketScenarioStep(new BEDelete(bet.entityID)));
		}
		list.add(new RobotScenarioStep(RobotAction.Respawn));
		if (bench.codec.getProtocol() instanceof ZE2V17Codec) {
			// I mean, how could you trust a random workbench server with flying boats to...
			// NOT have Herobrine lurking in it?
			BWhoListEntry who = new BWhoListEntry();
			who.username = "Herobrine";
			list.add(new PacketScenarioStep(who));
		}
		// -- Effect (Start) --
		if (bench.codec.getProtocol() instanceof ZE2V14Codec) {
			BEAddEffect effect = new BEAddEffect();
			effect.entityID = EID.PLAYER;
			effect.effectID = 17;
			effect.power = 0;
			effect.time = 1000;
			list.add(new PacketScenarioStep(effect));
		}
		// -- note block --
		if (bench.codec.getProtocol() instanceof ZE2V8Codec) {
			BSetBlock bsb = new BSetBlock();
			bsb.x = 7;
			bsb.y = 67;
			bsb.z = 7;
			bsb.type = ZE2V8IDs.B_NOTE_BLOCK;
			list.add(new PacketScenarioStep(bsb));
			for (int note : ominous) {
				list.add(new SleepScenarioStep(150));
				if ((note != -1) && (bench.codec.getProtocol() instanceof ZE2V8Codec)) {
					BBlockDoesAThing p = new BBlockDoesAThing();
					p.x = 7;
					p.y = 67;
					p.z = 7;
					p.iA = 0;
					p.iB = (byte) note;
					list.add(new PacketScenarioStep(p));
				}
			}
			bsb = new BSetBlock();
			bsb.x = 7;
			bsb.y = 67;
			bsb.z = 7;
			bsb.type = ZE2V8IDs.B_AIR;
			list.add(new PacketScenarioStep(bsb));
		} else {
			list.add(new SleepScenarioStep(500));
		}
		// -- Effect (End) --
		if (bench.codec.getProtocol() instanceof ZE2V14Codec) {
			BEDelEffect effect = new BEDelEffect();
			effect.entityID = EID.PLAYER;
			effect.effectID = 17;
			list.add(new PacketScenarioStep(effect));
		}
		return list;
	}
}
