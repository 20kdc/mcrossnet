package mccrossnet.workbench;

import java.io.IOException;
import java.net.ServerSocket;

import mccrossnet.base.TCPServer;
import mccrossnet.base.core.Connection;
import mccrossnet.universal.Handshaker;
import mccrossnet.universal.PEra;
import mccrossnet.universal.PVersion;

public class IdentifierMain {
	public static void main(String[] args) throws IOException {
		System.out.println("Known versions:");
		for (PEra e : PEra.values()) {
			System.out.println(e + " (" + e.name + "):");
			for (PVersion version : e.versions) {
				System.out.println(" " + version);
				System.out.println("  " + version.name);
			}
		}
		new TCPServer(new ServerSocket(25565), socket -> {
			try {
				Handshaker result = new Handshaker(socket.getInputStream(), socket.getOutputStream());
				PVersion version = result.getVersion();
				if (version != null) {
					result.sendDisconnect("You're using " + version + ": " + version.name);
				} else {
					// bit of a cheat TBH
					result.sendDisconnect("You're using " + result.era + "V" + result.getVersionID());
				}
				socket.close();
			} catch (Exception e) {
				try {
					socket.close();
				} catch (Exception e2) {
				}
				e.printStackTrace();
			}
		}).run();
	}
}
