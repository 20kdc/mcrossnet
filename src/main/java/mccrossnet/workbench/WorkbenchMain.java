package mccrossnet.workbench;

import java.io.IOException;
import java.net.ServerSocket;

import mccrossnet.base.TCPServer;
import mccrossnet.base.core.Connection;
import mccrossnet.universal.Handshaker;
import mccrossnet.universal.PVersion;

public class WorkbenchMain {
	public static void main(String[] args) throws IOException {
		new TCPServer(new ServerSocket(25565), socket -> {
			try {
				Handshaker result = new Handshaker(socket.getInputStream(), socket.getOutputStream());
				PVersion version = result.getVersion();
				Connection c = version.codec.makeConnection(socket.getInputStream(), socket.getOutputStream(), true);
				GenericWorkbench gw = AllWorkbenchVersions.createWorkbench(version, c, result);
				// prevent stupidity, thanks
				if (socket.getInetAddress().isLoopbackAddress())
					gw.localSecure = true;
				AllWorkbenchVersions.installScenario(gw);
				gw.run();
				socket.close();
			} catch (Exception e) {
				try {
					socket.close();
				} catch (Exception e2) {
				}
				e.printStackTrace();
			}
		}).run();
	}
}
