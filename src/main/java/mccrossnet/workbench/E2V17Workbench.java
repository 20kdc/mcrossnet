package mccrossnet.workbench;

import java.io.IOException;

import mccrossnet.base.core.Connection;
import mccrossnet.base.core.Packet;
import mccrossnet.protocol.all.PingKeepAlive;
import mccrossnet.protocol.era2.beta.w10.BWorldStatusUpdate;
import mccrossnet.protocol.era2.beta.w17.BWhoListEntry;
import mccrossnet.universal.Handshaker;

public class E2V17Workbench extends E2V7Workbench {

	public E2V17Workbench(Connection codec, Handshaker startState) {
		super(codec, startState);
		commands.put("creative", new Command() {
			@Override
			public void run(String... args) throws IOException {
				BWorldStatusUpdate wsu = new BWorldStatusUpdate();
				wsu.statusUpdate = 3;
				wsu.gameMode = 1;
				codec.write(wsu);
			}
			
			@Override
			public String getHelp() {
				return "Creative";
			}
		});
		commands.put("survival", new Command() {
			@Override
			public void run(String... args) throws IOException {
				BWorldStatusUpdate wsu = new BWorldStatusUpdate();
				wsu.statusUpdate = 3;
				wsu.gameMode = 0;
				codec.write(wsu);
			}
			
			@Override
			public String getHelp() {
				return "Survival";
			}
		});
	}
	
	@Override
	public void sendKeepAlive(Packet cause) throws IOException {
		// Don't send keepalives for keepalives. That will only end badly.
		if (!(cause instanceof PingKeepAlive))
			codec.write(new PingKeepAlive());
	}

}
