package mccrossnet.workbench;

import java.io.IOException;
import java.util.logging.Level;

import mccrossnet.base.core.Connection;
import mccrossnet.base.core.Packet;
import mccrossnet.protocol.all.PingKeepAlive;
import mccrossnet.protocol.era0.v7.AIdentificationStoc;
import mccrossnet.protocol.era0.v7.BELook;
import mccrossnet.protocol.era0.v7.BERelativeMove;
import mccrossnet.protocol.era0.v7.BESpawnPlayer;
import mccrossnet.protocol.era0.v7.BETeleport;
import mccrossnet.protocol.era0.v7.BLevelBegin;
import mccrossnet.protocol.era0.v7.BLevelData;
import mccrossnet.protocol.era0.v7.BLevelEnd;
import mccrossnet.protocol.era0.v7.BPlayerAction;
import mccrossnet.protocol.era0.v7.BPlayerMessage;
import mccrossnet.protocol.era1.v13.BEPlayerAction;
import mccrossnet.universal.Handshaker;
import mccrossnet.workbench.GenericWorkbench.Command;

/**
 * A workbench for Classic
 * TO USE:
 *  1. Use MultiMC
 *  2. Decode this: https://github.com/MultiMC/MultiMC5/blob/c1ea42d3d9b793b52d52310f69174f1c5ab9daef/libraries/launcher/org/multimc/LegacyFrame.java#L55
 *  the gist of it is,
 *   the directory outside the .minecraft directory must contain a file called "mpticket"
 *   that should look like this:
 *   
 *  ::1
 *  25565
 *  quackduck
 */
public class E0V7Workbench extends GenericWorkbench {

	public E0V7Workbench(Connection c, Handshaker s) {
		super(c, s);
	}
	
	@Override
	public void begin() throws IOException {
		sendConnectPacket();
		sendWorld();
		sendInitialPlayerPosition();
	}

	public void sendConnectPacket() throws IOException {
		AIdentificationStoc ais = new AIdentificationStoc();
		ais.powerLevel = 100;
		ais.protocolVersion = 7;
		codec.write(ais);
	}
	
	public void sendWorld() throws IOException {
		codec.write(new BLevelBegin());
		byte[] reorder = new byte[16 * 128 * 16];
		for (int i = 0; i < reorder.length; i++) {
			// Beta -> Classic conversion
			int totalColumns = 16 * 16;
			int worldHeight = 128;
			int oldI = ((i % totalColumns) * worldHeight) + (i / totalColumns);
			reorder[i] = WORLD_CONTENT[oldI];
		}
		for (BLevelData chunk : BLevelData.compressAndSplit(reorder))
			codec.write(chunk);
		codec.write(new BLevelEnd((short) 16, (short) 128, (short) 16));
	}
	
	public void sendInitialPlayerPosition() throws IOException {
		BESpawnPlayer player = new BESpawnPlayer();
		player.entityID = -1;
		player.position.setFixed(8, 128, 8);
		codec.write(player);
	}

	@Override
	public void sendKeepAlive(Packet cause) throws IOException {
		codec.write(new PingKeepAlive());
	}

	@Override
	public void distribute(Packet o) throws IOException {
		if (o instanceof BPlayerMessage)
			receiveChatMessage(((BPlayerMessage) o).text);
	}
	
	@Override
	public void sendChatMessage(String text) throws IOException {
		BPlayerMessage bpm = new BPlayerMessage();
		bpm.text = text;
		codec.write(bpm);
	}

	@Override
	protected void executeLimboMode(Packet o) throws IOException {
		// don't really need to do anything, Classic does it itself
	}
}
