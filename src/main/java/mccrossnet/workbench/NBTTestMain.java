package mccrossnet.workbench;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import mccrossnet.nbt.NamedBinaryTag;
import mccrossnet.nbt.SNBTReader;
import mccrossnet.nbt.SNBTWriter;
import mccrossnet.protocol.era0.v7.ZE0V7IDs;

/**
 * Tool to test that the NBT library can read and write NBT successfully.
 * Format information from: https://minecraft.gamepedia.com/Java_Edition_Indev_level_format
 */
public class NBTTestMain {
	public static void main(String[] args) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);
		NamedBinaryTag.write(dos, "MinecraftLevel", genTest());
		dos.flush();
		
		try {
			String where = System.getProperty("user.home");
			where += "/.minecraft/saves/NBTTestMain.mclevel";
			GZIPOutputStream gz = new GZIPOutputStream(new FileOutputStream(where));
			baos.writeTo(gz);
			gz.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
		DataInputStream dis = new DataInputStream(bais);
		//DataInputStream dis = new DataInputStream(new GZIPInputStream(new FileInputStream("/home/20kdc/.minecraft/saves/NBT.mclevel")));
		Object composite = NamedBinaryTag.read(dis, true);
		
		StringWriter sw = new StringWriter();
		new SNBTWriter(sw, "\t").write(composite);
		System.out.println(sw);
		
		System.out.println(new SNBTReader(new StringReader(sw.toString())).read());
	}
	
	private static Object genTest() {
		HashMap<String, Object> about = new HashMap<String, Object>();
		about.put("CreatedOn", 0L);
		about.put("Name", "A world");
		about.put("Author", "Chell Portaller");
		
		HashMap<String, Object> env = new HashMap<String, Object>();
		env.put("TimeOfDay", (short) 0);
		env.put("SkyBrightness", (byte) 15);
		env.put("SkyColor", 0xFFFFFF);
		env.put("FogColor", 0xFFFFFF);
		env.put("CloudColor", 0xFFFFFF);
		env.put("CloudHeight", (short) 66);
		
		env.put("SurroundingGroundType", (byte) 2);
		env.put("SurroundingGroundHeight", (short) 4);
		env.put("SurroundingWaterType", ZE0V7IDs.B_WATER);
		env.put("SurroundingWaterHeight", (short) 4);
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("Width", (short) 8);
		map.put("Height", (short) 8);
		map.put("Length", (short) 8);
		map.put("Spawn", new Object[] {
			(short) 4,
			(short) 8,
			(short) 4
		});
		byte[] blocksArea = new byte[8 * 8 * 8];
		for (int i = 0; i < 8 * 8 * 4; i++)
			blocksArea[i] = ZE0V7IDs.B_WATER;
		for (int i = 0; i < 8 * 8 * 1; i++)
			blocksArea[i] = ZE0V7IDs.B_STONE;
		map.put("Blocks", blocksArea);
		map.put("Data", new byte[8 * 8 * 8]);
		
		HashMap<String, Object> world = new HashMap<String, Object>();
		world.put("About", about);
		world.put("Environment", env);
		world.put("Map", map);
		world.put("Entities", new Object[] {});
		world.put("TileEntities", new Object[] {});
		return world;
	}
}
