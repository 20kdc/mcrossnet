package mccrossnet.workbench.introspection;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.LinkedList;

import mccrossnet.base.core.ID;
import mccrossnet.base.core.IDKind;
import mccrossnet.base.data.BaseFormat;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StandardStruct;
import mccrossnet.base.data.Struct;
import mccrossnet.base.data.StructFormat;
import mccrossnet.base.data.ValueFormat;
import mccrossnet.base.data.formats.NotPresentInThisVersionFormat;
import mccrossnet.base.data.formats.StructStructFormat;
import mccrossnet.base.netarch.hub.HubProtocol;
import mccrossnet.protocol.all.EmptyPacket;
import mccrossnet.protocol.all.PingKeepAlive;
import mccrossnet.universal.PVersion;

/**
 * This tool generates "XYZW.protocol.html" files in a Javadoc-style tree for the project.
 * The corresponding files have prepared links.
 * I'm quite aware this is a bit ugly.
 */
public class IntrospectionMain {
	public static void main(String[] args) throws Exception {
		if (args.length == 0)
			args = new String[] {"build/javadoc"};
		String targetBase = args[0];
		for (PVersion pv : PVersion.values()) {
			if (pv.codec != null) {
				Class cls = pv.codec.getClass();
				String pkg = cls.getName().replace('.', '/').replace(cls.getSimpleName(), "");
				String backDots = "";
				for (char c : pkg.toCharArray())
					if (c == '/')
						backDots += "../";
				String totalBase = targetBase + "/" + pkg;
				System.out.println(totalBase);
				String human = pv.era.name + " : " + pv.name + " (" + pv.toString() + ")";
				{
					HTMLGenHelper hgh = new HTMLGenHelper(backDots, human);
					hgh.sb.append("<li><a href=\"" + hgh.linkClass(pv.codec.getClass()) + "\">Class</a></li>\n");
					hgh.sb.append("<li class=\"navBarCell1Rev\">Protocol</li>\n");
					hgh.sb.append("<li><a href=\"ids.html\">IDs</a></li>\n");
					hgh.doneWithHeader();
					hgh.sb.append("<p>The primary source of information for this documentation is various old revisions of https://wiki.vg/index.php?title=Protocol.</p>\n");
					hgh.sb.append("<p>However, their approach to game version separation may make acquiring the specific information unreliable.</p>\n");
					hgh.sb.append("<p>I attempted to embed MediaWiki revision links for proper crediting reasons. Then they broke. I will not be repeating this mistake.</p>\n");
					hgh.sb.append("<p>You're looking at my solution to this.</p>\n");
					buildProtocolDetails(pv.codec, hgh);
					hgh.endFile();
					Files.write(Paths.get(totalBase + "protocol.html"), hgh.sb.toString().getBytes(StandardCharsets.UTF_8));
				}
				{
					HTMLGenHelper hgh = new HTMLGenHelper(backDots, human);
					hgh.sb.append("<li><a href=\"" + hgh.linkClass(pv.codec.getClass()) + "\">Class</a></li>\n");
					hgh.sb.append("<li><a href=\"protocol.html\">Protocol</a></li>\n");
					hgh.sb.append("<li class=\"navBarCell1Rev\">IDs</li>\n");
					hgh.doneWithHeader();
					LinkedList<IDKind> kinds = new LinkedList<IDKind>();
					LinkedList<ID> ids = new LinkedList<ID>();
					for (ID id : pv.codec.registry.getAllIDs()) {
						if (!kinds.contains(id.type))
							kinds.add(id.type);
						ids.add(id);
					}
					ids.sort(new Comparator<ID>() {
						@Override
						public int compare(ID o1, ID o2) {
							if (o1.id > o2.id)
								return 1;
							if (o1.id < o2.id)
								return -1;
							return 0;
						}
					});
					for (final IDKind kind : kinds) {
						hgh.summaryBlock(new Runnable() {
							@Override
							public void run() {
								hgh.anchor(kind.name);
								hgh.tableBlock(kind.name, new Runnable() {
									@Override
									public void run() {
										for (ID i : ids)
											if (i.type == kind)
												hgh.tableRow("" + i.id, i.name);
									}
								}, "ID", "Name");
							}
						});
					}
					hgh.endFile();
					Files.write(Paths.get(totalBase + "ids.html"), hgh.sb.toString().getBytes(StandardCharsets.UTF_8));
				}
			}
		}
	}
	
	private static void buildProtocolDetails(HubProtocol hub, HTMLGenHelper hgh) {
		StringBuilder sb = hgh.sb;
		// PACKET SUMMARY {
		LinkedList<Class> packets = new LinkedList<Class>();
		packetSummary("CtoS", hub.packetTypesCtos, hgh);
		for (Class c : hub.packetTypesCtos)
			if (c != null)
				if (!packets.contains(c))
					packets.add(c);
		hgh.pageBreak();
		packetSummary("StoC", hub.packetTypesStoc, hgh);
		for (Class c : hub.packetTypesStoc)
			if (c != null)
				if (!packets.contains(c))
					packets.add(c);
		sb.append("<p>Note: some packets may be listed as both CtoS and StoC when they actually aren't.<br/>\n");
		sb.append("This is a safety measure because it's easier to assume they are and fix it later.</p>\n");
		// }
		for (int i = 0; i < packets.size(); i++) {
			Class packetClass = packets.get(i);
			ExtractedDoc packetDoc = new ExtractedDoc(packetClass);
			hgh.pageBreak();
			hgh.summaryBlock(new Runnable() {
				
				@Override
				public void run() {
					hgh.anchor(packetClass.getSimpleName());
					sb.append("<h3>" + packetClass.getSimpleName());
					for (Class c2 : hub.packetTypesCtos) {
						if (packetClass == c2) {
							sb.append(" <a href=\"#" + packetClass.getSimpleName() + "_CTOS\">(CtoS)</a>");
							break;
						}
					}
					for (Class c2 : hub.packetTypesStoc) {
						if (packetClass == c2) {
							sb.append(" <a href=\"#" + packetClass.getSimpleName() + "_STOC\">(StoC)</a>");
							break;
						}
					}
					sb.append("</h3>");
					sb.append("<h3>");
					sb.append(" <span class=\"memberNameLink\"><a href=\"" + hgh.linkClass(packetClass) + "\">Class (" + packetClass.getName() + ")</a>");
					sb.append("</h3>");
					sb.append("<pre>");
					sb.append(packetDoc.getDescription());
					sb.append("</pre>");
					if (!BaseFormat.class.isAssignableFrom(packetClass))
						actualInteriorOfStructHandler(packetClass, packetDoc, hub, hgh, packets);
				}
			});
		}
		sb.append("</div>");
		// }
	}
	
	public static void packetSummary(String side, Class[] packets, HTMLGenHelper hgh) {
		hgh.summaryBlock(new Runnable() {
			@Override
			public void run() {
				StringBuilder target = hgh.sb;
				target.append("<h3>Packet Summary (" + side + ")</h3>\n");
				hgh.tableBlock("Packets", new Runnable() {
					@Override
					public void run() {
						for (int i = 0; i < packets.length; i++) {
							if (packets[i] != null) {
								String hexString = Integer.toHexString(i);
								if (hexString.length() == 1)
									hexString = "0" + hexString;
								String intB = "<a name=\"" + packets[i].getSimpleName() + "_" + side.toUpperCase() + "\"/>\n";
								intB += "<a href=\"#" + packets[i].getSimpleName() + "\">" + packets[i].getSimpleName() + "</a>\n";
								hgh.tableRow("0x" + hexString, intB);
							}
						}
					}
				}, "ID", "Class");
			}
		});
	}
	
	public static void actualInteriorOfStructHandler(Class structClass, ExtractedDoc structDoc, FormatContext hub, HTMLGenHelper hgh, LinkedList<Class> futureStuff) {
		StringBuilder sb = hgh.sb;
		final Object o;
		try {
			o = structClass.newInstance();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		if (o instanceof StandardStruct) {
			StandardStruct ss = (StandardStruct) o;
			hgh.tableBlock("Fields", new Runnable() {
				@Override
				public void run() {
					for (Field f : ss.getFieldOrder(hub.getClass())) {
						BaseFormat vf = ss.getFieldFormat(hub, f);
						if (vf == null)
							throw new RuntimeException("TOTAL FAILURE OF FORMAT AT " + f);
						Class fFmtClass = vf.getClass();
						String clarification = "";
						// Special cases/clarification
						if (fFmtClass == NotPresentInThisVersionFormat.class) {
							continue;
						} else if (fFmtClass == StructStructFormat.class) {
							fFmtClass = f.getType();
							clarification = " (structure)";
						} else if (vf instanceof StructFormat) {
							Object fValue = null;
							try {
								fValue = f.get(o);
							} catch (Exception e) {
								throw new RuntimeException(e);
							}
							clarification = ((StructFormat) vf).clarify(fValue);
						}
						if (!futureStuff.contains(fFmtClass))
							futureStuff.add(fFmtClass);
						String nameText = "<a href=\"" + hgh.linkClass(structClass) + "#" + f.getName() + "\">" + f.getName() + "</a>";
						String valueText = "<a href=\"#" + fFmtClass.getSimpleName() + "\">" + fFmtClass.getSimpleName() + "</a>" + clarification;
						hgh.tableRow(nameText, valueText, structDoc.getFieldDescription(f));
					}
				}
			}, "Name", "Format", "Description");
		} else if (EmptyPacket.class.isAssignableFrom(structClass)) {
			sb.append("It doesn't have any contents.<br/>\n");
		} else {
			sb.append("This isn't a handlable kind of struct.<br/>For structural details, please see the class's code.<br/>\n");
		}
		sb.append("<br/>");
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			((Struct) o).writeContent(hub, new DataOutputStream(baos));
			sb.append("Default state (this is not API unless documented):<br/><br/>\n");
			sb.append(o.toString() + "<br/><br/>\n");
			sb.append("Size (without ID byte): ");
			sb.append(baos.size());
			sb.append("<br/>\n");
		} catch (Exception e) {
			sb.append("<i>The default state of the struct is invalid.</i>\n");
		}
	}
}
