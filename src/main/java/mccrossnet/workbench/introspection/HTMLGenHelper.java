package mccrossnet.workbench.introspection;

/**
 * This is responsible for the introspection HTML generation state.
 */
class HTMLGenHelper {
	public StringBuilder sb = new StringBuilder();
	public String backDots, name;
	boolean flipper = false;
	
	public HTMLGenHelper(String backDots, String name) {
		sb.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n");
		sb.append("<html lang=\"en\">\n<head>\n");
		sb.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"" + backDots + "stylesheet.css\">\n");
		sb.append("</head><body>\n");
		this.backDots = backDots;
		this.name = name;
		sb.append("<div class=\"topNav\"><ul class=\"navList\">\n");
	}
	
	public void doneWithHeader() {
		// NAV {
		sb.append("</ul>\n</div>\n");
		sb.append("<div class=\"header\">\n");
		sb.append("<h2>" + name + "</h2>\n");
		sb.append("</div>\n");
		// }
		// CONTENT {
		sb.append("<div class=\"contentContainer\">");
	}
	
	/**
	 * Ends the file.
	 */
	public void endFile() {
		sb.append("</body></html>\n");
	}
	
	/**
	 * Places an anchor.
	 * @param s The anchor's hashthingy.
	 */
	public void anchor(String s) {
		sb.append("<a name=\"" + s + "\"/>");
	}
	
	/**
	 * Returns the relative path for the Javadoc page of a class.
	 * @param s1 The class
	 * @return The relative path
	 */
	public String linkClass(Class s1) {
		return backDots + s1.getName().replace('.', '/') + ".html";
	}
	
	/**
	 * Wraps something in a summary block.
	 * @param r The code to run during the summary block
	 */
	public void summaryBlock(Runnable r) {
		sb.append("<div class=\"summary\">");
		sb.append("<ul class=\"blockList\"><li class=\"blockList\">");
		sb.append("<ul class=\"blockList\"><li class=\"blockList\">");
		r.run();
		sb.append("</ul></li>\n");
		sb.append("</ul></li)>\n");
		sb.append("</div>\n");
	}
	
	/**
	 * Writes a table.
	 * @param title The caption of the table.
	 * @param content The code to run for table contents
	 * @param columns The column names.
	 */
	public void tableBlock(String title, Runnable content, String... columns) {
		sb.append("<table class=\"memberSummary\" cellspacing=\"0\" cellpadding=\"3\" border=\"0\">\n");
		sb.append("<caption><span>" + title + "</span><span class=\"tabEnd\"/></caption>\n");
		sb.append("<tbody>\n");
		sb.append("<tr>\n");
		for (int i = 0; i < columns.length; i++) {
			// The logic here doesn't really make sense,
			//  but it also makes a lot of sense.
			String classes = "colFirst";
			if (i == columns.length - 1)
				classes = "colLast";
			sb.append("<th class=\"" + classes + "\" scope=\"col\">" + columns[i] + "</th>\n");
		}
		sb.append("</tr>\n");
		flipper = false;
		content.run();
		sb.append("</tbody></table>\n");
	}
	
	/**
	 * Writes a table row.
	 * @param columns The column data.
	 */
	public void tableRow(String... columns) {
		sb.append("<tr class=\"" + (flipper ? "row" : "alt") + "Color\">\n");
		for (int i = 0; i < columns.length; i++) {
			// The logic here doesn't really make sense,
			//  but it also makes a lot of sense.
			String classes = "colFirst";
			if (i == columns.length - 1)
				classes = "colLast";
			sb.append("<td class=\"" + classes + "\" scope=\"col\">" + columns[i] + "</d>\n");
		}
		sb.append("</tr>\n");
		flipper = !flipper;
	}
	
	/**
	 * Places a page break.
	 * Strategic use of this can prevent awkward half-written tables.
	 * Overuse of this annoys the LaTaX, who speaks for the trees.
	 */
	public void pageBreak() {
		sb.append("<div style=\"page-break-before: always;\"></div>\n");
	}
}