package mccrossnet.workbench.introspection;

import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Field;
import java.util.LinkedList;

/**
 * This is not reliable. Do not touch it with a 1-kilometre pole.
 * If you touch this, and you tell me you mysteriously got ten kinds of space vomiting disease later...
 * Your problem. Not mine.
 */
public class ExtractedDoc {
	public final ExtractedDoc parent;
	public final Class clazz;
	
	public final LinkedList<String> entries = new LinkedList<String>();
	
	public ExtractedDoc(Class victim) {
		Class sc = victim.getSuperclass();
		if ((sc != null) && (sc != Object.class)) {
			parent = new ExtractedDoc(sc);
		} else {
			parent = null;
		}
		clazz = victim;
		try {
			BufferedReader br = new BufferedReader(new FileReader("src/main/java/" + clazz.getName().replace('.', '/') + ".java"));
			String buildingJavaDoc = null;
			while (true) {
				String s = br.readLine();
				if (s == null)
					break;
				s = s.trim();
				if (s.startsWith("/**")) {
					buildingJavaDoc = s + "\n";
				} else if (buildingJavaDoc != null) {
					buildingJavaDoc += s + "\n";
					if (s.startsWith("*/")) {
						entries.add(buildingJavaDoc);
						buildingJavaDoc = null;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getDescription() {
		if (entries.size() != 0)
			return entries.getFirst();
		return "";
	}
	
	public String getFieldDescription(Field f) {
		Field[] fields = clazz.getDeclaredFields();
		for (int i = 0; i < fields.length; i++) {
			if (fields[i].getName().equals(f.getName()))
				if (entries.size() > (i + 1))
					return entries.get(i + 1);
		}
		if (parent == null)
			return "";
		return parent.getFieldDescription(f);
	}
}
