package mccrossnet.base.core;

/**
 * The ID Registry structure is kinda weird.
 * Basically, it has to import the registry from fields.
 */
public interface IDRegistry {
	ID[] getAllIDs();
}
