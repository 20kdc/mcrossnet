/**
 * This package contains the basic outermost structural classes.
 * It does not; in fact actively avoids; providing any singletons.
 * If you're looking for those, look at: mccrossnet.interconnect
 * ...which uses enums to handle the indexing.
 * As that is quite 'rigid', I'm phasing out it's usage in the main code.
 */
package mccrossnet.base.core;