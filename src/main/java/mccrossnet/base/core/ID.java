package mccrossnet.base.core;

/**
 * Represents an ID in the ID registry.
 */
public final class ID {
	public final IDKind type;
	public final String name;
	public final int id;
	
	public ID(IDKind k, String n, int i) {
		type = k;
		name = n;
		id = i;
	}
	
	@Override
	public int hashCode() {
		return type.hashCode() + id + name.hashCode();
	}
	
	@Override
	public String toString() {
		return type.toString() + "[" + id + "] : " + name;
	}
}
