package mccrossnet.base.core;

/**
 * Represents a kind (namespace) of ID.
 * This is NOT an enum, nor is it final, for extensibility. 
 */
public class IDKind {
	public final String name;
	
	public static final IDKind BLOCK = new IDKind("block");
	public static final IDKind ITEM = new IDKind("item");
	public static final IDKind MOB = new IDKind("mob");
	public static final IDKind OBJECT = new IDKind("object");
	public static final IDKind WINDOW = new IDKind("window");
	public static final IDKind MOBACTION = new IDKind("mobaction");
	
	public IDKind(String name) {
		this.name = name;
	}
}
