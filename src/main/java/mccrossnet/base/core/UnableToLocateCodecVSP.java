package mccrossnet.base.core;

/**
 * This is a "partial failure" Version Specifying Packet.
 * Try not to get here. Ever.
 */
public class UnableToLocateCodecVSP implements VersionSpecifyingPacket {

	public final int version;
	
	public UnableToLocateCodecVSP(int v) {
		version = v;
	}
	
	@Override
	public String toString() {
		return super.toString() + "[" + version + "]";
	}
	
	@Override
	public int getProtocolVersion() {
		return version;
	}

}
