package mccrossnet.base.core;

import java.io.IOException;

/*
 * The interface for a generic codec.
 * Must have the constructor (InputStream, OutputStream, boolean)
 * The structure of packets is intended to be such that, for a packet that has
 *  identical semantics and structure between versions, you can pass it as-is.
 * A codec may be READ and WRITTEN TO on separate threads,
 *  as these target separate streams.
 * (NOTE: Encryption & compression options may complicate this;
 *  in this situation, codec-specific methods should be added,
 *  that atomically perform the whole sequence.)
 */
public interface Connection {
	/**
	 * Gets the Protocol this Connection is based on.
	 * @return The Protocol.
	 */
	public Protocol getProtocol();
	
	/**
	 * Receives a packet over the connection.
	 * 
	 * @return The packet received.
	 * @throws IOException
	 */
	public Packet read() throws IOException;

	/**
	 * Sends a packet over the connection.
	 * It must be supported to send the packet type in the current connection state.
	 * 
	 * @param p The packet to send.
	 * @throws IOException
	 */
	public void write(Packet packet) throws IOException;
}
