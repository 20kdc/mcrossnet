package mccrossnet.base.core;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Represents the information for a specific version of the protocol.
 */
public interface Protocol {
	/**
	 * Creates a Connection.
	 * @param in The input.
	 * @param out The output.
	 * @param amServer True if this is the server-side; otherwise this is the client-side.
	 * @return The connection.
	 */
	Connection makeConnection(InputStream in, OutputStream out, boolean amServer);
	
	/**
	 * @return The IDRegistry for this protocol version.
	 */
	IDRegistry getRegistry();
}
