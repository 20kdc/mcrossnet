package mccrossnet.base.core;

/**
 * A VersionSpecifyingPacket is the Handshaker response.
 * So for proper integration in the Handshaker, this needs to be present in the relevant packet.
 */
public interface VersionSpecifyingPacket extends Packet {
	/**
	 * @return The identified protocol version.
	 */
	int getProtocolVersion();
}
