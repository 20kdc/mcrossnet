package mccrossnet.base;

import java.io.IOException;
import java.io.InputStream;

/**
 * Solely for debugging.
 */
public class DebugInputStream extends InputStream {
	public final InputStream base;
	
	public DebugInputStream(InputStream b) {
		base = b;
	}
	
	@Override
	public int read() throws IOException {
		int b = base.read();
		try {
			throw new RuntimeException("byte = " + b);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return b;
	}
}
