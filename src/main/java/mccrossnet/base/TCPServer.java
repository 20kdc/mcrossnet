package mccrossnet.base;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.function.Consumer;

/**
 * A 'kinda-forking' TCP server.
 * Spawns a thread for each incoming socket, then runs the consumer on that thread.
 * Note that the consumer is given responsibility for getting rid of the socket.
 */
public class TCPServer implements Runnable {
	public final ServerSocket server;
	public final Consumer<Socket> consumer;
	
	public TCPServer(ServerSocket server, Consumer<Socket> consumer) {
		this.server = server;
		this.consumer = consumer;
	}
	
	public void run() {
		while (true) {
			try {
				final Socket st = server.accept();
				new Thread() {
					@Override
					public void run() {
						consumer.accept(st);
					}
				}.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
