package mccrossnet.base.valarch;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.LinkedList;

import mccrossnet.base.core.ID;
import mccrossnet.base.core.IDKind;
import mccrossnet.base.core.IDRegistry;

/**
 * A field-based IDRegistry.
 */
public class FieldStoreIDRegistry implements IDRegistry {
	private ID[] cachedIDs;
	
	@Override
	public ID[] getAllIDs() {
		ID[] cached = cachedIDs;
		if (cached == null) {
			LinkedList<ID> ids = new LinkedList<ID>();
			try {
				for (Field f : getClass().getFields()) {
					if (Modifier.isStatic(f.getModifiers())) {
						IDKind mapping = null;
						
						if (f.getName().startsWith("I_"))
							mapping = IDKind.ITEM;
						else if (f.getName().startsWith("B_"))
							mapping = IDKind.BLOCK;
						else if (f.getName().startsWith("O_"))
							mapping = IDKind.OBJECT;
						else if (f.getName().startsWith("M_"))
							mapping = IDKind.MOB;
						else if (f.getName().startsWith("W_"))
							mapping = IDKind.WINDOW;
						else if (f.getName().startsWith("MA_"))
							mapping = IDKind.MOBACTION;
						
						if (mapping == null)
							continue;
						Object value = f.get(null);
						int v = 0;
						if (value instanceof Byte)
							v = (byte) (Byte) value;
						else if (value instanceof Short)
							v = (short) (Short) value;
						else if (value instanceof Integer)
							v = (int) (Integer) value;
						else
							throw new RuntimeException("can't convert " + f);
						ids.add(new ID(mapping, f.getName().substring(2), v));
					}
				}
			} catch (Exception e) {
				
			}
			return cachedIDs = ids.toArray(new ID[0]);
		}
		return cached;
	}

}
