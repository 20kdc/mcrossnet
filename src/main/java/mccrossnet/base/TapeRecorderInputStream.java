package mccrossnet.base;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;

/**
 * This is a little pet utility.
 * It can be used to record everything read from it.
 * Then it can 'rewind the tape' and play that back.
 */
public class TapeRecorderInputStream extends FilterInputStream {
	private ByteArrayOutputStream baos = new ByteArrayOutputStream();
	
	private boolean recording = false;

	public TapeRecorderInputStream(InputStream base) {
		super(base);
	}
	
	@Override
	public int read() throws IOException {
		int b = in.read();
		if (b != -1)
			if (recording)
				baos.write(b);
		return b;
	}
	
	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		int amount = super.read(b, off, len);
		if (amount <= 0)
			return amount;
		// Assuming no shenanigans with threading, this'll be fine
		if (recording)
			baos.write(b, off, amount);
		return amount;
	}
	
	/**
	 * Begins recording.
	 */
	public void startPreread() {
		recording = true;
	}
	
	/**
	 * Cancels the recording.
	 */
	public void cancelPreread() {
		baos.reset();
		recording = false;
	}
	
	/**
	 * Ends the recording, prepending everything recorded so far to the stream for re-reading.
	 */
	public void endPreread() {
		in = new SequenceInputStream(new ByteArrayInputStream(baos.toByteArray()), in);
		baos.reset();
		recording = false;
	}
}
