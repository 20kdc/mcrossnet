package mccrossnet.base.netarch.hub;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Struct;
import java.util.HashMap;

import mccrossnet.base.core.Connection;
import mccrossnet.base.core.Packet;
import mccrossnet.base.core.Protocol;
import mccrossnet.base.data.BaseFormat;
import mccrossnet.base.data.DefaultFormatContext;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.ValueFormat;
import mccrossnet.base.data.formats.BooleanFormat;
import mccrossnet.base.data.formats.ByteFormat;
import mccrossnet.base.data.formats.IntFormat;
import mccrossnet.base.data.formats.LongFormat;
import mccrossnet.base.data.formats.ShortFormat;
import mccrossnet.base.data.formats.StructStructFormat;
import mccrossnet.universal.PVersion;

/**
 * Represents a "schematic" for E0/1/2 connection types that packets can be attached to.
 */
public class HubSchematic implements FormatContext {
	/**
	 * CtoS packet map. Try not to manipulate this directly.
	 */
	public final Class[] packetTypesCtos = new Class[256];
	
	/**
	 * StoC packet map. Try not to manipulate this directly.
	 */
	public final Class[] packetTypesStoc = new Class[256];

	public ValueFormat<String> stringFormat;

	public HubSchematic(ValueFormat<String> stringFormat) {
		this.stringFormat = stringFormat;
	}
	
	/**
	 * Declares a packet type (both CtoS and StoC).
	 * @param i The command byte.
	 * @param c The packet class.
	 */
	protected void attach(int i, Class c) {
		attachCtos(i, c);
		attachStoc(i, c);
	}

	/**
	 * Declares a packet type (CtoS).
	 * @param i The command byte.
	 * @param c The packet class.
	 */
	protected void attachCtos(int i, Class c) {
		if (!Packet.class.isAssignableFrom(c))
			throw new RuntimeException(c + " not packet");
		if (packetTypesCtos[i] != null)
			throw new RuntimeException("overwrite of " + i + " without detach");
		packetTypesCtos[i] = c;
	}

	/**
	 * Declares a packet type (StoC).
	 * @param i The command byte.
	 * @param c The packet class.
	 */
	protected void attachStoc(int i, Class c) {
		if (!Packet.class.isAssignableFrom(c))
			throw new RuntimeException(c + " not packet");
		if (packetTypesStoc[i] != null)
			throw new RuntimeException("overwrite of " + i + " without detach");
		packetTypesStoc[i] = c;
	}
	
	/**
	 * Detaches a packet type (both CtoS and StoC), presumably added by a superclass.
	 * @param i The command byte.
	 * @param c The packet class.
	 */
	protected void detach(int i) {
		detachCtos(i);
		detachStoc(i);
	}
	
	/**
	 * Detaches a packet type (CtoS), presumably added by a superclass.
	 * @param i The command byte.
	 * @param c The packet class.
	 */
	protected void detachCtos(int i) {
		if (packetTypesCtos[i] == null)
			throw new RuntimeException("detach of " + i + " without attach");
		packetTypesCtos[i] = null;
	}

	/**
	 * Detaches a packet type (StoC), presumably added by a superclass.
	 * @param i The command byte.
	 * @param c The packet class.
	 */
	protected void detachStoc(int i) {
		if (packetTypesStoc[i] == null)
			throw new RuntimeException("detach of " + i + " without attach");
		packetTypesStoc[i] = null;
	}

	@Override
	public <T> BaseFormat<T> getDefaultFormat(Class<T> fieldType) {
		if (fieldType == String.class)
			return (BaseFormat<T>) stringFormat;
		return DefaultFormatContext.INSTANCE.getDefaultFormat(fieldType);
	}
}
