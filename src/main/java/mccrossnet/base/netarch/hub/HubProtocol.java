package mccrossnet.base.netarch.hub;

import java.io.InputStream;
import java.io.OutputStream;

import mccrossnet.base.core.Connection;
import mccrossnet.base.core.IDRegistry;
import mccrossnet.base.core.Protocol;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.ValueFormat;
import mccrossnet.base.data.formats.ByteFormat;
import mccrossnet.base.data.formats.ShortFormat;

/**
 * ...Not going to lie:
 * This is pretty much just a device to make porting the older code a lot more convenient.
 */
public class HubProtocol extends HubSchematic implements Protocol {

	public IDRegistry registry;
	
	public HubProtocol(ValueFormat<String> stringFormat, IDRegistry registry) {
		super(stringFormat);
		this.registry = registry;
	}

	@Override
	public Connection makeConnection(InputStream in, OutputStream out, boolean amServer) {
		return new HubConnection(this, this, in, out, amServer);
	}

	@Override
	public IDRegistry getRegistry() {
		return registry;
	}
	
}
