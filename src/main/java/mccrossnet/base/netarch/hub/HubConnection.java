package mccrossnet.base.netarch.hub;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;

import mccrossnet.base.core.Connection;
import mccrossnet.base.core.Packet;
import mccrossnet.base.core.Protocol;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.Struct;
import mccrossnet.universal.PVersion;

/**
 * Decodes & encodes the Era0/Era1/Era2 packet framing.
 * This is basically no framing at all.
 * However, splitting things this way makes for a very simple design,
 *  that can adapt to packet ID changes and the like.
 */
public class HubConnection implements Connection {
	// Useful for auditing.
	public final Class[] packetTypesReceive;
	public final Class[] packetTypesSendInv;
	private final HashMap<Class, Integer> packetTypesSend = new HashMap<>();
	private final DataInputStream read;
	private final OutputStream write;
	private final FormatContext version;
	private final Protocol versionProto;
	
	public HubConnection(Protocol pro, HubSchematic ver, InputStream r, OutputStream w, boolean server) {
		read = new DataInputStream(r);
		write = w;
		
		version = ver;
		versionProto = pro;
		
		if (server) {
			packetTypesReceive = ver.packetTypesCtos;
			packetTypesSendInv = ver.packetTypesStoc;
		} else {
			packetTypesReceive = ver.packetTypesStoc;
			packetTypesSendInv = ver.packetTypesCtos;
		}
		for (int i = 0; i < 256; i++)
			if (packetTypesSendInv[i] != null)
				packetTypesSend.put(packetTypesSendInv[i], i);
	}
	
	@Override
	public Protocol getProtocol() {
		return versionProto;
	}
	
	@Override
	public Packet read() throws IOException {
		int p = read.readByte() & 0xFF;
		Class remainder = packetTypesReceive[p];
		if (remainder == null)
			throw new IOException("Unrecognized packet type " + p + ".");
		Struct o;
		try {
			o = (Struct) remainder.newInstance();
		} catch (Exception e) {
			// must not be possible
			throw new RuntimeException(e);
		}
		o.readContent(version, read);
		return (Packet) o;
	}
	
	@Override
	public void write(Packet o) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(baos);
		Integer i = packetTypesSend.get(o.getClass());
		if (i == null)
			throw new IOException("Unrecognized packet class" + o + ".");
		out.writeByte(i);
		((Struct) o).writeContent(version, out);
		baos.writeTo(write);
	}
}
