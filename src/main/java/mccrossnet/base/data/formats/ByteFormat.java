package mccrossnet.base.data.formats;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.ValueFormat;
import mccrossnet.base.data.FormatContext;

/**
 * This implements a conversion to and from Byte by reading and writing a byte.
 */
public class ByteFormat implements ValueFormat<Byte> {
	public static final ByteFormat INSTANCE = new ByteFormat();

	@Override
	public Byte read(FormatContext ver, DataInputStream dis) throws IOException {
		return dis.readByte();
	}

	@Override
	public void write(FormatContext ver, DataOutputStream dos, Byte value) throws IOException {
		dos.writeByte(value);
	}
}
