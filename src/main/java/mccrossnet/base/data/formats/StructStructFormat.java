package mccrossnet.base.data.formats;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.Struct;
import mccrossnet.base.data.StructFormat;

/**
 * The StructFormat for Structs.
 */
public class StructStructFormat implements StructFormat<Struct> {
	public static final StructStructFormat INSTANCE = new StructStructFormat();

	@Override
	public void write(FormatContext ver, DataOutputStream dos, Struct value) throws IOException {
		value.writeContent(ver, dos);
	}

	@Override
	public void read(FormatContext ver, DataInputStream dis, Struct value) throws IOException {
		value.readContent(ver, dis);
	}

	@Override
	public String clarify(Struct value) {
		return "[" + value.getClass().getSimpleName() + "]";
	}
}
