package mccrossnet.base.data.formats;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.ValueFormat;
import mccrossnet.base.data.FormatContext;

/**
 * This implements a conversion to and from Short by reading and writing a byte.
 * It's weird.
 */
public class ByteAsShortFormat implements ValueFormat<Short> {
	public static final ByteAsShortFormat INSTANCE = new ByteAsShortFormat();

	@Override
	public Short read(FormatContext ver, DataInputStream dis) throws IOException {
		return (short) dis.readByte();
	}

	@Override
	public void write(FormatContext ver, DataOutputStream dos, Short value) throws IOException {
		dos.writeByte(value);
	}
}
