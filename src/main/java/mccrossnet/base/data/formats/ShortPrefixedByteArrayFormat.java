package mccrossnet.base.data.formats;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.ValueFormat;
import mccrossnet.base.data.FormatContext;

/**
 * This stores a byte array as a short (length) followed by that many bytes.
 */
public class ShortPrefixedByteArrayFormat implements ValueFormat<byte[]> {
	public static final ShortPrefixedByteArrayFormat INSTANCE = new ShortPrefixedByteArrayFormat(1);
	public final int multiplier; 
	
	public ShortPrefixedByteArrayFormat(int m) {
		multiplier = m;
	}
	
	@Override
	public byte[] read(FormatContext ver, DataInputStream dis) throws IOException {
		byte[] data = new byte[dis.readUnsignedShort() * multiplier];
		dis.readFully(data);
		return data;
	}

	@Override
	public void write(FormatContext ver, DataOutputStream dos, byte[] value) throws IOException {
		int len = value.length / multiplier;
		if (len > 65535)
			throw new IOException("data too long");
		dos.writeShort((short) len);
		dos.write(value);
	}
}
