package mccrossnet.base.data.formats;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StructFormat;

/**
 * This is a fixed-size byte array.
 */
public class FixedSizeByteArrayFormat implements StructFormat<byte[]> {
	public static final FixedSizeByteArrayFormat INSTANCE = new FixedSizeByteArrayFormat();
	
	@Override
	public void write(FormatContext ver, DataOutputStream dos, byte[] value) throws IOException {
		dos.write(value);
	}

	@Override
	public void read(FormatContext ver, DataInputStream dis, byte[] value) throws IOException {
		dis.readFully(value);
	}

	@Override
	public String clarify(byte[] value) {
		return "[" + value.length + "]";
	}

}
