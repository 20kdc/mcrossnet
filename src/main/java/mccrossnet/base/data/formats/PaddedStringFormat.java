package mccrossnet.base.data.formats;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.ValueFormat;
import mccrossnet.base.data.FormatContext;

/**
 * Encodes and decodes the "padded string" format used by Classic.
 * This is 64 ASCII bytes.
 * The string begins at the start, and ends with the run of spaces starting at the end.
 */
public final class PaddedStringFormat implements ValueFormat<String> {
	public static final PaddedStringFormat INSTANCE = new PaddedStringFormat();

	@Override
	public String read(FormatContext ver, DataInputStream dis) throws IOException {
		byte[] data = new byte[64];
		dis.readFully(data);
		String text = new String(data, "US-ASCII");
		while (text.endsWith(" "))
			text = text.substring(0, text.length() - 1);
		return text;
	}

	@Override
	public void write(FormatContext ver, DataOutputStream dos, String value) throws IOException {
		byte[] data = value.getBytes("US-ASCII");
		if (data.length <= 64) {
			dos.write(data);
			for (int i = data.length; i < 64; i++)
				dos.write(0x20);
		} else {
			throw new IOException("string too long");
		}
	}
}
