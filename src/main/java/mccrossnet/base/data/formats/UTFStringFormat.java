package mccrossnet.base.data.formats;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.ValueFormat;
import mccrossnet.base.data.FormatContext;

/**
 * A wrapper around readUTF/writeUTF, the string format used by Alpha and earlier Beta.
 * See java.io.DataInputStream.writeUTF and such.
 */
public final class UTFStringFormat implements ValueFormat<String> {
	public static final UTFStringFormat INSTANCE = new UTFStringFormat();

	@Override
	public String read(FormatContext ver, DataInputStream dis) throws IOException {
		return dis.readUTF();
	}

	@Override
	public void write(FormatContext ver, DataOutputStream dos, String value) throws IOException {
		dos.writeUTF(value);
	}
}
