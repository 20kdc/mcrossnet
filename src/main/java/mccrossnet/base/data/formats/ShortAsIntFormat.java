package mccrossnet.base.data.formats;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.ValueFormat;
import mccrossnet.base.data.FormatContext;

/**
 * This implements a conversion to and from Integer by reading and writing a short.
 */
public class ShortAsIntFormat implements ValueFormat<Integer> {
	public static final ShortAsIntFormat INSTANCE = new ShortAsIntFormat();
	
	@Override
	public Integer read(FormatContext ver, DataInputStream dis) throws IOException {
		return (int) dis.readShort();
	}

	@Override
	public void write(FormatContext ver, DataOutputStream dos, Integer value) throws IOException {
		dos.writeShort(value);
	}
}
