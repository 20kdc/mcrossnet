package mccrossnet.base.data.formats;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.ValueFormat;
import mccrossnet.base.data.FormatContext;

/**
 * Implements the "wide string format" used by Beta 1.5 and onwards.
 * This is an unsigned short followed by that many Java characters (UTF-16 unsigned shorts).
 */
public final class WideStringFormat implements ValueFormat<String> {
	public static final WideStringFormat INSTANCE = new WideStringFormat();

	@Override
	public String read(FormatContext ver, DataInputStream dis) throws IOException {
		char[] res = new char[dis.readUnsignedShort()];
		for (int i = 0; i < res.length; i++)
			res[i] = dis.readChar();
		return new String(res);
	}

	@Override
	public void write(FormatContext ver, DataOutputStream dos, String value) throws IOException {
		char[] text = value.toCharArray();
		dos.writeShort(text.length);
		for (int i = 0; i < text.length; i++)
			dos.writeChar(text[i]);
	}
}
