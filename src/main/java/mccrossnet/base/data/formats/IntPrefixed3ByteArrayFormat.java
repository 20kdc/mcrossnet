package mccrossnet.base.data.formats;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.ValueFormat;
import mccrossnet.base.data.FormatContext;

/**
 * This stores a byte array as an int (length) followed by that many groups of 3 bytes.
 */
public class IntPrefixed3ByteArrayFormat extends IntPrefixedByteArrayFormat {
	public static final IntPrefixed3ByteArrayFormat INSTANCE = new IntPrefixed3ByteArrayFormat();
	
	public IntPrefixed3ByteArrayFormat() {
		super(3);
	}
}
