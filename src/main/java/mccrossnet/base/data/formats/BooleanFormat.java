package mccrossnet.base.data.formats;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;

import mccrossnet.base.data.ValueFormat;
import mccrossnet.base.data.FormatContext;

/**
 * This implements a conversion to and from Boolean by reading and writing a boolean.
 */
public class BooleanFormat implements ValueFormat<Boolean> {
	public static final BooleanFormat INSTANCE = new BooleanFormat();

	@Override
	public Boolean read(FormatContext ver, DataInputStream dis) throws IOException {
		return dis.readBoolean();
	}

	@Override
	public void write(FormatContext ver, DataOutputStream dos, Boolean value) throws IOException {
		dos.writeBoolean(value);
	}
}
