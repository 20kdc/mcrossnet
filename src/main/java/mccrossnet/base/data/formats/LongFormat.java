package mccrossnet.base.data.formats;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.ValueFormat;
import mccrossnet.base.data.FormatContext;

/**
 * This implements a conversion to and from Long by reading and writing a long.
 */
public class LongFormat implements ValueFormat<Long> {
	public static final LongFormat INSTANCE = new LongFormat();
	
	@Override
	public Long read(FormatContext ver, DataInputStream dis) throws IOException {
		return dis.readLong();
	}

	@Override
	public void write(FormatContext ver, DataOutputStream dos, Long value) throws IOException {
		dos.writeLong(value);
	}
}
