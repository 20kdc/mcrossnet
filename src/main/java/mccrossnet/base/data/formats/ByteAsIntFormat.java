package mccrossnet.base.data.formats;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.ValueFormat;
import mccrossnet.base.data.FormatContext;

/**
 * This implements a conversion to and from Integer by reading and writing a byte.
 * It's weird.
 */
public class ByteAsIntFormat implements ValueFormat<Integer> {
	public static final ByteAsIntFormat INSTANCE = new ByteAsIntFormat();

	@Override
	public Integer read(FormatContext ver, DataInputStream dis) throws IOException {
		return (int) dis.readByte();
	}

	@Override
	public void write(FormatContext ver, DataOutputStream dos, Integer value) throws IOException {
		dos.writeByte(value);
	}
}
