package mccrossnet.base.data.formats;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.ValueFormat;
import mccrossnet.base.data.FormatContext;
import mccrossnet.base.data.StructFormat;

/**
 * This is what it says on the tin: A format for fields that just aren't present.
 * As such, these get hidden from the protocol specification documents.
 * This is a NOP StructFormat.
 */
public class NotPresentInThisVersionFormat<T> implements StructFormat<T> {
	public static final NotPresentInThisVersionFormat INSTANCE = new NotPresentInThisVersionFormat();
	
	@Override
	public void read(FormatContext ver, DataInputStream dis, T value) throws IOException {

	}

	@Override
	public void write(FormatContext ver, DataOutputStream dos, T value) throws IOException {
		
	}

	@Override
	public String clarify(T value) {
		return "";
	}
}
