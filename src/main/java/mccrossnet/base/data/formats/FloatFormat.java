package mccrossnet.base.data.formats;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.ValueFormat;
import mccrossnet.base.data.FormatContext;

/**
 * This implements a conversion to and from Integer by reading and writing an int.
 */
public class FloatFormat implements ValueFormat<Float> {
	public static final FloatFormat INSTANCE = new FloatFormat();
	
	@Override
	public Float read(FormatContext ver, DataInputStream dis) throws IOException {
		return dis.readFloat();
	}

	@Override
	public void write(FormatContext ver, DataOutputStream dos, Float value) throws IOException {
		dos.writeFloat(value);
	}
}
