package mccrossnet.base.data.formats;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.ValueFormat;
import mccrossnet.base.data.FormatContext;

/**
 * This stores a byte array as an int (length) followed by that many bytes.
 */
public class IntPrefixedByteArrayFormat implements ValueFormat<byte[]> {
	public static final IntPrefixedByteArrayFormat INSTANCE = new IntPrefixedByteArrayFormat(1);
	public final int multiplier;
	
	public IntPrefixedByteArrayFormat(int m) {
		multiplier = m;
	}
	
	@Override
	public byte[] read(FormatContext ver, DataInputStream dis) throws IOException {
		byte[] data = new byte[dis.readInt() * multiplier];
		dis.readFully(data);
		return data;
	}

	@Override
	public void write(FormatContext ver, DataOutputStream dos, byte[] value) throws IOException {
		int len = value.length / multiplier;
		dos.writeInt(len);
		dos.write(value);
	}
}
