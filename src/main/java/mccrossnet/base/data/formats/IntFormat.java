package mccrossnet.base.data.formats;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.ValueFormat;
import mccrossnet.base.data.FormatContext;

/**
 * This implements a conversion to and from Integer by reading and writing an int.
 */
public class IntFormat implements ValueFormat<Integer> {
	public static final IntFormat INSTANCE = new IntFormat();
	
	@Override
	public Integer read(FormatContext ver, DataInputStream dis) throws IOException {
		return dis.readInt();
	}

	@Override
	public void write(FormatContext ver, DataOutputStream dos, Integer value) throws IOException {
		dos.writeInt(value);
	}
}
