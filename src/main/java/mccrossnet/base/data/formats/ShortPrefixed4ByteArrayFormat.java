package mccrossnet.base.data.formats;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.ValueFormat;
import mccrossnet.base.data.FormatContext;

/**
 * This stores a byte array as a short (length) followed by that many groups of 4 bytes.
 * So for a length of 1, the byte array has 4 bytes in it.
 */
public class ShortPrefixed4ByteArrayFormat extends ShortPrefixedByteArrayFormat {
	public static final ShortPrefixed4ByteArrayFormat INSTANCE = new ShortPrefixed4ByteArrayFormat();

	public ShortPrefixed4ByteArrayFormat() {
		super(4);
	}
}
