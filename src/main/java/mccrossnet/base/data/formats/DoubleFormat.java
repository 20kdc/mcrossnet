package mccrossnet.base.data.formats;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.ValueFormat;
import mccrossnet.base.data.FormatContext;

/**
 * This implements a conversion to and from Integer by reading and writing an int.
 */
public class DoubleFormat implements ValueFormat<Double> {
	public static final DoubleFormat INSTANCE = new DoubleFormat();
	
	@Override
	public Double read(FormatContext ver, DataInputStream dis) throws IOException {
		return dis.readDouble();
	}

	@Override
	public void write(FormatContext ver, DataOutputStream dos, Double value) throws IOException {
		dos.writeDouble(value);
	}
}
