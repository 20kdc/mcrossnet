package mccrossnet.base.data.formats;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.ValueFormat;
import mccrossnet.base.data.FormatContext;

/**
 * This implements a conversion to and from Short by reading and writing a short.
 */
public class ShortFormat implements ValueFormat<Short> {
	public static final ShortFormat INSTANCE = new ShortFormat();
	
	@Override
	public Short read(FormatContext ver, DataInputStream dis) throws IOException {
		return dis.readShort();
	}

	@Override
	public void write(FormatContext ver, DataOutputStream dos, Short value) throws IOException {
		dos.writeShort(value);
	}
}
