package mccrossnet.base.data.formats;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.data.ValueFormat;
import mccrossnet.base.data.FormatContext;

/**
 * This stores a byte array as an unsigned byte (length) followed by that many bytes.
 */
public class BytePrefixedByteArrayFormat implements ValueFormat<byte[]> {
	public static final BytePrefixedByteArrayFormat INSTANCE = new BytePrefixedByteArrayFormat(1);
	public final int multiplier; 
	
	public BytePrefixedByteArrayFormat(int m) {
		multiplier = m;
	}
	
	@Override
	public byte[] read(FormatContext ver, DataInputStream dis) throws IOException {
		byte[] data = new byte[dis.readUnsignedByte() * multiplier];
		dis.readFully(data);
		return data;
	}

	@Override
	public void write(FormatContext ver, DataOutputStream dos, byte[] value) throws IOException {
		int len = value.length / multiplier;
		if (len > 255)
			throw new IOException("data too long");
		dos.writeByte(len);
		dos.write(value);
	}
}
