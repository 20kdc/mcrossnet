package mccrossnet.base.data;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mccrossnet.base.core.Protocol;

/**
 * Represents a packet based on the java.io.DataInputStream / java.io.DataOutputStream classes.
 * All instances of this interface for use in standard protocols MUST:
 * 1. Implement the default constructor.
 */
public interface Struct {
	/**
	 * Reads content (this doesn't include framing) into the instance.
	 *
	 * @param ver The ValueFormatContext for the default assignment of sub-types.
	 * @param in The stream to work with.
	 * @throws IOException
	 */
	void readContent(FormatContext ver, DataInputStream in) throws IOException;
	
	/**
	 * Writes content (this doesn't include framing) from the instance.
	 * 
	 * @param ver The ValueFormatContext for the default assignment of sub-types.
	 * @param out Data to output.
	 * @throws IOException
	 */
	void writeContent(FormatContext ver, DataOutputStream out) throws IOException;
}
