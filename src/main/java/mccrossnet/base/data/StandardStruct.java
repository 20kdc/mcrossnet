package mccrossnet.base.data;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;

import mccrossnet.base.data.annotations.Avoids;
import mccrossnet.base.data.annotations.Format;
import mccrossnet.base.data.annotations.Marker;
import mccrossnet.base.data.annotations.Order;
import mccrossnet.base.data.annotations.OrderIf;
import mccrossnet.base.data.annotations.Requires;
import mccrossnet.base.data.formats.NotPresentInThisVersionFormat;

/**
 * A base class for simple "structure" types.
 * Automatically collates it's own fields to assemble the result.
 */
public class StandardStruct implements Struct {
	private static ConcurrentHashMap<CacheKey, Field[]> stashedOrders = new ConcurrentHashMap<CacheKey, Field[]>();
	
	/**
	 * @param contextClass context class, or null for view order
	 * @return Field array in the order the fields are serialized.
	 */
	public Field[] getFieldOrder(Class contextClass) {
		CacheKey key = new CacheKey(getClass(), contextClass);
		Field[] data = stashedOrders.get(key);
		if (data == null) {
			LinkedList<Field> fields = new LinkedList<Field>();
			HashMap<Field, Integer> fieldOrders = new HashMap<>();
			
			for (Field f : getClass().getFields()) {
				if (Modifier.isStatic(f.getModifiers()))
					continue;
				
				if (contextClass != null) {
					// Protocol-Time Existence Check
					boolean ok = true;
					for (Requires req : f.getAnnotationsByType(Requires.class))
						if (!req.value().isAssignableFrom(contextClass))
							ok = false;
					for (Avoids req : f.getAnnotationsByType(Avoids.class))
						if (req.value().isAssignableFrom(contextClass))
							ok = false;
				
					if (!ok)
						continue;
				}
				
				// Protocol-Time Ordering {
				Integer orderProvided = null;
				int priorityOfOrder = Integer.MIN_VALUE;
				
				for (Order o : f.getAnnotationsByType(Order.class))
					orderProvided = o.value();
				if (contextClass != null) {
					for (OrderIf i : f.getAnnotationsByType(OrderIf.class)) {
						if (i.flag().isAssignableFrom(contextClass)) {
							if (i.priority() > priorityOfOrder) {
								priorityOfOrder = i.priority();
								orderProvided = i.value();
							}
						}
					}
				}

				if (orderProvided == null)
					throw new RuntimeException("You broke it: " + f);
				fieldOrders.put(f, orderProvided);
				// }
				
				fields.add(f);
			}
			
			Collections.sort(fields, new Comparator<Field>() {
				@Override
				public int compare(Field o1, Field o2) {
					int oAi = fieldOrders.get(o1);
					int oBi = fieldOrders.get(o2);
					if (oAi < oBi)
						return -1;
					if (oAi > oBi)
						return 1;
					return 0;
				}
			});
			
			data = fields.toArray(new Field[0]);
			stashedOrders.put(key, data);
		}
		return data;
	}
	
	/**
	 * Gets the format of a field.
	 * @param ver Protocol version for default checks
	 * @param f Field
	 * @return The format instance, or null if not available.
	 */
	public BaseFormat getFieldFormat(FormatContext ver, Field f) {
		try {
			// Format override {
			Format fmt = f.getAnnotation(Format.class);
			// Note the usage of "getDeclaredField" here.
			// This is because of subclasses.
			if (fmt != null)
				return (BaseFormat) fmt.value().getDeclaredField("INSTANCE").get(null);
			// }
			// Lookup {
			Class c = f.getType();
			// Type override {
			Marker mrk = f.getAnnotation(Marker.class);
			if (mrk != null)
				c = mrk.value();
			// }
			return ver.getDefaultFormat(c);
			// }
		} catch (Exception e) {
			throw new RuntimeException("in " + f, e);
		}
	}
	
	@Override
	public void readContent(FormatContext ver, DataInputStream in) throws IOException {
		for (Field f : getFieldOrder(ver.getClass())) {
			try {
				BaseFormat vf = getFieldFormat(ver, f);
				if (vf != null) {
					if (vf instanceof ValueFormat) {
						Object val = ((ValueFormat) vf).read(ver, in);
						try {
							f.set(this, val);
						} catch (Exception e) {
							throw new RuntimeException(e);
						}
					} else {
						Object val;
						try {
							val = f.get(this);
						} catch (Exception e) {
							throw new RuntimeException(e);
						}
						((StructFormat) vf).read(ver, in, val);
					}
				}
			} catch (Exception e) {
				throw new IOException("during " + f.getName(), e);
			}
		}
	}

	@Override
	public void writeContent(FormatContext ver, DataOutputStream out) throws IOException {
		for (Field f : getFieldOrder(ver.getClass())) {
			BaseFormat vf = getFieldFormat(ver, f);
			if (vf != null) {
				Object val;
				try {
					val = f.get(this);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
				vf.write(ver, out, val);
			}
		}
	}
	
	@Override
	public String toString() {
		// Right, well, that finally fixed a long-standing problem with building these.
		StringBuilder sb = new StringBuilder(getClass().getSimpleName());
		sb.append('{');
		boolean first = true;
		for (Field f : getFieldOrder(null)) {
			if (!first)
				sb.append(", ");
			first = false;
			
			Object val;
			try {
				val = f.get(this);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
			sb.append(f.getName());
			sb.append(" = ");
			char hasSurround = 0;
			if (val instanceof String) {
				hasSurround = '"';
			} else if (val instanceof Character) {
				hasSurround = '\'';
			}
			if (hasSurround != 0)
				sb.append(hasSurround);
			sb.append(val);
			if (hasSurround != 0)
				sb.append(hasSurround);
		}
		sb.append('}');
		return sb.toString();
	}
	
	private class CacheKey {
		Class clazzA, clazzB;
		
		// B is nullable
		CacheKey(Class a, Class b) {
			clazzA = a;
			clazzB = b;
		}
		
		@Override
		public int hashCode() {
			if (clazzB == null)
				return clazzA.hashCode();
			return clazzA.hashCode() ^ clazzB.hashCode();
		}
		
		@Override
		public boolean equals(Object obj) {
			CacheKey ck = ((CacheKey) obj);
			if (ck.clazzA != clazzA)
				return false;
			if (ck.clazzB != clazzB)
				return false;
			return true;
		}
	}
}
