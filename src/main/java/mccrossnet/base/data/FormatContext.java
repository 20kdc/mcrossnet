package mccrossnet.base.data;

/**
 * Represents a context for ValueFormats to operate in.
 */
public interface FormatContext {
	/**
	 * Gets the default ValueFormat instance to use for a given value.
	 * 
	 * @param fieldType The class of the value to serialize/deserialize.
	 * @return The format (or null if not available)
	 */
	<T> BaseFormat<T> getDefaultFormat(Class<T> fieldType);
}
