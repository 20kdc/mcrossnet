package mccrossnet.base.data;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * Represents an encoder/decoder for another class for which GC work is prohibitive,
 *  so an attempt should be made to optimize to being simply a substructure.
 */
public interface StructFormat<T> extends BaseFormat<T> {
	/**
	 * Reads a value from a stream.
	 * 
	 * @param ver The ValueFormatContext for the default assignment of sub-types.
	 * @param dis The stream to read from.
	 * @param value The value to read into.
	 * @throws IOException
	 */
	public void read(FormatContext ver, DataInputStream dis, T value) throws IOException;
	
	/**
	 * Clarifies the nature of the format for documentation purposes.
	 * For example, a fixed-size array might return "[1024]".
	 * @param value
	 * @return The clarification (or an empty string for none)
	 */
	public String clarify(T value);
}
