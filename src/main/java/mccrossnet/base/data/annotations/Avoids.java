package mccrossnet.base.data.annotations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Documented
@Retention(RUNTIME)
@Target(FIELD)

/**
 * Indicates that the format context being castable to the given class removes this field.
 * Basically the opposite of Requires and follows the same AND semantics.
 */
public @interface Avoids {
	Class value();
}
