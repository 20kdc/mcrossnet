package mccrossnet.base.data.annotations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Documented
@Retention(RUNTIME)
@Target(FIELD)

/**
 * This represents the ordering of fields in a packet in a class.
 * Notably, the ordering number space is the same across the inheritance tree,
 *  allowing for subclasses to insert fields where they otherwise couldn't.
 */
public @interface Order {
	/**
	 * @return The value used for order sorting.
	 */
	int value();
}
