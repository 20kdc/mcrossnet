package mccrossnet.base.data.annotations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Documented
@Retention(RUNTIME)
@Target(FIELD)

/**
 * Indicates that the format context being castable to the given class is required.
 * If multiple Requires annotations are specified, the result is the AND of them all.
 * If this condition fails, the field is left unaffected on read and ignored on write.
 */
public @interface Requires {
	Class value();
}
