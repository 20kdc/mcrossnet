package mccrossnet.base.data.annotations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Documented
@Retention(RUNTIME)
@Target(FIELD)

/**
 * Sets the format of a given field.
 * That is, it indicates what class is used to serialize and deserialize it.
 */
public @interface Format {
	/**
	 * @return A JEFormat-derived class.
	 */
	Class value();
}
