package mccrossnet.base.data.annotations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Documented
@Retention(RUNTIME)
@Target(FIELD)

/**
 * Think Order, but now assume Requires was taped to it.
 */
public @interface OrderIf {
	/**
	 * The priority of this statement.
	 * The priority of a standard Order annotation is Integer.MIN_VALUE.
	 * @return The value used for sorting OrderIf 'statements'.
	 */
	int priority();
	
	/**
	 * The flag that enables this statement.
	 */
	Class flag();
	
	/**
	 * @return The value used for order sorting.
	 */
	int value();
}
