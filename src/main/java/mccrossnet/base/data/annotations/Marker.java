package mccrossnet.base.data.annotations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Documented
@Retention(RUNTIME)
@Target(FIELD)

/**
 * This is similar in intent to Format, but "tricks" the type into being seen differently.
 * This allows it to be picked up by the protocol. Good for inheritance-based data structures.
 */
public @interface Marker {
	public Class value();
}
