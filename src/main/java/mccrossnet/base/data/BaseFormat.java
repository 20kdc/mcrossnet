package mccrossnet.base.data;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * A base interface for both StructFormat and ValueFormat.
 * 
 * If ever passed by-class, and this will happen a lot:
 * MUST have a public static final value called INSTANCE that is initialized to an instance.
 */
public interface BaseFormat<T> {
	/**
	 * Writes a value to a stream.
	 * 
	 * @param ver The ValueFormatContext for the default assignment of sub-types.
	 * @param dos The stream to write to.
	 * @param value The value to write.
	 * @throws IOException
	 */
	public void write(FormatContext ver, DataOutputStream dos, T value) throws IOException;
}
