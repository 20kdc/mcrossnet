/**
 * This package contains the basic data components used to make actual packets.
 * Essentially, if something isn't explicitly protocol-specific, even if it is theoretically...
 * Put it in mccrossnet.base.data [ .formats if appropriate ] !
 */
package mccrossnet.base.data;