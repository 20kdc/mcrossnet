package mccrossnet.base.data;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * Represents an encoder/decoder for a primitive for which GC work doesn't matter.
 */
public interface ValueFormat<T> extends BaseFormat<T> {
	/**
	 * Reads a value from a stream.
	 * 
	 * @param ver The ValueFormatContext for the default assignment of sub-types.
	 * @param dis The stream to read from.
	 * @return The read value.
	 * @throws IOException
	 */
	public T read(FormatContext ver, DataInputStream dis) throws IOException;
}
