package mccrossnet.base.data;

import mccrossnet.base.data.formats.BooleanFormat;
import mccrossnet.base.data.formats.ByteFormat;
import mccrossnet.base.data.formats.DoubleFormat;
import mccrossnet.base.data.formats.FloatFormat;
import mccrossnet.base.data.formats.IntFormat;
import mccrossnet.base.data.formats.LongFormat;
import mccrossnet.base.data.formats.ShortFormat;
import mccrossnet.base.data.formats.StructStructFormat;

/**
 * Represents a 'sane default' format context.
 */
public class DefaultFormatContext implements FormatContext {
	public static final DefaultFormatContext INSTANCE = new DefaultFormatContext();
	
	@Override
	public <T> BaseFormat<T> getDefaultFormat(Class<T> fieldType) {
		if (fieldType == Byte.class || fieldType == byte.class) {
			return (BaseFormat<T>) ByteFormat.INSTANCE;
		} else if (fieldType == Short.class || fieldType == short.class) {
			return (BaseFormat<T>) ShortFormat.INSTANCE;
		} else if (fieldType == Integer.class || fieldType == int.class) {
			return (BaseFormat<T>) IntFormat.INSTANCE;
		} else if (fieldType == Long.class || fieldType == long.class) {
			return (BaseFormat<T>) LongFormat.INSTANCE;
		} else if (fieldType == Boolean.class || fieldType == boolean.class) {
			return (BaseFormat<T>) BooleanFormat.INSTANCE;
		} else if (fieldType == Float.class || fieldType == float.class) {
			return (BaseFormat<T>) FloatFormat.INSTANCE;
		} else if (fieldType == Double.class || fieldType == double.class) {
			return (BaseFormat<T>) DoubleFormat.INSTANCE;
		} else if (Struct.class.isAssignableFrom(fieldType)) {
			return (BaseFormat<T>) StructStructFormat.INSTANCE;
		}
		return null;
	}

}
