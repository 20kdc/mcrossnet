# WikiVG credits

## Explaination

wiki.vg was the main source of information used to build this library.

They did not write any of the code of this library, but they did provide the protocol specifications that made it possible.

As such, I'm trying to properly credit the contributors in this file.

## Classic

History appears to have been lost due to some sort of history deletion on wiki.vg.

This is the known list of contributors that I can gather:

+ Vexyl
+ F
+ Umby24
+ Aragas
+ Rom1504
+ Atybot
+ Egor
+ Galaxtone
+ Pokechu22
+ SpaceManiac
+ Sean

I simply don't have the information to be sure this is complete.

## The initial E2V3 header

Revision as of 03:18, 6 November 2010 by SpaceManiac (talk | contribs) (Removing redundant header and categorizing.)

```
This page presents a dissection of the new Minecraft Alpha protocol by aera (Andrew Godwin), ylt (Joe Carter) and TkTech (Tyler Kennedy). Additionally, credit goes to the citizens of #mcc who helped by providing packet dumps and insight. If you're having trouble, check out the FAQ. 
```

## The later header

Note: this was not the revision that introduced the header

Revision as of 22:54, 13 December 2013 by Xyphos (talk | contribs) (→‎Packet format)
(diff) ← Older revision | Latest revision (diff) | Newer revision → (diff)

```
Note: While you may use the contents of this page without restriction to create servers, clients, bots, etc… you still need to provide attribution to #mcdevs if you copy any of the contents of this page for publication elsewhere.
```

## Known Contributors By MediaWiki Usernames

This is in chronological order *of first contribution.*

These are grouped roughly by version areas (the grouping cannot be exact due to the structure).

But keep in mind the first-contribution rule.

### After E2V3

+ SpaceManiac
+ Immibis
+ Fador
+ Thejoshwolfe
+ Superjoe
+ Sadimusi
+ Jailout2000
+ Barneygale

### A Statistic Note

Statistic IDs were acquired from: https://www.minecraftforum.net/forums/minecraft-java-edition/survival-mode/221881-list-of-statistics-and-achievements-spoiler-alert

(minecraftforum user Zenexer)

### After E2V17

+ Jonneh
+ 14mRh4X0r
+ Umby24
+ TLUL
+ SirCmpwn
+ Vijfhoek
+ Ammaraskar
+ Clonejo
+ Vexyl
+ SimSonic
+ Dx
+ Md 5
+ Shoghicp
+ Pdelvo
+ Xoft
+ JoeDoyle23
+ DavidEGrayson
+ Socolin
+ Thinkofdeath
+ Riking
+ TkTech
+ Aadnk
+ Dav1d
+ Winny
+ Zuazo
+ Cybermaxke
+ Tehme
+ ZioCrocifisso
+ MinecraftShamrock
+ Tigerw

### During Netty

The entirety of the Netty transition was handled in the Pre-release Protocol page.

The details for that are documented later.

### After Netty (E2V4)

+ Ellisvlad
+ Geret13
+ 2016mfransen
+ Nuxas2
+ Drainedsoul
+ LazyLemons
+ Nickelpro1
+ Palz
+ STR Warrior
+ Devil Boy512
+ Brt
+ Xyphos
+ ReneBaracus
+ YukonAppleGeek
+ IBotPeaches
+ Yogu
+ Civilizedgravy
+ Xor Boole
+ Hunterbuscus 3rd
+ Heero
+ Cuitpoisson
+ Fysac
+ Stuntguy3000
+ Pollux31
+ TigerHix
+ Akaiyo
+ PaulBGD
+ Aaron1011
+ Fenhl
+ GuyPerplex
+ Podpage

(stopping here for now, because I'm not using information from these versions of the protocol yet)

## List For The Pre-release Protocol Page

For sanity reasons, this is a separate list.
Names will occur again in this list.

+ Barneygale
+ Jailout2000
+ Pdelvo
+ Sadimusi
+ SirCmpwn
+ Clonejo
+ Md 5
+ Shoghicp
+ DavidEGrayson
+ Thinkofdeath
+ Ammaraskar
+ Nickelpro
+ Dx
+ Dav1d
+ IBotPeaches
+ Aadnk
+ MonsieurApple
+ TLUL
+ Vbisbest
+ Nuxas
+ TheUnnamedDude
+ Steveice10
+ Ayunyan
+ MinecraftShamrock
+ Mct

### After 14w20a

(stopping here for now, because I'm not using information from these versions of the protocol yet)
